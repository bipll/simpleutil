/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__FUNCTIONAL_H_316cd487e530c5346a371c222c01f9cc1c4cd9b0
#define SIMPLEUTIL__FUNCTIONAL_H_316cd487e530c5346a371c222c01f9cc1c4cd9b0

#include <type_traits>
#include <limits>
#include <utility>
#include <functional>
#include "typeutil.h"
#include "tuple.h"

namespace nstd {

template<class F> struct function_type: function_type<decltype(::std::function(::std::declval<F>()))> {};
template<class F> using function_type_t = type_t<function_type<F>>;
template<class F> struct function_type<::std::function<F>>: box<F> {};


template<class... Vs> struct values: tuple<Vs...> {
	using This = values;
	using Parent = tuple<Vs...>;
	using Parent::Parent;

	constexpr tuple<Vs...> &&id() && noexcept { return ::std::move(*this); }
	constexpr tuple<Vs...> &id() & noexcept { return *this; }
	constexpr tuple<Vs...> const &id() const & noexcept { return *this; }
};
template<class... Vs> values(Vs &&...) -> values<Vs...>;

template<class V> struct values<V> {
	using This = values;
	constexpr values(V &&v) noexcept(::std::is_nothrow_move_constructible_v<V>): v(::std::forward<V>(v)) {}

	constexpr V &&id() && noexcept { return ::std::forward<V>(v); }
	constexpr V &id() & noexcept { return v; }
	constexpr V const &id() const & noexcept { return v; }
private:
	V v;
};

template<::std::size_t, class V> constexpr V &&get(values<V> &&v) noexcept { return ::std::move(v).id(); }
template<::std::size_t, class V> constexpr V &get(values<V> &v) noexcept { return v.id(); }
template<::std::size_t, class V> constexpr V const &get(values<V> const &v) noexcept { return v.id(); }

template<::std::size_t i, class... Vs> constexpr decltype(auto) get(values<Vs...> &&vs) noexcept { return ::std::get<i>(::std::move(vs).id()); }
template<::std::size_t i, class... Vs> constexpr decltype(auto) get(values<Vs...> &vs) noexcept { return ::std::get<i>(vs.id()); }
template<::std::size_t i, class... Vs> constexpr decltype(auto) get(values<Vs...> const &vs) noexcept { return ::std::get<i>(vs.id()); }


class feed_impl: private_namespace {
	template<class Vs, class F, ::std::size_t... is> static constexpr auto feed(Vs &&vs, ::std::index_sequence<is...>, F &&f)
		noexcept(NOEXCEPT(::std::invoke(::std::forward<F>(f), get<is>(::std::forward<Vs>(vs))...)))
	{
		return ::std::invoke(::std::forward<F>(f), get<is>(::std::forward<Vs>(vs))...);
	}

	template<class Vs, class Fs, ::std::size_t... is, ::std::size_t... js> static constexpr auto feed(
		Vs &&vs, ::std::index_sequence<is...> indices, Fs &&fs, ::std::index_sequence<js...>)
		noexcept(NOEXCEPT(values{feed(vs, indices, get<js>(::std::forward<Fs>(fs)))...}))
	{
		return values{feed(vs, indices, get<js>(::std::forward<Fs>(fs)))...};
	}

	template<class Vs, ::std::size_t... is, class... Fs> static constexpr auto feed(Vs &&vs, ::std::index_sequence<is...> indices, tuple<Fs...> &&fs)
		noexcept(NOEXCEPT(feed(::std::forward<Vs>(vs), indices, ::std::move(fs), ::std::index_sequence_for<Fs...>{})))
	{
		return feed(::std::forward<Vs>(vs), indices, ::std::move(fs), ::std::index_sequence_for<Fs...>{});
	}
	template<class Vs, ::std::size_t... is, class... Fs> static constexpr auto feed(Vs &&vs, ::std::index_sequence<is...> indices, tuple<Fs...> const &fs)
		noexcept(NOEXCEPT(feed(::std::forward<Vs>(vs), indices, fs, ::std::index_sequence_for<Fs...>{})))
	{
		return feed(::std::forward<Vs>(vs), indices, fs, ::std::index_sequence_for<Fs...>{});
	}
	template<class Vs, ::std::size_t... is, class... Fs> static constexpr auto feed(Vs &&vs, ::std::index_sequence<is...> indices, tuple<Fs...> &fs)
		noexcept(NOEXCEPT(feed(::std::forward<Vs>(vs), indices, fs, ::std::index_sequence_for<Fs...>{})))
	{
		return feed(::std::forward<Vs>(vs), indices, fs, ::std::index_sequence_for<Fs...>{});
	}

	template<class... Vs, class F> friend constexpr auto operator>>(values<Vs...> &&vs, F &&f)
		noexcept(NOEXCEPT(values{feed_impl::feed(::std::move(vs), ::std::index_sequence_for<Vs...>{}, ::std::forward<F>(f))}));
};

template<class... Vs, class F> constexpr auto operator>>(values<Vs...> &&vs, F &&f)
	noexcept(NOEXCEPT(values{feed_impl::feed(::std::move(vs), ::std::index_sequence_for<Vs...>{}, ::std::forward<F>(f))}))
{
	return values{feed_impl::feed(::std::move(vs), ::std::index_sequence_for<Vs...>{}, ::std::forward<F>(f))};
}


template<class... Fs> struct pipeline {
	using This = pipeline;

	constexpr pipeline(Fs &&...fs) noexcept(::std::is_nothrow_constructible_v<Elements, Fs...>): fs{::std::forward<Fs>(fs)...} {}

	template<class... Args> constexpr auto operator()(Args &&...args)
		noexcept(NOEXCEPT(perform(this, ::std::index_sequence_for<Fs...>{}, ::std::forward<Args>(args)...)))
	{
		return perform(this, ::std::index_sequence_for<Fs...>{}, ::std::forward<Args>(args)...);
	}
	template<class... Args> constexpr auto operator()(Args &&...args) const
		noexcept(NOEXCEPT(perform(this, ::std::index_sequence_for<Fs...>{}, ::std::forward<Args>(args)...)))
	{
		return perform(this, ::std::index_sequence_for<Fs...>{}, ::std::forward<Args>(args)...);
	}

	constexpr tuple<Fs...> elements() const noexcept(::std::is_nothrow_copy_constructible_v<tuple<Fs...>>) { return fs; }
private:
	using Elements = tuple<Fs...>;
	template<class Self, ::std::size_t... is, class... Args> static constexpr auto perform(Self *self, ::std::index_sequence<is...>, Args &&...args)
		noexcept(NOEXCEPT((values{::std::forward<Args>(args)...} >> ... >> get<is>(self->fs)).id()))
	{
		return (values{::std::forward<Args>(args)...} >> ... >> get<is>(self->fs)).id();
	}

	Elements fs;
};
template<class... Fs> pipeline(Fs &&...) -> pipeline<Fs...>;

template<class Base, class F, class... Gs> struct composition: ::std::pair<F, tuple<Gs...>>, Base {
	using This = composition;

	template<class Ctor> constexpr composition(Ctor &&base, F &&f, Gs &&...gs)
		noexcept(::std::is_nothrow_constructible_v<::std::pair<F, tuple<Gs...>>, F, tuple<Gs...>>
			 && ::std::is_nothrow_constructible_v<tuple<Gs...>, Gs...>
			 && ::std::is_nothrow_invocable_v<Ctor, ::std::add_lvalue_reference_t<F>, ::std::add_lvalue_reference_t<Gs>...>
			 && ::std::is_nothrow_constructible_v<Base, ::std::invoke_result_t<Ctor, F, Gs...>>)
		: ::std::pair<F, tuple<Gs...>>{::std::forward<F>(f), tuple<Gs...>{::std::forward<Gs...>(gs)}}
		, Base(::std::invoke(::std::forward<Ctor>(base), ::std::pair::first, ::std::pair::second)) {}

	constexpr ::std::pair<F2, F1> const &elements() const noexcept { return static_cast<::std::pair<F2, F1> const &>(*this); }
};
template<class Ctor, class F, class... Gs> composition(Base &&, F &&, Gs &&...) -> composition<::std::invoke_result_t<Ctor, F, Gs...>, F, Gs...>;

template<class Base, class F, class G> struct composition<F, G>: ::std::pair<F, G>, Base {
	using This = composition;

	template<class Ctor> constexpr composition(Ctor &&base, F &&f, G &&g)
		noexcept(::std::is_nothrow_constructible_v<::std::pair<F, G>, F, G>
			 && ::std::is_nothrow_invocable_v<Ctor, F, G>
			 && ::std::is_nothrow_constructible_v<Base, ::std::invoke_result_t<Ctor, ::std::add_lvalue_reference_t<F>, ::std::add_lvalue_reference_t<G>>>)
		: ::std::pair<F, tuple<Gs...>>{::std::forward<F>(f), tuple<Gs...>{::std::forward<Gs...>(gs)}}
		, Base(::std::invoke(::std::forward<Ctor>(base), ::std::pair::first, ::std::pair::second)) {}

	constexpr ::std::pair<F2, F1> const &elements() const noexcept { return static_cast<::std::pair<F2, F1> const &>(*this); }
};

class elements_impl: private_namespace {
	template<typename F> static constexpr bool is_nothrow_elementable_v() {
		if constexpr(like_one_of_v<::std::decay_t<F>, pipeline<>, composition<void, void, void>>) return noexcept(::std::declval<F>().elements());
		else return ::std::is_nothrow_constructible_v<tuple<F>, F>;
	}
	friend class pipe_impl;
};

class pipe_impl: private_namespace {
	template<typename F> static constexpr auto elements(F &&f) noexcept(elements_impl::is_nothrow_elementable_v<F>()) {
		if constexpr(like_one_of_v<::std::decay_t<F>, pipeline<>, composition<void, void, void>>) return f.elements();
		else return tuple<F>{::std::forward<F>(f)};
	}

	struct make_pipeline {
		template<typename... Fs> constexpr auto operator()(Fs &&...fs) noexcept(NOEXCEPT(pipeline(::std::forward<Fs>(fs)...))) {
			return pipeline(::std::forward<Fs>(fs)...);
		}
	};

	template<typename... Fs> friend constexpr auto pipe(Fs &&...fs)
		noexcept(NOEXCEPT(apply(pipe_impl::make_pipeline{}, pipe_impl::elements(::std::forward<Fs>(fs))...)));
};

template<typename... Fs> constexpr auto pipe(Fs &&...fs) noexcept(NOEXCEPT(apply(pipe_impl::make_pipeline{}, pipe_impl::elements(::std::forward<Fs>(fs))...))) {
	return apply(pipe_impl::make_pipeline{}, pipe_impl::elements(::std::forward<Fs>(fs))...);
}


template<class Root, class Branch> inline constexpr auto compose(Root &&root, Branch &&branch) {
	using BareRoot = ::std::decay_t<Root>;
	using BareBranch = ::std::decay_t<Branch>;
	if constexpr(like_one_of_v<BareRoot, pipeline<>, composition<void, void, void>>
		     || like_one_of_v<BareBranch, pipeline<>, composition<void, void, void>>
		     || pack::any_v<is_tuple, BareRoot, BareBranch>)
	{
		return pipe(::std::forward<Branch>(branch), ::std::forward<Root>(root));
	}
	else {
		return composition(
			[](Root const &root, Branch const &branch) constexpr {
				return [&root, &branch](auto &&...args) constexpr {
					return ::std::invoke(root, ::std::invoke(branch, ::std::forward<decltype(args)>(args)...));
				}
			}
			, ::std::forward<Root>(root), ::std::forward<Branch>(branch));
	}
}

template<class Root, class... Branches> inline constexpr auto compose(Root &&root, Branches &&...branches) {
	using BareRoot = ::std::decay_t<Root>;
	if constexpr(like_one_of_v<BareRoot, pipeline<>, composition<void, void, void>> || is_tuple<BareRoot>) {
		return pipe(tuple<Branches...>{::std::forward<Branches>(branches)...}, ::std::forward<Root>(root));
	}
	else {
		return composition(
			[](Root const &root, Branches const &...branches) constexpr {
				return [&root, &branches...](auto &&...args) constexpr {
					return ::std::invoke(root, ::std::invoke(branches, ::std::forward<decltype(args)>(args)...)...);
				}
			}
			, ::std::forward<Root>(root), tuple<Branches...>(::std::forward<Branches>(branches)...));
	}
}


template<class C> struct constructor {
	static constexpr ::std::size_t arity = max_size_v;
	template<class... Args> static constexpr bool compiles = ::std::is_constructible_v<C, Args...>;

	template<class... Args> static constexpr C call(Args &&...args) noexcept(::std::is_nothrow_constructible_v<C, Args...>) {
		return C(::std::forward<Args>(args)...);
	}
	template<class... Args> static constexpr C spread(Args &&...args) noexcept(::std::is_nothrow_constructible_v<C, Args...>) {
		return C(::std::forward<Args>(args)...);
	}
	template<class... Args> C operator()(Args &&...args) const noexcept(::std::is_nothrow_constructible_v<C, Args...>) {
		return C(::std::forward<Args>(args)...);
	}
};

}

#ifndef SHOW_NOTHING

#include "show.h"

namespace nstd {

SHOW_NSTD_ROOT(1, values);
SHOW_NSTD_ROOT(1, pipeline);
SHOW_NSTD_ROOT(1, composition);
SHOW_NSTD_ROOT(1, constructor);

}

#endif

#endif
