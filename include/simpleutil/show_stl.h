/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__SHOW_STL_H_4931b244d93ebf939915fa726645052744184106
#define SIMPLEUTIL__SHOW_STL_H_4931b244d93ebf939915fa726645052744184106

#ifndef SHOW_NOTHING

#include "show.h"

#include "__show/any.h"
#include "__show/array.h"
#include "__show/atomic.h"
#include "__show/bitset.h"
#include "__show/charconv.h"
#include "__show/chrono.h"
#include "__show/clocale.h"
#include "__show/compare.h"
#include "__show/complex.h"
#include "__show/condition_variable.h"
#include "__show/contract.h"
#include "__show/cstddef.h"
#include "__show/ctime.h"
#include "__show/deque.h"
#include "__show/exception.h"
#include "__show/filesystem.h"
#include "__show/forward_list.h"
#include "__show/fstream.h"
#include "__show/functional.h"
#include "__show/future.h"
#include "__show/initializer_list.h"
#include "__show/ios.h"
#include "__show/istream.h"
#include "__show/iterator.h"
#include "__show/limits.h"
#include "__show/list.h"
#include "__show/locale.h"
#include "__show/map.h"
#include "__show/memory.h"
#include "__show/mutex.h"
#include "__show/new.h"
#include "__show/optional.h"
#include "__show/ostream.h"
#include "__show/queue.h"
#include "__show/random.h"
#include "__show/ratio.h"
#include "__show/regex.h"
#include "__show/scoped_allocator.h"
#include "__show/set.h"
#include "__show/shared_mutex.h"
#include "__show/span.h"
#include "__show/sstream.h"
#include "__show/stack.h"
#include "__show/stdexcept.h"
#include "__show/streambuf.h"
#include "__show/string.h"
#include "__show/string_view.h"
#include "__show/system_error.h"
#include "__show/thread.h"
#include "__show/tuple.h"
#include "__show/typeindex.h"
#include "__show/typeinfo.h"
#include "__show/type_traits.h"
#include "__show/unordered_map.h"
#include "__show/unordered_set.h"
#include "__show/valarray.h"
#include "__show/variant.h"
#include "__show/vector.h"

#endif

#endif
