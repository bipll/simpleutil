/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__FUNCTOR_H_ac685d87aacb865786b20bc0279ffcb2f6fb53b8
#define SIMPLEUTIL__FUNCTOR_H_ac685d87aacb865786b20bc0279ffcb2f6fb53b8

#include <type_traits>
#include <utility>
#include <iterator>
#include <functional>
#include "iterator.h"
#include "operator.h"
#include "typeutil.h"
#include "functional.h"

namespace nstd {

template<class Ctr> struct functor;

template<typename Ctr> inline constexpr bool has_value(optional<Ctr> const &ctr) { return ctr.has_value(); }
template<typename Ctr> inline constexpr bool empty(optional<Ctr> const &ctr) { return ctr.empty(); }

template<typename Ctr> inline constexpr void seq(Ctr &&ctr) {
	if constexpr(::std::is_void_v<decltype(*adl_or_std::begin::call(ctr))>) {
		auto i = adl_or_std::begin::call(ctr);
		auto e = adl_or_std::end::call(ctr);
		while(i != e) *i++;
	}
	else for(auto &&i: ctr) {}
}

class functor_impl: private_namespace {
	template<class Ctr> friend struct functor;
	template<class Ctr, class F> class DiscardPoint {
		using Next = ::std::invoke_result_t<F>;
	public:
		constexpr DiscardPoint() = default;
		constexpr DiscardPoint(optional<Ctr> &&ctr, F &&f): ctr(::std::move(ctr)), f(::std::forward<F>(f)) { ctr.reset(); }

		auto begin() && { return iterator(::std::move(ctr), ::std::move(f), ::std::move(next)); }
		auto begin() const && { return iterator(::std::move(ctr), ::std::move(f), ::std::move(next)); }
		auto cbegin() const && { return iterator(::std::move(ctr), ::std::move(f), ::std::move(next)); }

		auto begin() & { return iterator(ctr, f, next); }
		auto begin() const & { return iterator(ctr, f, next); }
		auto cbegin() const & { return iterator(ctr, f, next); }

		auto end() && { return sentinel{}; }
		auto end() const && { return sentinel{}; }
		auto cend() const && { return sentinel{}; }

		auto end() & { return sentinel{}; }
		auto end() const & { return sentinel{}; }
		auto cend() const & { return sentinel{}; }

		constexpr operator Next() && {
			if(has_value(ctr)) {
				nstd::seq(*ctr);
				ctr.reset();
			}
			return f();
		}

		constexpr operator Next() const && {
			if(has_value(ctr)) {
				nstd::seq(*ctr);
				ctr.reset();
			}
			return f();
		}

		constexpr DiscardPoint &flatmap() { return *this; }
		constexpr DiscardPoint const &flatmap() const { return *this; }

		template<class G> constexpr auto flatmap(G &&g) && {
			return functor_impl::DiscardPoint(::std::move(ctr), compose(::std::forward<G>(g), ::std::move(f)));
		}
		template<class G> constexpr auto flatmap(G &&g) const && {
			return functor_impl::DiscardPoint(::std::move(ctr), compose(::std::forward<G>(g), ::std::move(f)));
		}
	private:
		using Iterator = iterator_type_t<const Next>;
		struct iterator {
			using iterator_category = ::std::input_iterator_tag;
			using value_type = decltype(*::std::declval<Iterator>());
			using reference = value_type;
			using difference_type = ::std::ptrdiff_t;
			using pointer = void;

			constexpr iterator() {}
			constexpr iterator(iterator &&) = default;
			constexpr iterator(iterator const &) = default;

			constexpr auto operator*() { return ignite(), *u; }
			constexpr auto operator*() const { return const_cast<iterator *>(this)->ignite(), *u; }
			constexpr iterator &operator++() { return ignite(), ++u, *this; }

			constexpr bool operator==(sentinel) const {
				return empty(ctr) && u == adl_or_std::end::call(next);
			}
			constexpr bool operator!=(sentinel) const {
				return ignite(), u != adl_or_std::end::call(next);
			}
		private:
			friend class DiscardPoint;
			constexpr iterator(optional<Ctr> &&ctr, F &&f, Next &&next): ctr(::std::move(ctr)), f(::std::forward<F>(f)), next(::std::move(next)) {
				ctr.reset();
			}
			constexpr iterator(optional<Ctr> const &ctr, F const &f, Next const &next): ctr(ctr), f(f), next(next) {}
			constexpr void ignite() const {
				if(has_value(ctr)) {
					nstd::seq(*ctr);
					const_cast<optional<Ctr> &>(ctr).reset();
					const_cast<Iterator &>(u) = adl_or_std::begin::call(const_cast<Next &>(next) = f());
				}
			}

			optional<Ctr> ctr;
			Iterator u;
			F f;
			Next next;
		};
		optional<Ctr> ctr;
		Next next;
		F f;
	};
	template<class Ctr, class F> DiscardPoint(optional<Ctr> &&, F &&) -> DiscardPoint<Ctr, F>;
	template<class F> DiscardPoint(F &&) -> DiscardPoint<void, F>;

	template<class Ctr, class F> struct Operator {
		constexpr Operator(optional<Ctr> &&ctr, F &&f): ctr(::std::move(ctr)), f(::std::forward<F>(f)) { ctr.reset(); }

		constexpr auto begin() const { return ignite(), adl_or_std::begin::call(then); }
		constexpr auto end() const { return ignite(), adl_or_std::end::call(then); }
	private:
		optional<Ctr> ctr;
		F f;
		using Then = decltype(::std::forward<F>(f)(*ctr));
		Then then;
		constexpr void ignite() const {
			if(has_value(ctr)) const_cast<Then &>(then) = ::std::invoke(const_cast<F &>(f), *ctr), const_cast<optional<Ctr> &>(ctr).reset();
		}
	};
	template<class Ctr, class F> Operator(optional<Ctr> &&, F &&) -> Operator<Ctr, F>;
};

template<class F> struct functor_impl::DiscardPoint<void, F> {
	constexpr DiscardPoint(F &&f): f(::std::forward<F>(f)) {}
	using Ctr = ::std::invoke_result_t<F>;

	auto begin() && { ignite(); return adl_or_std::begin::call(ctr); }
	auto begin() const && { ignite(); return adl_or_std::begin::call(ctr); }
	auto cbegin() const && { ignite(); return adl_or_std::begin::call(ctr); }

	auto begin() & { ignite(); return adl_or_std::begin::call(ctr); }
	auto begin() const & { ignite(); return adl_or_std::begin::call(ctr); }
	auto cbegin() const & { ignite(); return adl_or_std::begin::call(ctr); }

	auto end() && { ignite(); return adl_or_std::end::call(ctr); }
	auto end() const && { ignite(); return adl_or_std::end::call(ctr); }
	auto cend() const && { ignite(); return adl_or_std::end::call(ctr); }

	auto end() & { ignite(); return adl_or_std::end::call(ctr); }
	auto end() const & { ignite(); return adl_or_std::end::call(ctr); }
	auto cend() const & { ignite(); return adl_or_std::end::call(ctr); }
private:
	void ignite() && { has_value(ctr) || (ctr = f()); }
	void ignite() const && { has_value(ctr) || (ctr = f()); }
	void ignite() & { has_value(ctr) || (ctr = f()); }
	void ignite() const & { has_value(ctr) || (const_cast<optional<Ctr> &>(ctr) = f()); }
	optional<Ctr> ctr;
	F f;
};

template<class Ctr> struct functor {
	using This = functor;

	constexpr functor() = default;
	constexpr functor(Ctr &&ctr): ctr(::std::forward<Ctr>(ctr)) {}
	constexpr functor(functor &&) = default;
	//constexpr functor(Ctr const &ctr): ctr(ctr) {}
	constexpr functor(functor const &) = default;

	constexpr functor &operator=(functor &&) = default;
	constexpr functor &operator=(functor const &) = default;

	~functor() {}

	constexpr functor &discard() {
		ctr.reset();
		return *this;
	}
	constexpr functor &seq() {
		if(has_value(ctr)) {
			nstd::seq(*ctr);
			ctr.reset();
		}
		return *this;
	}

#define FLAT_MAP(...) if(has_value(ctr)) return then(__VA_ARGS__); return decltype(then(__VA_ARGS__))()
	// >= performs a flatmap: `ftr >= f' is the same as Haskell's `ftr >>= f'
	template<typename F> constexpr auto operator>=(F &&f) && { FLAT_MAP(flatmap(*::std::move(ctr), ::std::forward<F>(f))); }

	// <= performs a map: `ftr <= f' is the same as Haskell's `do x <- ftr; return $f x' (or simply `fmap f ftr')
	template<typename F> constexpr auto operator<=(F &&f) && { FLAT_MAP(transform(::std::forward<F>(f), *::std::move(ctr))); }

	// > performs a dropmap: `ftr > f' is the same as Haskell's `ftr >> f'
	template<typename F> constexpr auto operator>(F &&f) && { FLAT_MAP(functor_impl::DiscardPoint(::std::move(ctr), ::std::forward<F>(f))); }

	// < performs a full operator: `ftr < f' is the same as Haskell's `f ftr'
	template<typename F> constexpr auto operator<(F &&f) && { FLAT_MAP(functor_impl::Operator(::std::move(ctr), ::std::forward<F>(f))); }
#undef FLAT_MAP

	// , performs an extreme case of dropmap: `ftr , f' causes ftr to be _completely_ abandonned
	template<typename F> constexpr auto operator,(F &&f) && {
		auto retVal{then(functor_impl::DiscardPoint(::std::forward<F>(f)))};
		return discard(), ctr.reset(), retVal;
	}

	constexpr auto begin() noexcept(NOEXCEPT(iterator(ctr))) { return iterator(ctr); }
	constexpr auto begin() const noexcept(NOEXCEPT(const_iterator(ctr))) { return const_iterator(ctr); }
	constexpr auto cbegin() const noexcept(NOEXCEPT(const_iterator(ctr))) { return const_iterator(ctr); }

	constexpr sentinel end() const noexcept { return {}; }
	constexpr sentinel cend() const noexcept { return {}; }
private:
	optional<Ctr> ctr;
	template<typename Next> static constexpr functor<Next> then(Next &&next) noexcept(NOEXCEPT(functor<Next>(::std::forward<Next>(next)))) {
		return functor<Next>(::std::forward<Next>(next));
	}

	template<typename C, typename OptCtr> struct itr {
		constexpr itr() = default;
		constexpr itr(sentinel) {}
		constexpr itr(itr const &) = default;
		constexpr itr(itr &&) = default;

		constexpr itr &operator++() noexcept(noexcept(++here_run)) { return ++here_run, *this; }

		constexpr decltype(auto) operator*() const noexcept(noexcept(*here_run)) { return *here_run; }

		constexpr bool operator==(sentinel) noexcept(noexcept(bool(here_run == here_stop))) { return here_run == here_stop; }

		constexpr bool operator!=(sentinel) const noexcept(noexcept(bool(here_run != here_stop))) { return here_run != here_stop; }
	private:
		friend class functor;
		itr(OptCtr &ctr) noexcept(NOEXCEPT(ctr.flatmap(adl_or_std::begin{})) && NOEXCEPT(here_stop(ctr.flatmap(adl_or_std::end{}))))
			: here_run(ctr.flatmap(adl_or_std::begin{})), here_stop(ctr.flatmap(adl_or_std::end{})) {}

		iterator_type_t<C> here_run;
		end_type_t<C> here_stop;
	};
public:
	using iterator = itr<Ctr, optional<Ctr>>;
	using const_iterator = itr<const Ctr, const optional<Ctr>>;
};
template<class Ctr> functor(Ctr &&) noexcept -> functor<Ctr>;

}

#ifndef SHOW_NOTHING

#include "show.h"

namespace nstd {

SHOW_NSTD_ROOT(1, functor);

}

#endif

#endif
