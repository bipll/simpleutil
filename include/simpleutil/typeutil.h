/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__TYPEUTIL_H_f6c581877933bd35f523f213d32ff570545ff298
#define SIMPLEUTIL__TYPEUTIL_H_f6c581877933bd35f523f213d32ff570545ff298

#include <initializer_list>
#include <type_traits>
#include <utility>
#include <algorithm>
#include <iterator>
#include <limits>
#include <tuple>
#include <array>
#include <functional>
#include <limits>
#include "private_namespace.h"

namespace nstd {

#define PREFIX_TYPE(op, T) decltype(op std::declval<T>())
#define POSTFIX_TYPE(T, op) decltype(std::declval<T>() op)
#define INFIX_TYPE(L, op, R) decltype(std::declval<L>() op (std::declval<R>()))


template<::std::size_t n> using size_constant = ::std::integral_constant<::std::size_t, n>;
// size_constant_v<n> is not defined


using zero_size = size_constant<0>;
using one_size = size_constant<1>;
using max_size = size_constant<::std::numeric_limits<::std::size_t>::max()>;
static constexpr ::std::size_t max_size_v = max_size::value;


struct sfinae {
protected:
	using No = zero_size;
	using Yes = one_size;
	static constexpr auto can(...) -> No;
	template<class T> static constexpr bool is_yes_v = ::std::is_same_v<T, Yes>;
};


template<class T> class has_member_type_type: protected sfinae {
	using sfinae::can;
	template<class U> static constexpr auto can(U &&) -> decltype(std::declval<typename U::type>(), Yes{});
	template<class U, bool> struct impl { using type = U; };
	template<class U> struct impl<U, true> { using type = typename U::type; };
	using U = std::decay_t<T>;
public:
	static constexpr bool value = is_yes_v<decltype(can(std::declval<U>()))>;
	using type = typename impl<U, value>::type;
};
template<class T> using unbox = has_member_type_type<T>;
template<class T> using unbox_t = typename unbox<T>::type;
template<class T> using type_t = typename unbox<T>::type;
template<class T> inline constexpr bool has_member_type_type_v = has_member_type_type<T>::value;


template<typename T> using add_lvalue_const_reference = ::std::add_lvalue_reference<::std::add_const_t<T>>;
template<typename T> using add_lvalue_const_reference_t = type_t<add_lvalue_const_reference<T>>;


template<class T> struct box { using type = T; };
template<auto v> struct value_box { static constexpr auto value = v; };


template<class... Ts> struct mjoin;
template<class T, class U> using mjoin_t = type_t<mjoin<T, U>>;
template<class T, class... Ts> struct mjoin<T, Ts...>: box<T> {};
template<class... Ts> struct mjoin<void, Ts...>: mjoin<Ts...> {};
template<> struct mjoin<>: box<void> {};


#define HAS_MEMBER_TYPE_P(name)																	\
template<class HAS_MEMBER_TYPE_argument> class has_member_type_ ## name: protected sfinae {									\
	using sfinae::can;																	\
	template<class HAS_MEMBER_TYPE_variable> static constexpr auto can(HAS_MEMBER_TYPE_variable &&)								\
		-> decltype(std::declval<typename HAS_MEMBER_TYPE_variable::name>(), Yes{});									\
	template<class HAS_MEMBER_TYPE_variable, bool> struct impl { using type = HAS_MEMBER_TYPE_variable; };							\
	template<class HAS_MEMBER_TYPE_variable> struct impl<HAS_MEMBER_TYPE_variable, true> { using type = typename HAS_MEMBER_TYPE_variable::name; };		\
	using HAS_MEMBER_TYPE_variable = std::decay_t<HAS_MEMBER_TYPE_argument>;										\
public:																				\
	static constexpr bool value = is_yes_v<decltype(can(std::declval<HAS_MEMBER_TYPE_variable>()))>;							\
	using type = type_t<impl<HAS_MEMBER_TYPE_variable, value>>;												\
};																				\
template<class HAS_MEMBER_TYPE_argument> static constexpr bool has_member_type_ ## name ## _v = has_member_type_ ## name<HAS_MEMBER_TYPE_argument>::value;	\
template<class HAS_MEMBER_TYPE_argument> using has_member_type_ ## name ## _t = type_t<has_member_type_ ## name<HAS_MEMBER_TYPE_argument>>

#define SFINAE_U(name, form, formal_params, actual_params)											\
template<formal_params class HAS_NAME_argument> class has_ ## name: protected sfinae {								\
	using sfinae::can;															\
	template<class HAS_NAME_variable> static constexpr auto can(HAS_NAME_variable &&u) -> decltype(form, Yes{});				\
	using HAS_NAME_variable = std::decay_t<HAS_NAME_argument>;										\
public:																		\
	static constexpr bool value = is_yes_v<decltype(can(std::declval<HAS_NAME_variable>()))>;						\
};																		\
template<formal_params class HAS_NAME_argument> inline constexpr bool has_ ## name ## _v = has_ ## name<actual_params HAS_NAME_argument>::value


// This can't be avoided
#define COMMA() ,


#define HAS_MEMBER_P(name) SFINAE_U(member_ ## name, u.name,,)


HAS_MEMBER_P(value);
template<class T> inline constexpr auto value_v = std::enable_if_t<has_member_value<T>::value, T>::value;


template<class T, bool b> inline constexpr bool is_nothrow_return_v = (::std::is_void_v<T> || ::std::is_nothrow_move_constructible_v<T>) && b;
#define NOEXCEPT(...) is_nothrow_return_v<decltype(__VA_ARGS__), noexcept(__VA_ARGS__)>


template<class F> struct arity: max_size {};
template<class F> inline constexpr auto arity_v = value_v<arity<F>>;
template<class R, class... Args> struct arity<R (&)(Args...)>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...)>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...) const>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...) volatile>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...) const volatile>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...) &>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...) &&>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...) const &>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...) const &&>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...) volatile &>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...) volatile &&>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...) const volatile &>: size_constant<sizeof...(Args)> {};
template<class C, class R, class... Args> struct arity<R (C::*)(Args...) const volatile &&>: size_constant<sizeof...(Args)> {};


template<class Sink> struct postable {};

template<class T> class postman;
template<class T> class post_office: sfinae, private_namespace {
	template<class U> friend class postman;
	using sfinae::can;
	template<class U, class Sink> static constexpr auto can_push_back(Sink &&s, U &&u) -> decltype(s.push_back(::std::forward<U>(u)), Yes{});
	static constexpr No can_push_back(...);

	template<class U, class... IOPars> static constexpr auto can_shl(::std::basic_ostream<IOPars...> &&s, U &&u) -> decltype(s << ::std::forward<U>(u), Yes{});
	static constexpr No can_shl(...);

	template<class U, class Sink> static constexpr auto can_insert(Sink &&s, U &&u) -> decltype(s.insert(::std::forward<U>(u)), Yes{});
	static constexpr No can_insert(...);

	template<class U, class Sink> static constexpr auto can_append(Sink &&s, U &&u) -> decltype(s.insert(::std::end(s), ::std::begin(u), ::std::end(u)), Yes{});
	static constexpr No can_append(...);

	template<class U, class Sink> static constexpr auto can_postably_call(Sink &&s, U &&u) -> decltype(postable<Sink>::call(s, ::std::forward<U>(u)), Yes{});
	static constexpr No can_postably_call(...);

	template<class U, class... IOParams> static constexpr auto can_copy(std::basic_ostream<IOParams...> &&s, U &&u)
		-> decltype(std::copy(std::begin(u), std::end(u), std::ostream_iterator<U>(s, "")), Yes{});
	static constexpr No can_copy(...);

	template<class Sink> static constexpr bool can_push_back() noexcept {
		return is_yes_v<decltype(can_push_back(::std::declval<Sink>(), ::std::declval<T>()))>;
	}
	template<class Sink> static constexpr bool can_shl() noexcept { return is_yes_v<decltype(can_shl(::std::declval<Sink>(), ::std::declval<T>()))>; }
	template<class Sink> static constexpr bool can_insert() noexcept {
		return is_yes_v<decltype(can_insert(::std::declval<Sink>(), ::std::declval<T>()))>;
	}
	template<class Sink> static constexpr bool can_append() noexcept {
		return is_yes_v<decltype(can_append(::std::declval<Sink>(), ::std::declval<T>()))>;
	}
	template<class Sink> static constexpr bool can_postably_call() noexcept {
		return is_yes_v<decltype(can_postably_call(::std::declval<Sink>(), ::std::declval<T>()))>;
	}
	template<class Sink> static constexpr bool can_copy() noexcept { return is_yes_v<decltype(can_copy(::std::declval<Sink>(), ::std::declval<T>()))>; }

	template<class Sink> static constexpr bool can_call() noexcept {
		return can_push_back<Sink>() || can_shl<Sink>() || can_insert<Sink>() || can_append<Sink>() || can_postably_call<Sink>() || can_copy<Sink>();
	}
};

using ::std::enable_if;
using ::std::enable_if_t;
using ::std::conditional;
using ::std::conditional_t;

template<class T> struct postman {
	template<class Sink> static constexpr bool can_push_back = post_office<T>::template can_push_back<Sink>();
	template<class Sink> static constexpr bool can_shl = post_office<T>::template can_shl<Sink>();
	template<class Sink> static constexpr bool can_insert = post_office<T>::template can_insert<Sink>();
	template<class Sink> static constexpr bool can_append = post_office<T>::template can_append<Sink>();
	template<class Sink> static constexpr bool can_postably_call = post_office<T>::template can_postably_call<Sink>();
	template<class Sink> static constexpr bool can_copy = post_office<T>::template can_copy<Sink>();

	template<class Sink> static constexpr bool can_call = post_office<T>::template can_call<Sink>();

	template<class Sink> static enable_if_t<post_office<T>::template can_call<Sink>(), Sink &> call(Sink &s, T &&t) {
		if constexpr(post_office<T>::template can_postably_call<Sink>()) return postable<Sink>::call(s, ::std::forward<T>(t)), s;
		else if constexpr(post_office<T>::template can_push_back<Sink>()) return s.push_back(::std::forward<T>(t)), s;
		else if constexpr(post_office<T>::template can_shl<Sink>()) return s << ::std::forward<T>(t);
		else if constexpr(post_office<T>::template can_insert<Sink>()) return s.insert(::std::forward<T>(t)), s;
		else if constexpr(post_office<T>::template can_append<Sink>()) return s.insert(::std::end(s), ::std::begin(t), ::std::end(t)), s;
		else if constexpr(post_office<T>::template can_copy<Sink>()) return ::std::copy(::std::begin(t), ::std::end(t), ::std::ostream_iterator<T>(s, "")), s;
		else return s;
	}
};

template<class Sink, class T> inline constexpr decltype(auto) post(Sink &s, T &&t) { return postman<T>::call(s, ::std::forward<T>(t)); }


namespace comb {

template<class Arg, class...> struct id { using type = Arg; };
template<class... Args> using id_t = type_t<id<Args...>>;


template<template<class...> class... Ops> struct comp;
template<template<class...> class Op> struct comp<Op> { template<class... Args> using type = Op<Args...>; };
template<template<class...> class Op1, template<class...> class... Ops> struct comp<Op1, Ops...> {
	template<class... Args> using type = Op1<typename comp<Ops...>::template type<Args...>>;
};
template<> struct comp<> { template<class... Args> using type = id_t<Args...>; };


template<template<class...> class F> struct flip { template<class A1, class A2, class... As> using type = type_t<F<A2, A1, As...>>; };

}

template<auto... ns> inline constexpr auto min_value_v = ::std::min({ns...});

template<template<class...> class Rank, class T, class... Ts> class min_by_rank {
	using continuation = min_by_rank<Rank, Ts...>;
	static constexpr auto continuation_v = value_v<continuation>;
public:
	using type = ::std::conditional_t<value_v<Rank<T>> < continuation_v, T, type_t<continuation>>;
	static constexpr auto value = ::std::min(value_v<Rank<T>>, continuation_v);
};
template<template<class...> class Rank, class T, class... Ts> using min_by_rank_t = type_t<min_by_rank<Rank, T, Ts...>>;
template<template<class...> class Rank, class T, class... Ts> inline constexpr auto min_by_rank_v = value_v<min_by_rank<Rank, T, Ts...>>;
template<template<class...> class Rank, class T> struct min_by_rank<Rank, T> {
	using type = T;
	static constexpr auto value = value_v<Rank<T>>;
};

template<class... Clauses> struct select {
	using type = type_t<::std::disjunction<has_member_type_type<Clauses>...>>;
	static constexpr auto value = ::std::disjunction_v<has_member_type_type<Clauses>...>;
};
template<class... Clauses> using select_t = type_t<select<Clauses...>>;
template<class... Clauses> static constexpr auto select_v = value_v<select<Clauses...>>;

template<class Left, class Right> struct uncommon_type
	: select<enable_if<::std::is_same_v<::std::common_type_t<Left, Right>, Right>, Left>
		  , enable_if<::std::is_same_v<std::common_type_t<Left, Right>, Left>, Right>
		  , void> {};
template<class Left, class Right> using uncommon_type_t = type_t<uncommon_type<Left, Right>>;


template<class T1, class T2> struct is_similar: ::std::is_same<T1, T2> {};
template<class T1, class T2> inline constexpr auto is_similar_v = value_v<is_similar<T1, T2>>;

template<template<class...> class Root, class... Args1, class... Args2> struct is_similar<Root<Args1...>, Root<Args2...>>: ::std::true_type {};

template<template<auto, class...> class Root, auto n1, class... Args1, auto n2, class... Args2> struct is_similar<Root<n1, Args1...>, Root<n2, Args2...>>
	: ::std::true_type {};
template<template<class, auto, class...> class Root, class Arg11, auto n1, class... Args1, class Arg21, auto n2, class... Args2>
struct is_similar<Root<Arg11, n1, Args1...>, Root<Arg21, n2, Args2...>>
	: ::std::true_type {};
template<template<class, class, auto, class...> class Root, class Arg11, class Arg12, auto n1, class... Args1, class Arg21, class Arg22, auto n2, class... Args2>
struct is_similar<Root<Arg11, Arg12, n1, Args1...>, Root<Arg21, Arg22, n2, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, auto, class...> class Root
	, class Arg11, class Arg12, class Arg13, auto n1, class... Args1, class Arg21, class Arg22, class Arg23, auto n2, class... Args2>
struct is_similar<Root<Arg11, Arg12, Arg13, n1, Args1...>, Root<Arg21, Arg22, Arg23, n2, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, class, auto, class...> class Root
	, class Arg11, class Arg12, class Arg13, class Arg14, auto n1, class... Args1, class Arg21, class Arg22, class Arg23, class Arg24, auto n2, class... Args2>
struct is_similar<Root<Arg11, Arg12, Arg13, Arg14, n1, Args1...>, Root<Arg21, Arg22, Arg23, Arg24, n2, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, class, class, auto, class...> class Root
	, class Arg11, class Arg12, class Arg13, class Arg14, class Arg15, auto n1, class... Args1
	, class Arg21, class Arg22, class Arg23, class Arg24, class Arg25, auto n2, class... Args2>
struct is_similar<Root<Arg11, Arg12, Arg13, Arg14, Arg15, n1, Args1...>, Root<Arg21, Arg22, Arg23, Arg24, Arg25, n2, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, class, class, class, auto, class...> class Root
	, class Arg11, class Arg12, class Arg13, class Arg14, class Arg15, class Arg16, auto n1, class... Args1
	, class Arg21, class Arg22, class Arg23, class Arg24, class Arg25, class Arg26, auto n2, class... Args2>
struct is_similar<Root<Arg11, Arg12, Arg13, Arg14, Arg15, Arg16, n1, Args1...>, Root<Arg21, Arg22, Arg23, Arg24, Arg25, Arg26, n2, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, class, class, class, class, auto, class...> class Root
	, class Arg11, class Arg12, class Arg13, class Arg14, class Arg15, class Arg16, class Arg17, auto n1, class... Args1
	, class Arg21, class Arg22, class Arg23, class Arg24, class Arg25, class Arg26, class Arg27, auto n2, class... Args2>
struct is_similar<Root<Arg11, Arg12, Arg13, Arg14, Arg15, Arg16, Arg17, n1, Args1...>, Root<Arg21, Arg22, Arg23, Arg24, Arg25, Arg26, Arg27, n2, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, class, class, class, class, class, auto, class...> class Root
	, class Arg11, class Arg12, class Arg13, class Arg14, class Arg15, class Arg16, class Arg17, class Arg18, auto n1, class... Args1
	, class Arg21, class Arg22, class Arg23, class Arg24, class Arg25, class Arg26, class Arg27, class Arg28, auto n2, class... Args2>
struct is_similar<Root<Arg11, Arg12, Arg13, Arg14, Arg15, Arg16, Arg17, Arg18, n1, Args1...>
		, Root<Arg21, Arg22, Arg23, Arg24, Arg25, Arg26, Arg27, Arg28, n2, Args2...>>
	: ::std::true_type {};

template<template<auto, auto, class...> class Root
		, auto n11, auto n12, class... Args1
		, auto n21, auto n22, class... Args2>
struct is_similar<
		Root<n11, n12, Args1...>
		, Root<n21, n22, Args2...>>
	: ::std::true_type {};
template<template<auto, class, auto, class...> class Root
		, auto n11, class Arg11, auto n12, class... Args1
		, auto n21, class Arg21, auto n22, class... Args2>
struct is_similar<
		Root<n11, Arg11, n12, Args1...>
		, Root<n21, Arg21, n22, Args2...>>
	: ::std::true_type {};
template<template<auto, class, class, auto, class...> class Root
		, auto n11, class Arg11, class Arg12, auto n12, class... Args1
		, auto n21, class Arg21, class Arg22, auto n22, class... Args2>
struct is_similar<
		Root<n11, Arg11, Arg12, n12, Args1...>
		, Root<n21, Arg21, Arg22, n22, Args2...>>
	: ::std::true_type {};
template<template<auto, class, class, class, auto, class...> class Root
		, auto n11, class Arg11, class Arg12, class Arg13, auto n12, class... Args1
		, auto n21, class Arg21, class Arg22, class Arg23, auto n22, class... Args2>
struct is_similar<
		Root<n11, Arg11, Arg12, Arg13, n12, Args1...>
		, Root<n21, Arg21, Arg22, Arg23, n22, Args2...>>
	: ::std::true_type {};
template<template<auto, class, class, class, class, auto, class...> class Root
		, auto n11, class Arg11, class Arg12, class Arg13, class Arg14, auto n12, class... Args1
		, auto n21, class Arg21, class Arg22, class Arg23, class Arg24, auto n22, class... Args2>
struct is_similar<
		Root<n11, Arg11, Arg12, Arg13, Arg14, n12, Args1...>
		, Root<n21, Arg21, Arg22, Arg23, Arg24, n22, Args2...>>
	: ::std::true_type {};

template<template<class, auto, auto, class...> class Root
		, class Arg11, auto n11, auto n12, class... Args1
		, class Arg21, auto n21, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, n11, n12, Args1...>
		, Root<Arg21, n21, n22, Args2...>>
	: ::std::true_type {};
template<template<class, auto, class, auto, class...> class Root
		, class Arg11, auto n11, class Arg12, auto n12, class... Args1
		, class Arg21, auto n21, class Arg22, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, n11, Arg12, n12, Args1...>
		, Root<Arg21, n21, Arg22, n22, Args2...>>
	: ::std::true_type {};
template<template<class, auto, class, class, auto, class...> class Root
		, class Arg11, auto n11, class Arg12, class Arg13, auto n12, class... Args1
		, class Arg21, auto n21, class Arg22, class Arg23, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, n11, Arg12, Arg13, n12, Args1...>
		, Root<Arg21, n21, Arg22, Arg23, n22, Args2...>>
	: ::std::true_type {};
template<template<class, auto, class, class, class, auto, class...> class Root
		, class Arg11, auto n11, class Arg12, class Arg13, class Arg14, auto n12, class... Args1
		, class Arg21, auto n21, class Arg22, class Arg23, class Arg24, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, n11, Arg12, Arg13, Arg14, n12, Args1...>
		, Root<Arg21, n21, Arg22, Arg23, Arg24, n22, Args2...>>
	: ::std::true_type {};
template<template<class, auto, class, class, class, class, auto, class...> class Root
		, class Arg11, auto n11, class Arg12, class Arg13, class Arg14, class Arg15, auto n12, class... Args1
		, class Arg21, auto n21, class Arg22, class Arg23, class Arg24, class Arg25, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, n11, Arg12, Arg13, Arg14, Arg15, n12, Args1...>
		, Root<Arg21, n21, Arg22, Arg23, Arg24, Arg25, n22, Args2...>>
	: ::std::true_type {};

template<template<class, class, auto, auto, class...> class Root
		, class Arg11, class Arg12, auto n11, auto n12, class... Args1
		, class Arg21, class Arg22, auto n21, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, n11, n12, Args1...>
		, Root<Arg21, Arg22, n21, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, auto, class, auto, class...> class Root
		, class Arg11, class Arg12, auto n11, class Arg13, auto n12, class... Args1
		, class Arg21, class Arg22, auto n21, class Arg23, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, n11, Arg13, n12, Args1...>
		, Root<Arg21, Arg22, n21, Arg23, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, auto, class, class, auto, class...> class Root
		, class Arg11, class Arg12, auto n11, class Arg13, class Arg14, auto n12, class... Args1
		, class Arg21, class Arg22, auto n21, class Arg23, class Arg24, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, n11, Arg13, Arg14, n12, Args1...>
		, Root<Arg21, Arg22, n21, Arg23, Arg24, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, auto, class, class, class, auto, class...> class Root
		, class Arg11, class Arg12, auto n11, class Arg13, class Arg14, class Arg15, auto n12, class... Args1
		, class Arg21, class Arg22, auto n21, class Arg23, class Arg24, class Arg25, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, n11, Arg13, Arg14, Arg15, n12, Args1...>
		, Root<Arg21, Arg22, n21, Arg23, Arg24, Arg25, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, auto, class, class, class, class, auto, class...> class Root
		, class Arg11, class Arg12, auto n11, class Arg13, class Arg14, class Arg15, class Arg16, auto n12, class... Args1
		, class Arg21, class Arg22, auto n21, class Arg23, class Arg24, class Arg25, class Arg26, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, n11, Arg13, Arg14, Arg15, Arg16, n12, Args1...>
		, Root<Arg21, Arg22, n21, Arg23, Arg24, Arg25, Arg26, n22, Args2...>>
	: ::std::true_type {};

template<template<class, class, class, auto, auto, class...> class Root
		, class Arg11, class Arg12, class Arg13, auto n11, auto n12, class... Args1
		, class Arg21, class Arg22, class Arg23, auto n21, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, Arg13, n11, n12, Args1...>
		, Root<Arg21, Arg22, Arg23, n21, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, auto, class, auto, class...> class Root
		, class Arg11, class Arg12, class Arg13, auto n11, class Arg14, auto n12, class... Args1
		, class Arg21, class Arg22, class Arg23, auto n21, class Arg24, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, Arg13, n11, Arg14, n12, Args1...>
		, Root<Arg21, Arg22, Arg23, n21, Arg24, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, auto, class, class, auto, class...> class Root
		, class Arg11, class Arg12, class Arg13, auto n11, class Arg14, class Arg15, auto n12, class... Args1
		, class Arg21, class Arg22, class Arg23, auto n21, class Arg24, class Arg25, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, Arg13, n11, Arg14, Arg15, n12, Args1...>
		, Root<Arg21, Arg22, Arg23, n21, Arg24, Arg25, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, auto, class, class, class, auto, class...> class Root
		, class Arg11, class Arg12, class Arg13, auto n11, class Arg14, class Arg15, class Arg16, auto n12, class... Args1
		, class Arg21, class Arg22, class Arg23, auto n21, class Arg24, class Arg25, class Arg26, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, Arg13, n11, Arg14, Arg15, Arg16, n12, Args1...>
		, Root<Arg21, Arg22, Arg23, n21, Arg24, Arg25, Arg26, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, auto, class, class, class, class, auto, class...> class Root
		, class Arg11, class Arg12, class Arg13, auto n11, class Arg14, class Arg15, class Arg16, class Arg17, auto n12, class... Args1
		, class Arg21, class Arg22, class Arg23, auto n21, class Arg24, class Arg25, class Arg26, class Arg27, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, Arg13, n11, Arg14, Arg15, Arg16, Arg17, n12, Args1...>
		, Root<Arg21, Arg22, Arg23, n21, Arg24, Arg25, Arg26, Arg27, n22, Args2...>>
	: ::std::true_type {};

template<template<class, class, class, class, auto, auto, class...> class Root
		, class Arg11, class Arg12, class Arg13, class Arg14, auto n11, auto n12, class... Args1
		, class Arg21, class Arg22, class Arg23, class Arg24, auto n21, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, Arg13, Arg14, n11, n12, Args1...>
		, Root<Arg21, Arg22, Arg23, Arg24, n21, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, class, auto, class, auto, class...> class Root
		, class Arg11, class Arg12, class Arg13, class Arg14, auto n11, class Arg15, auto n12, class... Args1
		, class Arg21, class Arg22, class Arg23, class Arg24, auto n21, class Arg25, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, Arg13, Arg14, n11, Arg15, n12, Args1...>
		, Root<Arg21, Arg22, Arg23, Arg24, n21, Arg25, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, class, auto, class, class, auto, class...> class Root
		, class Arg11, class Arg12, class Arg13, class Arg14, auto n11, class Arg15, class Arg16, auto n12, class... Args1
		, class Arg21, class Arg22, class Arg23, class Arg24, auto n21, class Arg25, class Arg26, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, Arg13, Arg14, n11, Arg15, Arg16, n12, Args1...>
		, Root<Arg21, Arg22, Arg23, Arg24, n21, Arg25, Arg26, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, class, auto, class, class, class, auto, class...> class Root
		, class Arg11, class Arg12, class Arg13, class Arg14, auto n11, class Arg15, class Arg16, class Arg17, auto n12, class... Args1
		, class Arg21, class Arg22, class Arg23, class Arg24, auto n21, class Arg25, class Arg26, class Arg27, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, Arg13, Arg14, n11, Arg15, Arg16, Arg17, n12, Args1...>
		, Root<Arg21, Arg22, Arg23, Arg24, n21, Arg25, Arg26, Arg27, n22, Args2...>>
	: ::std::true_type {};
template<template<class, class, class, class, auto, class, class, class, class, auto, class...> class Root
		, class Arg11, class Arg12, class Arg13, class Arg14, auto n11, class Arg15, class Arg16, class Arg17, class Arg18, auto n12, class... Args1
		, class Arg21, class Arg22, class Arg23, class Arg24, auto n21, class Arg25, class Arg26, class Arg27, class Arg28, auto n22, class... Args2>
struct is_similar<
		Root<Arg11, Arg12, Arg13, Arg14, n11, Arg15, Arg16, Arg17, Arg18, n12, Args1...>
		, Root<Arg21, Arg22, Arg23, Arg24, n21, Arg25, Arg26, Arg27, Arg28, n22, Args2...>>
	: ::std::true_type {};


template<class T, class... Ts> struct like_one_of;
template<class T, class... Ts> inline constexpr auto like_one_of_v = value_v<like_one_of<T, Ts...>>;
template<class T, class T1, class... Ts> struct like_one_of<T, T1, Ts...>: ::std::disjunction<is_similar<T, T1>, like_one_of<T, Ts...>> {};
template<class T> struct like_one_of<T>: ::std::false_type {};


template<::std::size_t, class> struct index_cons;
template<::std::size_t h, class T> using index_cons_t = type_t<index_cons<h, T>>;
template<::std::size_t i, ::std::size_t... is> struct index_cons<i, ::std::index_sequence<is...>>: box<::std::index_sequence<i, is...>> {};

using index_nil = box<::std::index_sequence<>>;
using index_nil_t = ::std::index_sequence<>;

template<::std::size_t i, ::std::size_t j> struct make_boxed_sequence: index_cons<i, typename make_boxed_sequence<i + 1, j>::type> {};
template<::std::size_t i, ::std::size_t j> using make_index_sequence = type_t<make_boxed_sequence<i, j>>;
template<::std::size_t i> struct make_boxed_sequence<i, i>: index_nil {};

template<class F, class... Args> using function_object = ::std::conditional<::std::is_default_constructible_v<::std::decay_t<F>>
										, F, ::std::function<::std::invoke_result_t<F, Args...>(Args...)>>;
template<class F, class... Args> using function_object_t = type_t<function_object<F, Args...>>;

}

#include "__typeutil/pack.h"

namespace nstd {

struct with_few_impl: private_namespace {
	template<::std::size_t... is, class F, class... Args> static constexpr decltype(auto) call(::std::index_sequence<is...>, F &&f, Args &&...args)
		noexcept(noexcept(::std::invoke(::std::forward<F>(f), pack::get<is>(::std::forward<Args>(args)...)...)))
	{
		return ::std::invoke(::std::forward<F>(f), pack::get<is>(::std::forward<Args>(args)...)...);
	}

	template<::std::size_t n, class F, class... Args> friend constexpr decltype(auto) with_few_first(F &&f, Args &&...args)
		noexcept(noexcept(with_few_impl::call(::std::make_index_sequence<n>{}, ::std::forward<F>(f), ::std::forward<Args>(args)...)));

	template<::std::size_t n, class F, class... Args> friend constexpr decltype(auto) with_few_last(F &&f, Args &&...args)
		noexcept(noexcept(with_few_impl::call(make_index_sequence<n, sizeof...(Args)>{}, ::std::forward<F>(f), ::std::forward<Args>(args)...)));
};

template<::std::size_t n, class F, class... Args> inline constexpr decltype(auto) with_few_first(F &&f, Args &&...args)
	noexcept(noexcept(with_few_impl::call(::std::make_index_sequence<n>{}, ::std::forward<F>(f), ::std::forward<Args>(args)...)))
{
	return with_few_impl::call(::std::make_index_sequence<n>{}, ::std::forward<F>(f), ::std::forward<Args>(args)...);
}

template<::std::size_t n, class F, class... Args> inline constexpr decltype(auto) with_few_last(F &&f, Args &&...args)
	noexcept(noexcept(with_few_impl::call(make_index_sequence<n, sizeof...(Args)>{}, ::std::forward<F>(f), ::std::forward<Args>(args)...)))
{
	return with_few_impl::call(make_index_sequence<n, sizeof...(Args)>{}, ::std::forward<F>(f), ::std::forward<Args>(args)...);
}


template<class... Ts> inline constexpr void ignore(Ts &&...) noexcept {}


template<class T, class... Ts> inline constexpr T return_first(T &&retVal, Ts &&...) noexcept(::std::is_nothrow_move_constructible_v<T>) {
	return ::std::forward<T>(retVal);
}


template<class T, class U = ::std::remove_reference_t<T>> using is_nothrow_assignable = std::disjunction<::std::is_nothrow_assignable<T, U>
												, ::std::is_nothrow_swappable_with<T, U>
												, ::std::is_nothrow_assignable<T, ::std::add_lvalue_reference_t<U>>>;
template<class T, class U = ::std::remove_reference_t<T>> inline constexpr auto is_nothrow_assignable_v = value_v<is_nothrow_assignable<T, U>>;


template<class...> struct are_nothrow_assignable: ::std::true_type {};
template<class... Ts> inline constexpr auto are_nothrow_assignable_v = value_v<are_nothrow_assignable<Ts...>>;
template<class T, class U, class... Ts> struct are_nothrow_assignable<T, U, Ts...>: ::std::conjunction<is_nothrow_assignable<T, U>, are_nothrow_assignable<Ts...>> {};


template<class... Ts> using are_nothrow_move_assignable = ::std::conjunction<::std::is_nothrow_move_assignable<Ts>...>;
template<class... Ts> inline constexpr auto are_nothrow_move_assignable_v = value_v<are_nothrow_move_assignable<Ts...>>;


template<class T, class U> inline constexpr void value_assign(T &sink, U &&source) noexcept(is_nothrow_assignable_v<T, U>) {
	using ::std::swap;
	if constexpr(::std::is_nothrow_assignable_v<T, U>) sink = ::std::forward<U>(source);
	else if constexpr(::std::is_nothrow_swappable_with_v<T, U>) swap(sink, ::std::forward<U>(source));
	else if constexpr(::std::is_nothrow_assignable_v<T, ::std::add_lvalue_reference_t<U>>) sink = source;
	else if constexpr(::std::is_assignable_v<T, U>) sink = ::std::forward<U>(source);
	else if constexpr(::std::is_swappable_with_v<T, U>) swap(sink, ::std::forward<U>(source));
	else if constexpr(::std::is_assignable_v<T, ::std::add_lvalue_reference_t<U>>) sink = source;
}

template<class T, class U, class... Ts> inline constexpr void value_assign(T &sink, U &&source, Ts &&...others) noexcept(are_nothrow_assignable_v<T, U, Ts...>) {
	if constexpr(is_nothrow_assignable_v<T>) value_assign(::std::forward<Ts>(others)...), value_assign(sink, ::std::forward<U>(source));
	else value_assign(sink, ::std::move(source)), value_assign(::std::forward<Ts>(others)...);
}


inline constexpr void value_swap() noexcept {}
template<class T, class T1, class... Ts> inline constexpr void value_swap(T &left, T1 &right, Ts &&...others)
	noexcept(pack::all_v<::std::is_nothrow_swappable, ::std::remove_reference_t<T>, pack::odds_t<::std::remove_reference_t<Ts>...>>)
{
	using ::std::swap;
	if constexpr(::std::is_nothrow_swappable_v<::std::remove_reference_t<T>>) value_swap(::std::forward<Ts>(others)...), swap(left, right);
	else swap(left, right), value_swap(::std::forward<Ts>(others)...);
}

}

#endif
