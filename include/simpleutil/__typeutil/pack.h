/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__PACK_H_c7fad3b32ef04a9f5c6f5c1801c93a0276d84be7
#define SIMPLEUTIL__PACK_H_c7fad3b32ef04a9f5c6f5c1801c93a0276d84be7

#include <tuple>
#include <array>
#include <utility>
#include <type_traits>

namespace nstd::pack {

template<class IntSeq> struct integer_split;
template<class IntSeq> using integer_split_t = typename integer_split<IntSeq>::type;

template<class T, T... ts> struct integer_split<::std::integer_sequence<T, ts...>> { using type = ::std::tuple<::std::integral_constant<T, ts>...>; };


template<template<class...> class Op, class A, class... As> struct reduce { using type = A; };
template<template<class...> class Op, class A, class... As> using reduce_t = nstd::type_t<reduce<Op, A, As...>>;
template<template<class...> class Op, class A1, class A2, class... As> struct reduce<Op, A1, A2, As...> { using type = reduce_t<Op, type_t<Op<A1, A2>>, As...>; };


template<class H, class T> struct cons: box<::std::pair<H, T>> {};
template<class H, class T> using cons_t = type_t<cons<H, T>>;


template<class... Elems> class list;
using nil = list<>;
template<class Elem> using singleton = list<Elem>;
template<class H, class... Ts> struct cons<H, list<Ts...>>: box<list<H, Ts...>> {};


template<class... Ls> struct concat;
template<class... Ls> using concat_t = typename concat<Ls...>::type;
template<class... First, class... Second, class... Rest> struct concat<list<First...>, list<Second...>, Rest...>: concat<list<First..., Second...>, Rest...> {};
template<class List> struct concat<List>: box<List> {};
template<> struct concat<>: box<nil> {};


template<class L> using null = ::std::is_same<L, nil>;
template<class L> inline constexpr auto null_v = value_v<null<L>>;
template<class L> using not_null = std::negation<::std::is_same<L, nil>>;
template<class L> inline constexpr auto not_null_v = value_v<not_null<L>>;


template<class L> struct size;
template<class L> inline constexpr auto size_v = value_v<size<L>>;
template<class... Ts> struct size<list<Ts...>>: size_constant<sizeof...(Ts)> {};


template<::std::size_t i, class L> struct drop: drop<i / 2, typename drop<i - i / 2, L>::type> {};
template<::std::size_t i, class L> using drop_t = typename drop<i, L>::type;
template<class L> using cdr = drop<1, L>;
template<class L> using cdr_t = type_t<cdr<L>>;
template<class L> using tail = type_t<cdr<L>>;
template<class L> struct drop<0, L>: box<L> {};

template<::std::size_t i> struct drop<i, nil>: box<nil> {};
template<> struct drop<0, nil>: box<nil> {};
template<class H0, class... Ts> struct drop<1, list<H0, Ts...>>: box<list<Ts...>> {};

template<class H, class T> struct drop<1, ::std::pair<H, T>>: box<list<T>> {};
template<class H, class... Ts> struct drop<1, ::std::tuple<H, Ts...>>: box<list<Ts...>> {};
template<> struct drop<1, ::std::tuple<>>: box<nil> {};


template<class L> struct car;
template<class L> using head = typename car<L>::type;
template<class L> using car_t = typename car<L>::type;
template<class H, class... Ts> struct car<list<H, Ts...>>: box<H> {};
template<class H, class T> struct car<::std::pair<H, T>>: box<H> {};
template<class H, class... Ts> struct car<::std::tuple<H, Ts...>>: box<H> {};
template<class T, ::std::size_t n> struct car<::std::array<T, n>>: ::std::enable_if<(n > 0), T> {};


template<::std::size_t i, class L> using element = ::std::enable_if<i < size_v<L>, car_t<drop_t<i, L>>>;
template<::std::size_t i, class L> using element_t = type_t<element<i, L>>;


template<::std::size_t i, class T, class... Ts> element_t<i, list<T, Ts...>> &&get(T &&t, Ts &&...ts) {
	if constexpr(i == 0) return ::std::forward<T>(t);
	else return pack::get<i - 1>(::std::forward<Ts>(ts)...);
}


template<class... Ts> using last = element<sizeof...(Ts) - 1, list<Ts...>>;
template<class... Ts> using last_t = type_t<last<Ts...>>;


template<class...> struct splice;
template<class... Ts> using splice_t = type_t<splice<Ts...>>;
template<class H, class... Ts> struct splice<H, Ts...>: splice<list<H>, Ts...> {};
template<class... Hs, class... Ts> struct splice<list<Hs...>, Ts...>: concat<list<Hs...>, splice_t<Ts...>> {};
template<class... Hs, class... Ts> struct splice<::std::tuple<Hs...>, Ts...>: concat<list<Hs...>, splice_t<Ts...>> {};
template<class... Hs, class... Ts> struct splice<::std::pair<Hs...>, Ts...>: concat<list<Hs...>, splice_t<Ts...>> {};
template<> struct splice<>: box<nil> {};


template<template<class...> class F, class... Ts> struct apply: apply<F, splice_t<Ts...>> {};
template<template<class...> class F, class... Ts> using apply_t = type_t<apply<F, Ts...>>;
template<template<class...> class F, class... Ts> inline constexpr auto apply_v = value_v<apply<F, Ts...>>;
template<template<class...> class F, class... Args> struct apply<F, list<Args...>> { using type = F<Args...>; };


template<class... Ts> using index_sequence_for = ::std::make_index_sequence<size_v<splice_t<Ts...>>>;


template<template<class...> class F, class... Args> struct bind { template<class... Brgs> using type = F<Args..., Brgs...>; };


template<template<class...> class F, class... Ls> struct map: map<F, splice_t<Ls...>> {};
template<template<class...> class F, class... Ls> using map_t = type_t<map<F, Ls...>>;
template<template<class...> class F, class... Ts> struct map<F, list<Ts...>>: box<list<F<Ts>...>> {};


template<template<class...> class F, class... Args> struct all: all<F, splice_t<Args...>> {};
template<template<class...> class F, class... Args> inline constexpr auto all_v = value_v<all<F, Args...>>;
template<template<class...> class F, class... Args> using all_t = type_t<all<F, Args...>>;
template<template<class...> class F, class... Args> struct all<F, list<Args...>>: ::std::conjunction<F<Args>...> {};

template<template<class...> class F, class... Args> struct any: any<F, splice_t<Args...>> {};
template<template<class...> class F, class... Args> inline constexpr auto any_v = value_v<any<F, Args...>>;
template<template<class...> class F, class... Args> using any_t = type_t<any<F, Args...>>;
template<template<class...> class F, class... Args> struct any<F, list<Args...>>: ::std::disjunction<F<Args>...> {};


template<class T, class... Ts> using elem = any<bind<::std::is_same, T>::template type, Ts...>;
template<class T, class... Ts> inline constexpr auto elem_v = value_v<elem<T, Ts...>>;


template<::std::size_t n, class T> struct replicate: concat<typename replicate<n / 2, T>::type, typename replicate<n - n / 2, T>::type> {};
template<::std::size_t n, class T> using replicate_t = type_t<replicate<n, T>>;
template<class T> struct replicate<0, T>: box<nil> {};
template<class T> struct replicate<1, T>: box<list<T>> {};


template<class T, ::std::size_t n> struct drop<1, ::std::array<T, n>>: replicate<n - 1, T> {};
template<class T> struct drop<1, ::std::array<T, 0>>: box<nil> {};
template<class T, ::std::size_t n, class... Ts> struct splice<::std::array<T, n>, Ts...>: concat<replicate_t<n, T>, splice_t<Ts...>> {};


template<class F, class... Args> struct longest_addendum;
template<class F, class... Args> using longest_addendum_t = type_t<longest_addendum<F, Args...>>;
template<template<class...> class F, class... Accepted, class H, class... Ts> struct longest_addendum<F<Accepted...>, H, Ts...>
	: ::std::conditional<value_v<F<Accepted..., H>>, longest_addendum_t<F<Accepted..., H>, Ts...>, ::std::pair<list<Accepted...>, list<H, Ts...>>> {};
template<template<class...> class F, class... Accepted> class longest_addendum<F<Accepted...>>: box<::std::pair<list<Accepted...>, nil>> {};


template<class...> struct odds;
template<class... Ts> using odds_t = type_t<odds<Ts...>>;
template<class T1, class T2, class... Ts> struct odds<T1, T2, Ts...>: cons<T1, odds_t<Ts...>> {};
template<class T> struct odds<T>: box<list<T>> {};
template<> struct odds<>: box<nil> {};

template<class...> struct evens;
template<class... Ts> using evens_t = type_t<evens<Ts...>>;
template<class T1, class T2, class... Ts> struct evens<T1, T2, Ts...>: cons<T2, evens_t<Ts...>> {};
template<class T> struct evens<T>: box<nil> {};
template<> struct evens<>: box<nil> {};

}

#endif
