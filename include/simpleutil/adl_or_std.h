/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__ADL_OR_STD_H_a9a35e676f712ec6625dc5edb1b68d57008c9a9f
#define SIMPLEUTIL__ADL_OR_STD_H_a9a35e676f712ec6625dc5edb1b68d57008c9a9f

#define ADL_OR_STD_IMPL(name)																	\
namespace adl_or_std {																		\
																				\
class name ## _member_impl: sfinae, private_namespace {														\
	template<::std::size_t> friend class name ## _impl;													\
	friend class name;																	\
																				\
	using sfinae::can;																	\
	template<class T> static constexpr auto can(T &&t) noexcept -> decltype(t.name(), Yes{});								\
																				\
	template<class T> static constexpr ::std::size_t value = is_yes_v<decltype(can(::std::declval<T>()))>? 4 : 0;						\
};																				\
class name ## _std_impl: sfinae, private_namespace {														\
	template<::std::size_t> friend class name ## _impl;													\
	friend class name;																	\
																				\
	using sfinae::can;																	\
	template<class T> static constexpr auto can(T &&t) noexcept -> decltype(::std::name(t), Yes{});								\
																				\
	template<class T> static constexpr ::std::size_t value = is_yes_v<decltype(can(::std::declval<T>()))>? 2 : 0;						\
};																				\
class name ## _adl_impl: sfinae, private_namespace {														\
	template<::std::size_t> friend class name ## _impl;													\
	friend class name;																	\
																				\
	using sfinae::can;																	\
	template<class T> static constexpr auto can(T &&t) noexcept -> decltype(name(t), Yes{});								\
																				\
	template<class T> static constexpr ::std::size_t value = is_yes_v<decltype(can(::std::declval<T>()))>;							\
};																				\
																				\
template<::std::size_t> class name ## _impl {};															\
																				\
template<> class name ## _impl<7>: private_namespace {														\
	friend class name;																	\
	template<class T> static constexpr bool is_noexcept() noexcept {											\
		return noexcept(::std::declval<T>().name()) && ::std::is_nothrow_move_constructible_v<T>;							\
	}																			\
	template<class T> static constexpr auto call(T &&t) noexcept(noexcept(::std::forward<T>(t).name())) { return ::std::forward<T>(t).name(); }		\
};																				\
																				\
template<> class name ## _impl<6>: private_namespace {														\
	friend class name;																	\
	template<class T> static constexpr bool is_noexcept() noexcept {											\
		return noexcept(::std::declval<T>().name()) && ::std::is_nothrow_move_constructible_v<T>;							\
	}																			\
	template<class T> static constexpr auto call(T &&t) noexcept(noexcept(::std::forward<T>(t).name())) { return ::std::forward<T>(t).name(); }		\
};																				\
																				\
template<> class name ## _impl<5>: private_namespace {														\
	friend class name;																	\
	template<class T> static constexpr bool is_noexcept() noexcept {											\
		return noexcept(::std::declval<T>().name()) && ::std::is_nothrow_move_constructible_v<T>;							\
	}																			\
	template<class T> static constexpr auto call(T &&t) noexcept(noexcept(::std::forward<T>(t).name())) { return ::std::forward<T>(t).name(); }		\
};																				\
																				\
template<> class name ## _impl<4>: private_namespace {														\
	friend class name;																	\
	template<class T> static constexpr bool is_noexcept() noexcept {											\
		return noexcept(::std::declval<T>().name()) && ::std::is_nothrow_move_constructible_v<T>;							\
	}																			\
	template<class T> static constexpr auto call(T &&t) noexcept(noexcept(::std::forward<T>(t).name())) { return ::std::forward<T>(t).name(); }		\
};																				\
																				\
template<> class name ## _impl<3>: private_namespace {														\
	friend class name;																	\
	template<class T> static constexpr bool is_noexcept() noexcept {											\
		return noexcept(::std::name(::std::declval<T>())) && ::std::is_nothrow_move_constructible_v<T>;							\
	}																			\
	template<class T> static constexpr auto call(T &&t) noexcept(noexcept(::std::name(::std::forward<T>(t)))) { return ::std::name(::std::forward<T>(t)); }	\
};																				\
																				\
template<> class name ## _impl<2>: private_namespace {														\
	friend class name;																	\
	template<class T> static constexpr bool is_noexcept() noexcept {											\
		return noexcept(::std::name(::std::declval<T>())) && ::std::is_nothrow_move_constructible_v<T>;							\
	}																			\
	template<class T> static constexpr auto call(T &&t) noexcept(noexcept(::std::name(::std::forward<T>(t)))) { return ::std::name(::std::forward<T>(t)); }	\
};																				\
																				\
template<> class name ## _impl<1>: private_namespace {														\
	friend class name;																	\
	template<class T> static constexpr bool is_noexcept() noexcept {											\
		return noexcept(name(::std::declval<T>())) && ::std::is_nothrow_move_constructible_v<T>;							\
       	}																			\
	template<class T> static constexpr auto call(T &&t) noexcept(noexcept(name(::std::forward<T>(t)))) { return name(::std::forward<T>(t)); }		\
};																				\
																				\
struct name {																			\
	template<class T> using adl_or_std_impl = name ## _impl<name ## _member_impl::value<T>									\
										+ name ## _std_impl::value<T>							\
										+ name ## _adl_impl::value<T>>;							\
public:																				\
	template<class T> static constexpr decltype(adl_or_std_impl<T>::template is_noexcept<T>()) is_noexcept() noexcept {					\
		return adl_or_std_impl<T>::template is_noexcept<T>();												\
	}																			\
	template<class T> static constexpr decltype(adl_or_std_impl<T>::call(::std::declval<T>())) call(T &&t) noexcept(adl_or_std_impl<T>::template is_noexcept<T>()) {\
		return adl_or_std_impl<T>::call(::std::forward<T>(t));												\
	}																			\
	template<class T> constexpr decltype(adl_or_std_impl<T>::call(::std::declval<T>())) operator()(T &&t) noexcept(adl_or_std_impl<T>::template is_noexcept<T>()) {\
		return call(::std::forward<T>(t));														\
	}																			\
																				\
	template<class T> static constexpr bool value = name ## _member_impl::value<T>										\
							+ name ## _std_impl::value<T>										\
							+ name ## _adl_impl::value<T>;										\
	template<class T> using type = decltype(call(::std::declval<T>()));											\
};																				\
																				\
}

#ifdef SHOW_NOTHING
#	define ADL_OR_STD(name) ADL_OR_STD_IMPL(name)
#else
#	define ADL_OR_STD(name) ADL_OR_STD_IMPL(name); SHOW_NSTD_SIMPLE(adl_or_std::name)
#endif

#endif
