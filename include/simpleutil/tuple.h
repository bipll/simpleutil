/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__TUPLE_H_376bf119ccb39719cf320b5acea3b8d8abbfc2e7
#define SIMPLEUTIL__TUPLE_H_376bf119ccb39719cf320b5acea3b8d8abbfc2e7

#include <type_traits>
#include <utility>
#include <tuple>
#include <array>
#include <variant>
#include "private_namespace.h"
#include "typeutil.h"

namespace nstd {

template<class T> using has_std_tuple_size = has_member_value<::std::tuple_size<T>>;
template<class T> inline constexpr auto has_std_tuple_size_v = value_v<has_std_tuple_size<T>>;


template<class T> struct is_tuple: has_std_tuple_size<T> {};
template<class T> inline constexpr auto is_tuple_v = value_v<is_tuple<T>>;


template<class... Ts> struct tuple_size: size_constant<(tuple_size<Ts>::value + ...)> {};
template<class... Ts> inline constexpr auto tuple_size_v = value_v<tuple_size<Ts...>>;

template<class T> struct tuple_size<T>: ::std::conditional_t<has_std_tuple_size_v<T>, ::std::tuple_size<T>, one_size> {};
template<class T> struct tuple_size<const T>: tuple_size<T> {};
template<class T> struct tuple_size<volatile T>: tuple_size<T> {};
template<class T> struct tuple_size<const volatile T>: tuple_size<T> {};
template<class T> struct tuple_size<T &>: tuple_size<T> {};
template<class T> struct tuple_size<T &&>: tuple_size<T> {};


template<::std::size_t i, class... Ts> struct tuple_element;
template<::std::size_t i, class... Ts> using tuple_element_t = typename tuple_element<i, Ts...>::type;
template<::std::size_t i, class T, class... Ts> struct tuple_element<i, T, Ts...>
	: ::std::conditional_t<(i < tuple_size_v<T>), tuple_element<i, T>, tuple_element<i - tuple_size_v<T>, Ts...>> {};

template<::std::size_t i, class T> struct tuple_element<i, T>: ::std::conditional_t<has_std_tuple_size_v<T>, ::std::tuple_element<i, T>, ::std::enable_if<i == 0, T>> {};
template<::std::size_t i, class T> struct tuple_element<i, const T>: tuple_element<i, T> {};
template<::std::size_t i, class T> struct tuple_element<i, volatile T>: tuple_element<i, T> {};
template<::std::size_t i, class T> struct tuple_element<i, const volatile T>: tuple_element<i, T> {};
template<::std::size_t i, class T> struct tuple_element<i, T &>: tuple_element<i, T> {};
template<::std::size_t i, class T> struct tuple_element<i, T &&>: tuple_element<i, T> {};


template<class...> struct tuple {
	using This = tuple;

	constexpr tuple() = default;
	constexpr tuple(This &&) = default;
	constexpr tuple(This const &) = default;

	constexpr tuple &operator=(This &&) = default;
	constexpr tuple &operator=(This const &) = default;

	constexpr void swap(This) {}
};

template<class... Ts> struct is_tuple<tuple<Ts...>>: ::std::true_type {};
template<class... Ts> struct tuple_size<tuple<Ts...>>: size_constant<sizeof...(Ts)> {};
template<::std::size_t i, class... Ts> struct tuple_element<i, tuple<Ts...>>: pack::element_t<i, pack::list<Ts...>> {};

template<class H, class... Ts> class tuple<H, Ts...>: tuple<Ts...> {
	H h;
	using Parent = tuple<Ts...>;
public:
	using This = tuple;

	constexpr tuple() = default;
	constexpr tuple(tuple &&) = default;
	constexpr tuple(tuple const &) = default;

	template<class I, class... Us> constexpr tuple(I &&i, Us &&...us)
		noexcept((::std::is_nothrow_constructible_v<H, I> && ... && ::std::is_nothrow_constructible_v<Ts, Us>))
		: Parent(::std::forward<Us>(us)...), h(::std::forward<I>(i)) {}
	template<class I, class... Us> constexpr tuple(tuple<I, Us...> &&that)
		noexcept((::std::is_nothrow_constructible_v<H, I> && ... && ::std::is_nothrow_constructible_v<Ts, Us>))
		: tuple(::std::move(that), make_index_sequence<1, sizeof...(Ts) + 1>{}) {}
	template<class I, class... Us> constexpr tuple(tuple<I, Us...> const &that)
		noexcept((::std::is_nothrow_constructible_v<H, add_lvalue_const_reference_t<I>> && ...
			  && ::std::is_nothrow_constructible_v<Ts, add_lvalue_const_reference_t<Us>>))
		: tuple(that, make_index_sequence<1, sizeof...(Ts) + 1>{}) {}

	constexpr tuple &operator=(tuple &&that) = default;
	constexpr tuple &operator=(tuple const &that) = default;

	template<class... Us> constexpr tuple &operator=(tuple<Us...> &&that)
		noexcept(noexcept(assign(::std::move(that), make_index_sequence<1, sizeof...(Ts) + 1>{})))
	{
		return assign(::std::move(that), make_index_sequence<1, sizeof...(Ts) + 1>{});
	}
	template<class... Us> constexpr tuple &operator=(tuple<Us...> const &that)
		noexcept(noexcept(assign(that, make_index_sequence<1, sizeof...(Ts) + 1>{})))
	{
		return assign(that, make_index_sequence<1, sizeof...(Ts) + 1>{});
	}

	constexpr void swap(tuple &that) noexcept(pack::all_v<::std::is_nothrow_swappable, H, Ts...>) {
		using ::std::swap;
		if constexpr(::std::is_nothrow_swappable_v<H>) Parent::swap(that), swap(h, that.h);
		else swap(h, that.h), Parent::swap(that);
	}

	template<::std::size_t i> constexpr decltype(auto) get() && noexcept {
		if constexpr(i == 0) return ::std::move(h);
		else return Parent::template get<i - 1>();
	}
	template<::std::size_t i> constexpr decltype(auto) get() const && noexcept {
		if constexpr(i == 0) return ::std::move(h);
		else return Parent::template get<i - 1>();
	}
	template<::std::size_t i> constexpr decltype(auto) get() & noexcept {
		if constexpr(i == 0) return h;
		else return Parent::template get<i - 1>();
	}
	template<::std::size_t i> constexpr decltype(auto) get() const & noexcept {
		if constexpr(i == 0) return h;
		else return Parent::template get<i - 1>();
	}

	template<class T> constexpr T &&get() && noexcept {
		if constexpr(::std::is_same_v<T, H>) return ::std::move(h);
		else return Parent::template get<T>();
	}
	template<class T> constexpr T const &&get() const && noexcept {
		if constexpr(::std::is_same_v<T, H>) return ::std::move(h);
		else return Parent::template get<T>();
	}
	template<class T> constexpr T &get() & noexcept {
		if constexpr(::std::is_same_v<T, H>) return h;
		else return Parent::template get<T>();
	}
	template<class T> constexpr T const &get() const & noexcept {
		if constexpr(::std::is_same_v<T, H>) return h;
		else return Parent::template get<T>();
	}

	constexpr Parent &&tail() && noexcept { return static_cast<Parent &&>(*this); }
	constexpr Parent const &&tail() const && noexcept { return static_cast<Parent const &&>(*this); }
	constexpr Parent &tail() & noexcept { return static_cast<Parent &>(*this); }
	constexpr Parent const &tail() const & noexcept { return static_cast<Parent const &>(*this); }
private:
	template<class That, ::std::size_t... is> constexpr tuple(That &&that, ::std::index_sequence<is...>)
		noexcept(noexcept(Parent{::std::forward<That>(that).template get<is>()...}, H(::std::forward<That>(that).template get<0>())))
		: Parent{::std::forward<That>(that).template get<is>()...}, h(::std::forward<That>(that).template get<0>()) {}

	template<class That, ::std::size_t... is> constexpr tuple &assign(That &&that, ::std::index_sequence<is...>)
		noexcept(noexcept(nstd::value_assign(h, that.template get<0>(), tail(), that.tail())))
	{
		return nstd::value_assign(h, that.template get<0>(), tail(), that.tail());
	}
};
template<class... Ts> tuple(Ts &&...) -> tuple<Ts...>;


class make_tuple_impl: private_namespace {
	template<class T> using U = ::std::conditional_t<is_similar_v<T, ::std::reference_wrapper<int>>, type_t<T> &, T>;
	template<class... Ts> friend constexpr auto make_tuple(Ts &&...ts) noexcept((::std::is_nothrow_constructible_v<::std::decay_t<Ts>, Ts> && ...));
};

template<class... Ts> inline constexpr auto make_tuple(Ts &&...ts) noexcept((::std::is_nothrow_constructible_v<::std::decay_t<Ts>, Ts> && ...)) {
	return tuple<make_tuple_impl::U<::std::decay_t<Ts>>...>{::std::forward<Ts>(ts)...};
}


template<class... Ts> inline constexpr tuple<Ts &...> tie(Ts &...ts) noexcept { return {ts...}; }


template<class... Ts> inline constexpr tuple<Ts &&...> forward_as_tuple(Ts &&...ts) noexcept { return {::std::forward<Ts>(ts)...}; }


SFINAE_U(method_get, u.template get<i>(), ::std::size_t i COMMA(), i COMMA());
SFINAE_U(method_unique_get, u.template get<T>(), class T COMMA(), T COMMA());
//SFINAE_U(adl_get, get<i>(u), ::std::size_t i COMMA(), i COMMA());
//SFINAE_U(adl_unique_get, get<T>(u), class T COMMA(), T COMMA());
SFINAE_U(std_get, ::std::get<i>(u), ::std::size_t i COMMA(), i COMMA());
SFINAE_U(std_unique_get, ::std::get<T>(u), class T COMMA(), T COMMA());


class tuple_unsafe: private_namespace {
	template<class T> struct getter {
		template<::std::size_t i> static constexpr decltype(auto) call(T &&t) noexcept {
			if constexpr(has_method_get_v<i, T>) return ::std::move(t).template get<i>();
			// else if constexpr(has_adl_get_v<i, T>) return get<i>(::std::move(t));
			else if constexpr(has_std_get_v<i, T>) return ::std::get<i>(::std::move(t));
			else return ::std::move(t);
		}
		template<::std::size_t i> static constexpr decltype(auto) call(T const &&t) noexcept {
			if constexpr(has_method_get_v<i, T>) return ::std::move(t).template get<i>();
			// else if constexpr(has_adl_get_v<i, T>) return get<i>(::std::move(t));
			else if constexpr(has_std_get_v<i, T>) return ::std::get<i>(::std::move(t));
			else return ::std::move(t);
		}
		template<::std::size_t i> static constexpr decltype(auto) call(T &t) noexcept {
			if constexpr(has_method_get_v<i, T>) return t.template get<i>();
			// else if constexpr(has_adl_get_v<i, T>) return get<i>(t);
			else if constexpr(has_std_get_v<i, T>) return ::std::get<i>(t);
			else return t;
		}
		template<::std::size_t i> static constexpr decltype(auto) call(T const &t) noexcept {
			if constexpr(has_method_get_v<i, T>) return t.template get<i>();
			// else if constexpr(has_adl_get_v<i, T>) return get<i>(t);
			else if constexpr(has_std_get_v<i, T>) return ::std::get<i>(t);
			else return t;
		}
		template<class U> static constexpr decltype(auto) call(T &&t) noexcept {
			if constexpr(has_method_unique_get_v<U, T>) return ::std::move(t).template get<U>();
			// else if constexpr(has_adl_unique_get_v<U, T>) return get<U>(::std::move(t));
			else if constexpr(has_std_unique_get_v<U, T>) return ::std::get<U>(::std::move(t));
			else return U(::std::move(t));
		}
		template<class U> static constexpr decltype(auto) call(T const &&t) noexcept {
			if constexpr(has_method_unique_get_v<U, T>) return ::std::move(t).template get<U>();
			// else if constexpr(has_adl_unique_get_v<U, T>) return get<U>(::std::move(t));
			else if constexpr(has_std_unique_get_v<U, T>) return ::std::get<U>(::std::move(t));
			else return U(::std::move(t));
		}
		template<class U> static constexpr decltype(auto) call(T &t) noexcept {
			if constexpr(has_method_unique_get_v<U, T>) return t.template get<U>();
			// else if constexpr(has_adl_unique_get_v<U, T>) return get<U>(t);
			else if constexpr(has_std_unique_get_v<U, T>) return ::std::get<U>(t);
			else return U(t);
		}
		template<class U> static constexpr decltype(auto) call(T const &t) noexcept {
			if constexpr(has_method_unique_get_v<U, T>) return t.template get<U>();
			// else if constexpr(has_adl_unique_get_v<U, T>) return get<U>(t);
			else if constexpr(has_std_unique_get_v<U, T>) return ::std::get<U>(t);
			else return U(t);
		}
	};

	template<::std::size_t i, class H, class...Ts> static constexpr decltype(auto) call(H &&h, Ts &&...ts) noexcept {
		if constexpr(i < tuple_size_v<H>) return getter<::std::decay_t<H>>::template call<i>(::std::forward<H>(h));
		else return tuple_unsafe::call<i - tuple_size_v<H>>(::std::forward<Ts>(ts)...);
	}

	template<class U, class T> static constexpr decltype(auto) call(T &&t) noexcept {
		return getter<::std::decay_t<T>>::template call<T>(::std::forward<T>(t));
	}

	template<::std::size_t i, class T, class... Ts> friend constexpr decltype(auto) get(T &&t, Ts &&...ts) noexcept;
	template<class U, class T, class... Ts> friend constexpr decltype(auto) get(T &&t, Ts &&...ts) noexcept;
};

template<::std::size_t i, class T, class... Ts> inline constexpr decltype(auto) get(T &&t, Ts &&...ts) noexcept {
	static_assert(i < tuple_size_v<T, Ts...>, "nstd::get: index out of range");
	return tuple_unsafe::call<i>(::std::forward<T>(t), ::std::forward<Ts>(ts)...);
}

template<class U, class T> inline constexpr decltype(auto) get(T &&t) noexcept {
	return tuple_unsafe::call<U>(::std::forward<T>(t));
}


template<class... Ts> inline constexpr decltype(auto) tuple_head(Ts &&...ts) noexcept {
	return nstd::get<0>(::std::forward<Ts>(ts)...);
}

template<class... Ts> inline constexpr decltype(auto) tuple_last(Ts &&...ts) noexcept {
	constexpr ::std::size_t sz = (tuple_size_v<Ts> + ...);
	static_assert(sz > 0, "tuple_last: argument(s) empty");
	return nstd::get<sz - 1>(::std::forward<Ts>(ts)...);
}


namespace pack {

template<class... Hs, class... Ts> struct splice<tuple<Hs...>, Ts...>: concat<list<Hs...>, splice_t<Ts...>> {};

}

class tuple_cat_impl: private_namespace {
	template<class... Ts, ::std::size_t... is> static constexpr pack::apply_t<tuple, Ts...> tuple_cat(::std::index_sequence<is...>, Ts &&...ts)
		noexcept(noexcept(pack::apply_t<tuple, Ts...>{get<is>(::std::forward<Ts>(ts)...)...}))
	{
		return {get<is>(::std::forward<Ts>(ts)...)...};
	}

	template<class... Ts> friend constexpr tuple<pack::splice_t<Ts...>> tuple_cat(Ts &&...ts)
		noexcept(noexcept(tuple_cat_impl::tuple_cat(pack::index_sequence_for<Ts...>{}, ::std::forward<Ts>(ts)...)));
};

template<class... Ts> inline constexpr pack::apply_t<tuple, Ts...> tuple_cat(Ts &&...ts)
	noexcept(noexcept(tuple_cat_impl::tuple_cat(pack::index_sequence_for<Ts...>{}, ::std::forward<Ts>(ts)...)))
{
	return tuple_cat_impl::tuple_cat(pack::index_sequence_for<Ts...>{}, ::std::forward<Ts>(ts)...);
}


class tuple_splice_impl: private_namespace {
	template<class... Ts, ::std::size_t... is>
	static constexpr pack::apply_t<tuple, pack::map<::std::add_rvalue_reference, Ts...>> splice_as_tuple(::std::index_sequence<is...>, Ts &&...ts)
		noexcept(noexcept(pack::apply<tuple, pack::map<::std::add_rvalue_reference, Ts...>>{get<is>(::std::forward<Ts>(ts)...)...}))
	{
		return {get<is>(::std::forward<Ts>(ts)...)...};
	}

	template<class... Ts> friend constexpr pack::apply<tuple, pack::map<::std::add_rvalue_reference, pack::splice_t<Ts...>>> splice_as_tuple(Ts &&...ts)
		noexcept(noexcept(tuple_splice_impl::splice_as_tuple(pack::index_sequence_for<Ts...>{}, ::std::forward<Ts>(ts)...)));
};

template<class... Ts> inline constexpr pack::apply_t<tuple, pack::map<::std::add_rvalue_reference, Ts...>> splice_as_tuple(Ts &&...ts)
	noexcept(noexcept(tuple_splice_impl::splice_as_tuple(pack::index_sequence_for<Ts...>{}, ::std::forward<Ts>(ts)...)))
{
	return tuple_splice_impl::splice_as_tuple(pack::index_sequence_for<Ts...>{}, ::std::forward<Ts>(ts)...);
}


template<class... Ts> inline constexpr bool operator==(tuple<Ts...> const &left, tuple<Ts...> const &right)
	noexcept(noexcept(left.template get<0>() == right.template get<0>() && left.tail() == right.tail()))
{
	return left.template get<0>() == right.template get<0>() && left.tail() == right.tail();
}
constexpr bool operator==(tuple<>, tuple<>) noexcept { return true; }

template<class... Ts> inline constexpr bool operator!=(tuple<Ts...> const &left, tuple<Ts...> const &right)
	noexcept(noexcept(left.template get<0>() != right.template get<0>() || left.tail() != right.tail()))
{
	return left.template get<0>() != right.template get<0>() || left.tail() != right.tail();
}
constexpr bool operator!=(tuple<>, tuple<>) noexcept { return false; }

template<class... Ts> inline constexpr bool operator<(tuple<Ts...> const &left, tuple<Ts...> const &right)
	noexcept(noexcept(left.template get<0>() < right.template get<0>() || (left.template get<0>() == right.template get<0>() && left.tail() < right.tail())))
{
	return left.template get<0>() < right.template get<0>() || (left.template get<0>() == right.template get<0>() && left.tail() < right.tail());
}
constexpr bool operator<(tuple<>, tuple<>) noexcept { return false; }

template<class... Ts> inline constexpr bool operator<=(tuple<Ts...> const &left, tuple<Ts...> const &right)
	noexcept(noexcept(left.template get<0>() <= right.template get<0>() && left.tail() <= right.tail()))
{
	return left.template get<0>() <= right.template get<0>() && left.tail() <= right.tail();
}
constexpr bool operator<=(tuple<>, tuple<>) noexcept { return true; }

template<class... Ts> inline constexpr bool operator>(tuple<Ts...> const &left, tuple<Ts...> const &right)
	noexcept(noexcept(left.template get<0>() > right.template get<0>() || (left.template get<0>() == right.template get<0>() && left.tail() > right.tail())))
{
	return left.template get<0>() > right.template get<0>() || (left.template get<0>() == right.template get<0>() && left.tail() > right.tail());
}
constexpr bool operator>(tuple<>, tuple<>) noexcept { return false; }

template<class... Ts> inline constexpr bool operator>=(tuple<Ts...> const &left, tuple<Ts...> const &right)
	noexcept(noexcept(left.template get<0>() >= right.template get<0>() && left.tail() >= right.tail()))
{
	return left.template get<0>() >= right.template get<0>() && left.tail() >= right.tail();
}
constexpr bool operator>=(tuple<>, tuple<>) noexcept { return true; }

template<class... Ts> inline constexpr void swap(tuple<Ts...> &left, tuple<Ts...> &right) noexcept(noexcept(left.swap(right))) { left.swap(right); }


class tuple_for_each_impl: private_namespace {
	template<class F, class T, ::std::size_t i> static constexpr bool prev_noexcept() noexcept {
		if constexpr(i == tuple_size_v<T>) return true;
		else return ::std::is_nothrow_invocable_v<F, tuple_element_t<i, T>> && prev_noexcept<F, T, i + 1>();
	}

	template<class F> static constexpr bool is_noexcept() noexcept { return true; }
	template<class F, class T> static constexpr bool is_noexcept() noexcept { return prev_noexcept<F, T, 0>(); }
	template<class F, class T1, class T2, class... Ts> static constexpr bool is_noexcept() noexcept {
		return prev_noexcept<F, T1, 0>() && is_noexcept<F, T2, Ts...>();
	}

	template<class F, class T, ::std::size_t... is> static constexpr void run(F &&f, T &&t, ::std::index_sequence<is...>)
		noexcept(noexcept(::std::invoke(f, get<is>(::std::forward<T>(t))...)))
	{
		::std::invoke(f, get<is>(::std::forward<T>(t))...);
	}
	template<::std::size_t i, class F, class T, ::std::size_t... is> static constexpr decltype(auto) ret(F &&f, T &&t, ::std::index_sequence<is...>)
		noexcept(noexcept((::std::invoke(f, get<is>(::std::forward<T>(t))), ...)) && noexcept(::std::invoke(f, get<i>(::std::forward<T>(t)))))
	{
		::std::invoke(f, get<is>(::std::forward<T>(t))...);
		return ::std::invoke(f, get<i>(::std::forward<T>(t)));
	}

	template<class F, class T, class... Ts> friend constexpr decltype(auto) tuple_for_each(F &&f, T &&t, Ts &&...ts)
		noexcept(tuple_for_each_impl::is_noexcept<F, T, Ts...>());
};

template<class F, class T, class... Ts> inline constexpr decltype(auto) tuple_for_each(F &&f, T &&t, Ts &&...ts)
	noexcept(tuple_for_each_impl::is_noexcept<F, T, Ts...>())
{
	if constexpr(sizeof...(Ts) == 0) {
		return tuple_for_each_impl::ret<tuple_size_v<T> - 1>(::std::forward<F>(f), ::std::forward<T>(t), ::std::make_index_sequence<tuple_size_v<T> - 1>{});
	}
	else {
		tuple_for_each_impl::run(f, ::std::forward<T>(t), ::std::make_index_sequence<tuple_size_v<T>>{});
		return tuple_for_each(::std::forward<F>(f), ::std::forward<Ts>(ts)...);
	}
}


class tuple_transform_impl: private_namespace {
	template<class F, class... Ts, ::std::size_t... is> static constexpr pack::apply_t<tuple, Ts...> call(F &&f, ::std::index_sequence<is...>, Ts &&...ts)
		noexcept(noexcept(pack::apply_t<tuple, Ts...>{::std::invoke(f, get<is>(::std::forward<Ts>(ts)...))...}))
	{
		return {::std::invoke(f, get<is>(::std::forward<Ts>(ts)...))...};
	}

	template<class F, class T, class... Ts> friend constexpr decltype(auto) tuple_transform(F &&f, T &&t, Ts &&...ts) noexcept(noexcept(tuple_transform_impl::call(
				::std::forward<F>(f), pack::index_sequence_for<T, Ts...>{}, ::std::forward<T>(t), ::std::forward<Ts>(ts)...)));
};

template<class F, class T, class... Ts> inline constexpr decltype(auto) tuple_transform(F &&f, T &&t, Ts &&...ts)
	noexcept(noexcept(tuple_transform_impl::call(::std::forward<F>(f), pack::index_sequence_for<T, Ts...>{}, ::std::forward<T>(t), ::std::forward<Ts>(ts)...)))
{
	return tuple_transform_impl::call(::std::forward<F>(f), pack::index_sequence_for<T, Ts...>{}, ::std::forward<T>(t), ::std::forward<Ts>(ts)...);
}


class apply_impl: private_namespace {
	template<class F, class... Ts, ::std::size_t... is> static constexpr auto call(F &&f, ::std::index_sequence<is...>, Ts &&...ts)
		noexcept(noexcept(::std::invoke(::std::forward<F>(f), get<is>(::std::forward<Ts>(ts)...)...)))
	{
		return ::std::invoke(::std::forward<F>(f), get<is>(::std::forward<Ts>(ts)...)...);
	}

	template<class F, class... Ts> friend constexpr decltype(auto) apply(F &&f, Ts &&...ts)
		noexcept(noexcept(apply_impl::call(::std::forward<F>(f), pack::index_sequence_for<::std::decay_t<Ts>...>{}, ::std::forward<Ts>(ts)...)));
};

template<class F, class... Ts> inline constexpr decltype(auto) apply(F &&f, Ts &&...ts)
	noexcept(noexcept(apply_impl::call(::std::forward<F>(f), pack::index_sequence_for<::std::decay_t<Ts>...>{}, ::std::forward<Ts>(ts)...)))
{
	return apply_impl::call(::std::forward<F>(f), pack::index_sequence_for<::std::decay_t<Ts>...>{}, ::std::forward<Ts>(ts)...);
}


class tuple_reduce_impl: private_namespace {
	template<::std::size_t left, ::std::size_t right, class F, class T, class... Ts> static constexpr auto call(F &&f, T &&t, Ts &&...ts)
		noexcept(left + 1 == right?
			 noexcept(::std::invoke(::std::forward<F>(f), ::std::forward<T>(t), get<left>(::std::forward<Ts>(ts)...)))
			 : noexcept(call<left + 1, right>(f, ::std::invoke(f, ::std::forward<T>(t), get<left>(::std::forward<Ts>(ts)...)), ::std::forward<Ts>(ts)...)))
	{
		if constexpr(left + 1 == right) return ::std::invoke(::std::forward<F>(f), ::std::forward<T>(t), get<left>(::std::forward<Ts>(ts)...));
		else return call<left + 1, right>(f, ::std::invoke(f, ::std::forward<T>(t), get<left>(::std::forward<Ts>(ts)...)), ::std::forward<Ts>(ts)...);
	}
	template<class F, class... Ts> friend constexpr auto tuple_reduce(F &&f, Ts &&...ts)
		noexcept(tuple_size_v<Ts...> == 1?
			 noexcept(tuple_element_t<0, ::std::decay_t<Ts>...>(get<0>(::std::forward<Ts>(ts)...)))
			 : noexcept(tuple_reduce_impl::call<1, tuple_size_v<Ts...>>(
					 ::std::forward<F>(f), get<0>(::std::forward<Ts>(ts)...), ::std::forward<Ts>(ts)...)));
};

template<class F, class... Ts> inline constexpr auto tuple_reduce(F &&f, Ts &&...ts)
	noexcept(tuple_size_v<Ts...> == 1?
		 noexcept(tuple_element_t<0, ::std::decay_t<Ts>...>(get<0>(::std::forward<Ts>(ts)...)))
		 : noexcept(tuple_reduce_impl::call<1, tuple_size_v<Ts...>>(::std::forward<F>(f), get<0>(::std::forward<Ts>(ts)...), ::std::forward<Ts>(ts)...)))
{
	constexpr ::std::size_t nArgs = tuple_size_v<Ts...>;
	static_assert(nArgs > 0, "No tuple elements to reduce");
	if constexpr(nArgs == 1) return get<0>(::std::forward<Ts>(ts)...);
	else return tuple_reduce_impl::call<1, nArgs>(::std::forward<F>(f), get<0>(::std::forward<Ts>(ts)...), ::std::forward<Ts>(ts)...);
}


template<::std::size_t n, class T> constexpr auto tuple_replicate(T const &t)
	/* noexcept(n == 0
		 || (n == 1 && ::std::is_nothrow_constructible_v<tuple<T>, T const &>)
		 || (n > 1 && noexcept(tuple_cat(tuple_replicate<n / 2, T>(t), tuple_replicate<n - n / 2, T>(t))))) */;

template<::std::size_t n, class T> inline constexpr auto tuple_replicate(T const &t)
	// noexcept(n == 0
		 // || (n == 1 && ::std::is_nothrow_constructible_v<tuple<T>, T const &>)
		 // || (n > 1 && noexcept(tuple_cat(tuple_replicate<n / 2, T>(t), tuple_replicate<n - n / 2, T>(t)))))
{
	if constexpr(n == 0) return tuple<>{};
	else if constexpr(n == 1) return tuple<T>{t};
	else return tuple_cat(tuple_replicate<n / 2, T>(t), tuple_replicate<n - n / 2, T>(t));
}


template<::std::size_t n, class T> inline constexpr auto tuple_replicate(T &&t)
	/* noexcept(n == 0
		 || (n == 1 && ::std::is_nothrow_constructible_v<tuple<T>, T &&>)
		 || (n > 1 && noexcept(tuple_cat(tuple_replicate<n / 2, T>(t), tuple_replicate<n - n / 2, T>(::std::forward<T>(t)))))) */;

template<::std::size_t n, class T> inline constexpr auto tuple_replicate(T &&t)
	// noexcept(n == 0
		 // || (n == 1 && ::std::is_nothrow_constructible_v<tuple<T>, T &&>)
		 // || (n > 1 && noexcept(tuple_cat(tuple_replicate<n / 2, T>(t), tuple_replicate<n - n / 2, T>(::std::forward<T>(t))))))
{
	if constexpr(n == 0) return tuple<>{};
	else if constexpr(n == 1) return tuple<T>{::std::forward<T>(t)};
	else return tuple_cat(tuple_replicate<n / 2, T>(t), tuple_replicate<n - n / 2, T>(::std::forward<T>(t)));
}

}

#ifndef SHOW_NOTHING

#include "show.h"

namespace nstd {

SHOW_NSTD_ROOT(1, tuple);

}

#endif

#endif
