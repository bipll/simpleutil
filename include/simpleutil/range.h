/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__RANGE_H_6eb1221d614b133cc9d306f00cf843da16fb4894
#define SIMPLEUTIL__RANGE_H_6eb1221d614b133cc9d306f00cf843da16fb4894

#include <type_traits>
#include "typeutil.h"
#include "iterator.h"
#include "tuple.h"

namespace nstd {

template<class Itr, class End> struct range {
	using This = range;

	constexpr range() = default;
	constexpr range(Itr itr, End end) noexcept(pack::all_v<::std::is_nothrow_move_constructible, Itr, End>)
		: itr(::std::forward<Itr>(itr)), stop(::std::forward<End>(end)) {}

	constexpr decltype(auto) begin() noexcept(NOEXCEPT(itr)) { return itr; }
	constexpr decltype(auto) begin() const noexcept(NOEXCEPT(itr)) { return itr; }
	constexpr decltype(auto) cbegin() const noexcept(NOEXCEPT(itr)) { return itr; }

	constexpr decltype(auto) end() noexcept(NOEXCEPT(stop)) { return stop; }
	constexpr decltype(auto) end() const noexcept(NOEXCEPT(stop)) { return stop; }
	constexpr decltype(auto) cend() const noexcept(NOEXCEPT(stop)) { return stop; }
private:
	Itr itr;
	End stop;
};
template<class Itr, class End> range(Itr, End) -> range<Itr, End>;

template<class Itr> struct range<Itr, sentinel> {
	using This = range;

	constexpr range() = default;
	constexpr range(Itr &&itr) noexcept(::std::is_nothrow_move_constructible_v<Itr>): itr(::std::forward<Itr>(itr)) {}

	constexpr auto begin() noexcept(NOEXCEPT(itr)) { return itr; }
	constexpr auto begin() const noexcept(NOEXCEPT(itr)) { return itr; }
	constexpr auto cbegin() const noexcept(NOEXCEPT(itr)) { return itr; }

	constexpr sentinel end() const noexcept { return {}; }
	constexpr sentinel cend() const noexcept { return {}; }
private:
	Itr itr;
};
template<class Itr> range(Itr &&) -> range<Itr, sentinel>;

template<class Ctr> static constexpr bool ends_dereferenceably() { return dereference::compiles<decltype(adl_or_std::end::call(::std::declval<Ctr>()))>; }

template<class Ctr> inline bool can_nothrow_begin_v = noexcept(adl_or_std::begin::call(::std::declval<Ctr>()));
template<class Ctr> inline bool can_nothrow_end_v = noexcept(adl_or_std::end::call(::std::declval<Ctr>()));

template<class Container, class Range> inline constexpr bool is_nothrow_containable_v = ::std::is_nothrow_move_constructible_v<Container>
	&& (::std::is_same_v<iterator_type_t<Range>, end_type_t<Range>>?
		    can_nothrow_begin_v<Range> && can_nothrow_end_v<Range> && ::std::is_nothrow_constructible_v<Container, iterator_type_t<Range>>
		    : ::std::is_nothrow_constructible_v<Container>
			    && noexcept(post(::std::declval<::std::add_lvalue_reference_t<Container>>(), *::std::declval<iterator_type_t<Range>>())));

template<class Container, class Range> inline constexpr auto make_container(Range &&range) noexcept(is_nothrow_containable_v<Container, Range>) {
	if constexpr(::std::is_same_v<iterator_type_t<Range>, end_type_t<Range>>) return Container(adl_or_std::begin::call(range), adl_or_std::end::call(range));
	else {
		Container retVal;
		for(auto &&x: range) post(retVal, ::std::forward<decltype(x)>(x));
		return retVal;
	}
}

template<class Container> struct tail {
	tail(Container &&container): c(std::forward<Container>(container)) {}

	constexpr auto begin() noexcept(NOEXCEPT(++adl_or_std::begin::call(c))) { return ++adl_or_std::begin::call(c); }
	constexpr auto begin() const noexcept(NOEXCEPT(++adl_or_std::begin::call(c))) { return ++adl_or_std::begin::call(c); }
	constexpr auto end() noexcept(NOEXCEPT(adl_or_std::end::call(c))) { return adl_or_std::end::call(c); }
	constexpr auto end() const noexcept(NOEXCEPT(adl_or_std::end::call(c))) { return adl_or_std::end::call(c); }
private:
	Container c;
};
template<class Container> tail(Container &&) -> tail<Container>;

template<class Container> struct init {
	init(Container &&container): c(std::forward<Container>(container)) {}

	constexpr auto begin() noexcept(NOEXCEPT(adl_or_std::begin::call(c))) { return adl_or_std::begin::call(c); }
	constexpr auto begin() const noexcept(NOEXCEPT(adl_or_std::begin::call(c))) { return adl_or_std::begin::call(c); }
	constexpr auto end() noexcept(NOEXCEPT(adl_or_std::end::call(c))) { return --adl_or_std::end::call(c); }
	constexpr auto end() const noexcept(NOEXCEPT(adl_or_std::end::call(c))) { return --adl_or_std::end::call(c); }
private:
	Container c;
};
template<class Container> init(Container &&) -> init<Container>;

template<class Container> struct trim {
	constexpr trim(Container &&container, int head = 0, int tail = 0): c(std::forward<Container>(container)), h(head), t(-tail) {}

	constexpr auto begin()
		noexcept(NOEXCEPT(adl_or_std::begin::call(c)) && noexcept(::std::advance(::std::declval<iterator_type_t<Container>>(), h)))
	{
		auto retVal{adl_or_std::begin::call(c)};
		std::advance(retVal, h);
		return retVal;
	}
	constexpr auto begin() const
		noexcept(NOEXCEPT(adl_or_std::begin::call(c)) && noexcept(::std::advance(::std::declval<iterator_type_t<Container>>(), h)))
	{
		auto retVal{adl_or_std::begin::call(c)};
		std::advance(retVal, h);
		return retVal;
	}
	constexpr auto end()
		noexcept(NOEXCEPT(adl_or_std::end::call(c)) && noexcept(::std::advance(::std::declval<end_type_t<Container>>(), t)))
	{
		auto retVal{adl_or_std::end::call(c)};
		std::advance(retVal, t);
		return retVal;
	}
	constexpr auto end() const
		noexcept(NOEXCEPT(adl_or_std::end::call(c)) && noexcept(::std::advance(::std::declval<end_type_t<Container>>(), t)))
	{
		auto retVal{adl_or_std::end::call(c)};
		std::advance(retVal, t);
		return retVal;
	}
private:
	Container c;
	int h, t;
};
template<class Container> trim(Container &&, int = 0, int = 0) -> trim<Container>;

template<class... Ctrs> class zip {
	using Zipper = tuple<Ctrs...>;

	template<::std::size_t... is> constexpr auto begin(::std::index_sequence<is...>)
		noexcept(NOEXCEPT(zip_iterator{adl_or_std::begin::call(::std::get<is>(zipper))...}))
	{
		return zip_iterator{adl_or_std::begin::call(::std::get<is>(zipper))...};
	}
	template<::std::size_t... is> constexpr auto end(::std::index_sequence<is...>)
		noexcept(NOEXCEPT(zip_iterator{adl_or_std::end::call(::std::get<is>(zipper))...}))
	{
		return zip_iterator{adl_or_std::end::call(::std::get<is>(zipper))...};
	}

	Zipper zipper;
public:
	constexpr zip(Ctrs &&...ctrs) noexcept(noexcept(::std::is_nothrow_constructible_v<Zipper, Ctrs &&...>)): zipper(std::forward<Ctrs>(ctrs)...) {}
	constexpr zip(zip &&) noexcept(::std::is_nothrow_move_constructible_v<Zipper>) = default;
	constexpr zip(zip const &) noexcept(::std::is_nothrow_copy_constructible_v<Zipper>) = default;

	zip &operator=(zip &&) noexcept(::std::is_nothrow_move_assignable_v<Zipper>) = default;
	zip &operator=(zip const &) noexcept(::std::is_nothrow_copy_assignable_v<Zipper>) = default;

	constexpr decltype(auto) begin() noexcept(noexcept(begin(::std::index_sequence_for<Ctrs...>{}))) {
		return begin(::std::index_sequence_for<Ctrs...>{});
	}
	constexpr decltype(auto) end() noexcept(noexcept(end(::std::index_sequence_for<Ctrs...>{}))) {
		return end(::std::index_sequence_for<Ctrs...>{});
	}
};
template<class... Ctrs> zip(Ctrs &&...) -> zip<Ctrs...>;

template<class... Ctrs> class mzip {
	using MZipper = tuple<Ctrs...>;

	template<::std::size_t... is> constexpr auto begin(::std::index_sequence<is...>)
		noexcept(noexcept(mzip_iterator{adl_or_std::begin::call(::std::get<is>(mzipper))...}))
	{
		return mzip_iterator{adl_or_std::begin::call(::std::get<is>(mzipper))...};
	}
	template<::std::size_t... is> constexpr auto end(::std::index_sequence<is...>)
		noexcept(noexcept(mzip_iterator{adl_or_std::end::call(::std::get<is>(mzipper))...}))
	{
		return mzip_iterator{adl_or_std::end::call(::std::get<is>(mzipper))...};
	}

	MZipper mzipper;
public:
	constexpr mzip(Ctrs &&...ctrs) noexcept(noexcept(::std::is_nothrow_constructible_v<MZipper, Ctrs &&...>)): mzipper(std::forward<Ctrs>(ctrs)...) {}
	constexpr mzip(mzip &&) noexcept(::std::is_nothrow_move_constructible_v<MZipper>) = default;
	constexpr mzip(mzip const &) noexcept(::std::is_nothrow_copy_constructible_v<MZipper>) = default;

	mzip &operator=(mzip &&) noexcept(::std::is_nothrow_move_assignable_v<MZipper>) = default;
	mzip &operator=(mzip const &) noexcept(::std::is_nothrow_copy_assignable_v<MZipper>) = default;

	constexpr decltype(auto) begin() noexcept(noexcept(begin(::std::index_sequence_for<Ctrs...>{}))) {
		return begin(::std::index_sequence_for<Ctrs...>{});
	}
	constexpr decltype(auto) end() noexcept(noexcept(end(::std::index_sequence_for<Ctrs...>{}))) {
		return end(::std::index_sequence_for<Ctrs...>{});
	}
};
template<class... Ctrs> mzip(Ctrs &&...) -> mzip<Ctrs...>;

template<class F, class... Ctrs> struct transform {
	using This = transform;
	using iterator = transform_iterator<F, iterator_type_t<Ctrs>...>;
	using const_iterator = transform_iterator<::std::add_const_t<F>, const_iterator_type_t<Ctrs>...>;
private:
	using Zipper = tuple<Ctrs...>;
	using end_type_t = stop_iteration<end_type_t<Ctrs>...>;
	using const_end_type_t = stop_iteration<const_end_type_t<Ctrs>...>;

	F f;
	Zipper zipper;

	template<::std::size_t... is> constexpr iterator begin(::std::index_sequence<is...>)
		noexcept(noexcept(transform_iterator{f, adl_or_std::begin::call(::std::get<is>(zipper))...})
			 && ::std::is_nothrow_move_constructible_v<decltype(transform_iterator(f, adl_or_std::begin::call(::std::get<is>(zipper))...))>)
	{
		return iterator(::std::move(f), adl_or_std::begin::call(::std::get<is>(zipper))...);
	}
	template<::std::size_t... is> constexpr const_iterator begin(::std::index_sequence<is...>) const
		noexcept(noexcept(transform_iterator{f, adl_or_std::begin::call(::std::get<is>(zipper))...})
			 && ::std::is_nothrow_move_constructible_v<decltype(transform_iterator(f, adl_or_std::begin::call(::std::get<is>(zipper))...))>)
	{
		return transform_iterator(::std::move(f), adl_or_std::begin::call(::std::get<is>(zipper))...);
	}

	template<::std::size_t... is> constexpr end_type_t end(::std::index_sequence<is...>)
		noexcept(NOEXCEPT(end_type_t{adl_or_std::end::call(::std::get<is>(zipper))...}))
	{
		return end_type_t{adl_or_std::end::call(::std::get<is>(zipper))...};
	}
	template<::std::size_t... is> constexpr const_end_type_t end(::std::index_sequence<is...>) const
		noexcept(NOEXCEPT(const_end_type_t{adl_or_std::end::call(::std::get<is>(zipper))...}))
	{
		return const_end_type_t{adl_or_std::end::call(::std::get<is>(zipper))...};
	}

	template<::std::size_t... is> constexpr bool empty(::std::index_sequence<is...>) const noexcept {
		using ::std::empty;
		return (empty(::std::get<is>(zipper)) || ...);
	}
public:
	constexpr transform(F f, Ctrs &&...ctrs) noexcept(::std::is_nothrow_move_constructible_v<F> && ::std::is_nothrow_constructible_v<Zipper, Ctrs &&...>)
		: f(::std::move(f)), zipper{::std::forward<Ctrs>(ctrs)...} {}
	constexpr transform(transform &&) noexcept(::std::is_nothrow_move_constructible_v<F> && ::std::is_nothrow_move_constructible_v<Zipper>) = default;
	constexpr transform(transform const &) noexcept(::std::is_nothrow_copy_constructible_v<F> && ::std::is_nothrow_copy_constructible_v<Zipper>) = default;

	transform &operator=(transform that) noexcept(NOEXCEPT(value_assign(f, ::std::move(that.f), zipper, ::std::move(that.zipper)))) {
		return value_assign(f, ::std::move(that.f), zipper, ::std::move(that.zipper)), *this;
	}

	constexpr iterator begin() noexcept(noexcept(begin(::std::index_sequence_for<Ctrs...>{}))) {
		return begin(::std::index_sequence_for<Ctrs...>{});
	}
	constexpr const_iterator begin() const noexcept(noexcept(begin(::std::index_sequence_for<Ctrs...>{}))) {
		return begin(::std::index_sequence_for<Ctrs...>{});
	}
	constexpr const_iterator cbegin() const noexcept(noexcept(begin(::std::index_sequence_for<Ctrs...>{}))) {
		return begin(::std::index_sequence_for<Ctrs...>{});
	}

	constexpr auto end() noexcept(noexcept(end(::std::index_sequence_for<Ctrs...>{}))) {
		return end(::std::index_sequence_for<Ctrs...>{});
	}
	constexpr auto end() const noexcept(noexcept(end(::std::index_sequence_for<Ctrs...>{}))) {
		return end(::std::index_sequence_for<Ctrs...>{});
	}
	constexpr auto cend() const noexcept(noexcept(end(::std::index_sequence_for<Ctrs...>{}))) {
		return end(::std::index_sequence_for<Ctrs...>{});
	}

	constexpr bool empty() const noexcept { return empty(::std::index_sequence_for<Ctrs...>{}); }
};
template<class F, class... Ctrs> transform(F, Ctrs &&...) -> transform<F, Ctrs...>;

template<class F, class Ctr> class transform<F, Ctr> {
	F f;
	Ctr ctr;
	using end_type_t = stop_iteration<end_type_t<Ctr>>;
	using const_end_type_t = stop_iteration<const_end_type_t<Ctr>>;
public:
	constexpr transform(F &&f, Ctr &&ctr) noexcept(noexcept(::std::is_nothrow_constructible_v<F, F &&> && ::std::is_nothrow_constructible_v<Ctr, Ctr &&>))
		: f(::std::forward<F>(f)), ctr(::std::forward<Ctr>(ctr)) {}
	constexpr transform(transform &&) noexcept(::std::is_nothrow_move_constructible_v<F> && ::std::is_nothrow_move_constructible_v<Ctr>) = default;
	constexpr transform(transform const &) noexcept(::std::is_nothrow_copy_constructible_v<F> && ::std::is_nothrow_copy_constructible_v<Ctr>) = default;

	transform &operator=(transform that) noexcept(NOEXCEPT(value_assign(f, ::std::move(that.f), ctr, ::std::move(that.ctr)))) {
		return value_assign(f, ::std::move(that.f), ctr, ::std::move(that.ctr)), *this;
	}

	constexpr auto begin() noexcept(NOEXCEPT(transform_iterator(f, adl_or_std::begin::call(ctr)))) { return transform_iterator(f, adl_or_std::begin::call(ctr)); }
	constexpr auto begin() const noexcept(NOEXCEPT(transform_iterator(f, adl_or_std::begin::call(ctr)))) {
		return transform_iterator(f, adl_or_std::begin::call(ctr));
	}
	constexpr auto cbegin() const noexcept(NOEXCEPT(transform_iterator(f, adl_or_std::begin::call(ctr)))) {
		return transform_iterator(f, adl_or_std::begin::call(ctr));
	}

	constexpr end_type_t end() noexcept(noexcept(end_type_t{adl_or_std::end::call(ctr)})) { return end_type_t{adl_or_std::end::call(ctr)}; }
	constexpr const_end_type_t end() const noexcept(noexcept(const_end_type_t{adl_or_std::end::call(ctr)})) { return const_end_type_t{adl_or_std::end::call(ctr)}; }
	constexpr const_end_type_t cend() const noexcept(noexcept(const_end_type_t{adl_or_std::end::call(ctr)})) { return const_end_type_t{adl_or_std::end::call(ctr)}; }

	constexpr bool empty() const noexcept(adl_or_std::empty::is_noexcept<Ctr>()) { return adl_or_std::empty::call(ctr); }
};

template<class Ctr, class F, class... Fs> class flatten {
	Ctr ctr;
	tuple<F, Fs...> fs;

	using Left = decltype(adl_or_std::begin::call(ctr));
	using ConstLeft = decltype(adl_or_std::begin::call(::std::as_const(ctr)));
	using Right = decltype(adl_or_std::end::call(ctr));
	using ConstRight = decltype(adl_or_std::end::call(::std::as_const(ctr)));

	using iterator = flat_iterator<Left, Right, F, Fs...>;
	using const_iterator = flat_iterator<ConstLeft, ConstRight, F, Fs...>;
public:
	using This = flatten;

	constexpr flatten() = default;
	constexpr flatten(Ctr ctr, F f, Fs &&...fs): ctr(::std::forward<Ctr>(ctr)), fs(::std::forward<F>(f), ::std::forward<Fs>(fs)...) {}
	constexpr flatten(flatten &&) = default;
	constexpr flatten(flatten const &) = default;

	constexpr flatten &operator=(flatten that) noexcept(NOEXCEPT(value_assign(fs, ::std::move(that.fs), ctr, ::std::move(that.ctr)))) {
		return value_assign(fs, ::std::move(that.fs), ctr, ::std::move(that.ctr)), *this;
	}

	constexpr auto begin() noexcept(NOEXCEPT(iterator(adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr), fs))) {
		return iterator(adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr), fs);
	}
	constexpr auto begin() const noexcept(NOEXCEPT(const_iterator(adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr), fs))) {
		return const_iterator(adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr), fs);
	}
	constexpr auto cbegin() const noexcept(NOEXCEPT(const_iterator(adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr), fs))) {
		return const_iterator(adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr), fs);
	}

	constexpr auto end() noexcept(NOEXCEPT(stop_iteration{adl_or_std::end::call(ctr)})) {
		return stop_iteration{adl_or_std::end::call(ctr)};
	}
	constexpr auto end() const noexcept(NOEXCEPT(stop_iteration{adl_or_std::end::call(ctr)})) {
		return stop_iteration{adl_or_std::end::call(ctr)};
	}
	constexpr auto cend() const noexcept(NOEXCEPT(stop_iteration{adl_or_std::end::call(ctr)})) {
		return stop_iteration{adl_or_std::end::call(ctr)};
	}

	constexpr bool empty() const noexcept(NOEXCEPT(adl_or_std::empty::call(ctr))) { return adl_or_std::empty::call(ctr); }

	/*
	constexpr This &flatmap() & noexcept { return *this; }
	constexpr This const &flatmap() const & noexcept { return *this; }

	template<class G> flatten<This, G> constexpr flatmap(G &&g) & noexcept(noexcept(flatten<This, G>(*this, ::std::forward<G>(g)))) {
		return flatten<This, G>(*this, ::std::forward<G>(g));
	}
	template<class G, class... Gs> auto constexpr flatmap(G &&g, Gs &&...gs) &
		noexcept(noexcept(flatten<This, G>(*this, ::std::forward<G>(g)).flatmap(::std::forward<Gs>(gs)...)))
	{
		return flatten<This, G>(*this, ::std::forward<G>(g)).flatmap(::std::forward<Gs>(gs)...);
	}

	template<class G> flatten<This, G> constexpr flatmap(G &&g) const & noexcept(noexcept(flatten<This, G>(*this, ::std::forward<G>(g)))) {
		return flatten<This, G>(*this, ::std::forward<G>(g));
	}
	template<class G, class... Gs> auto constexpr flatmap(G &&g, Gs &&...gs) const &
		noexcept(noexcept(flatten<This, G>(*this, ::std::forward<G>(g)).flatmap(::std::forward<Gs>(gs)...)))
	{
		return flatten<This, G>(*this, ::std::forward<G>(g)).flatmap(::std::forward<Gs>(gs)...);
	}

	template<class G> flatten<This, G> constexpr flatmap(G &&g) && noexcept(noexcept(flatten<This, G>(::std::move(*this), ::std::forward<G>(g)))) {
		return flatten<This, G>(::std::move(*this), ::std::forward<G>(g));
	}
	template<class G, class... Gs> auto constexpr flatmap(G &&g, Gs &&...gs) &&
		noexcept(noexcept(flatten<This, G>(::std::move(*this), ::std::forward<G>(g)).flatmap(::std::forward<Gs>(gs)...)))
	{
		return flatten<This, G>(::std::move(*this), ::std::forward<G>(g)).flatmap(::std::forward<Gs>(gs)...);
	}
	*/
};
template<class Ctr, class F, class... Fs> flatten(Ctr, F, Fs...) -> flatten<Ctr, F, Fs...>;

template<class Ctr, class... Fs> constexpr auto flatmap(Ctr &&ctr, Fs &&...fs);

namespace impl {

template<class T, class... Fs> class can_flatmap: protected sfinae {
	using sfinae::can; template<class U, class... Gs> static constexpr auto can(U &&u, Gs &&...gs) -> decltype(u.flatmap(gs...), Yes{});
public:
	static constexpr bool value = is_yes_v<decltype(can(::std::declval<T>(), ::std::declval<Fs>()...))>;
};
template<class T, class F> static constexpr bool can_flatmap_v = can_flatmap<T, F>::value;

template<class T, class... Fs> struct is_noexcept_direct_flatmap: ::std::bool_constant<noexcept(::std::declval<T>().flatmap(::std::declval<Fs>()...))> {};
template<class T, class... Fs> inline constexpr auto is_noexcept_direct_flatmap_v = value_v<is_noexcept_direct_flatmap<T, Fs...>>;
template<class T> struct is_noexcept_direct_flatmap<T>: ::std::true_type {};

template<class Ctr> inline constexpr decltype(auto) direct_flatmap(Ctr &&ctr) noexcept { return ::std::forward<Ctr>(ctr); }
template<class Ctr, class... Fs> inline constexpr auto direct_flatmap(Ctr &&ctr, Fs &&...fs) noexcept(is_noexcept_direct_flatmap_v<Ctr, Fs...>) {
	return ::std::forward<Ctr>(ctr).flatmap(::std::forward<Fs>(fs)...);
}

template<class Ctr> inline constexpr decltype(auto) default_flatmap(Ctr &&ctr) noexcept { return ::std::forward<Ctr>(ctr); }

template<::std::size_t... is, class Ctr, class Fs> inline constexpr decltype(auto) flatmap_with_few_first_impl(::std::index_sequence<is...>, Ctr &&ctr, Fs &&fs)
	noexcept(NOEXCEPT(::std::forward<Ctr>(ctr).flatmap(::std::get<is>(fs)...)))
{
	return ::std::forward<Ctr>(ctr).flatmap(::std::get<is>(fs)...);
}
template<::std::size_t n, class Ctr, class... Fs> inline constexpr decltype(auto) flatmap_with_few_first(Ctr &&ctr, Fs &&...fs)
	noexcept(NOEXCEPT(flatmap_with_few_first_impl(::std::make_index_sequence<n>{}, ::std::forward<Ctr>(ctr), ::std::forward_as_tuple(::std::forward<Fs>(fs)...))))
{
	return flatmap_with_few_first_impl(::std::make_index_sequence<n>{}, ::std::forward<Ctr>(ctr), ::std::forward_as_tuple(::std::forward<Fs>(fs)...));
}

template<::std::size_t n, class Ctr, class F, class... Fs> inline constexpr auto flatten_suffix(Ctr &&ctr, F &&f, Fs &&...fs)
	/*
	noexcept(n == 0? NOEXCEPT(flatten(::std::forward<Ctr>(ctr), ::std::forward<F>(f), ::std::forward<Fs>(fs)...))
		 : NOEXCEPT(flatten_suffix<n - 1>(::std::forward<Ctr>(ctr), ::std::forward<Fs>(fs)...)))
		 */
{
	if constexpr(n == 0) return flatten(::std::forward<Ctr>(ctr), ::std::forward<F>(f), ::std::forward<Fs>(fs)...);
	else return flatten_suffix<n - 1>(::std::forward<Ctr>(ctr), ::std::forward<Fs>(fs)...);
}

}

template<class Ctr, class F, class... Fs> inline constexpr auto flatmap(Ctr &&ctr, F &&f, Fs &&...fs) {
	if constexpr(sizeof...(Fs)) {
		using flatmapped = pack::longest_addendum_t<impl::can_flatmap<Ctr>, F, Fs...>;
		using self_service = pack::tail<typename flatmapped::first_type>;	// types swallowed by Ctr::flatmap
		using defaulted = typename flatmapped::second_type;
		constexpr ::std::size_t self_flatmapped = pack::size_v<self_service>;
		if constexpr(self_flatmapped > 0) {
			if constexpr(pack::null_v<defaulted>) return ::std::forward<Ctr>(ctr).flatmap(::std::forward<F>(f), ::std::forward<Fs>(fs)...);
			else return impl::flatten_suffix<self_flatmapped>(
				impl::flatmap_with_few_first<self_flatmapped>(::std::forward<Ctr>(ctr), ::std::forward<F>(f), ::std::forward<Fs>(fs)...)
				, ::std::forward<Fs>(fs)...);
		}
		else return flatten(::std::forward<Ctr>(ctr), ::std::forward<F>(f), ::std::forward<Fs>(fs)...);
	}
	else if constexpr(impl::can_flatmap_v<Ctr, F>) return ::std::forward<Ctr>(ctr).flatmap(::std::forward<F>(f));
	else return flatten(::std::forward<Ctr>(ctr), ::std::forward<F>(f));
}

template<class...> struct concat;
template<class Ctr1, class Ctr2, class... Ctrs> struct concat<Ctr1, Ctr2, Ctrs...> {
	constexpr concat() {}
	constexpr concat(Ctr1 &&c1, Ctr2 &&c2, Ctrs &&...cs): c1(::std::forward<Ctr1>(c1)), aftermath(::std::forward<Ctr2>(c2), std::forward<Ctrs>(cs)...) {}

	constexpr auto begin() { return concat_iterator(adl_or_std::begin::call(c1), adl_or_std::end::call(c1), aftermath); }
	constexpr auto begin() const { return concat_iterator(adl_or_std::begin::call(c1), adl_or_std::end::call(c1), aftermath); }
	constexpr auto cbegin() const { return concat_iterator(adl_or_std::begin::call(c1), adl_or_std::end::call(c1), aftermath); }

	constexpr auto end() const noexcept { return sentinel{}; }
	constexpr auto cend() const noexcept { return sentinel{}; }
private:
	using Aftermath = ::std::conditional_t<sizeof...(Ctrs) == 0, Ctr2, concat<Ctr2, Ctrs...>>;

	Ctr1 c1;
	Aftermath aftermath;
};
template<class Ctr1, class Ctr2, class... Ctrs> concat(Ctr1 &&, Ctr2 &&, Ctrs &&...) -> concat<Ctr1, Ctr2, Ctrs...>;

template<class Head, class Tail> inline constexpr auto cons(Head &&head, Tail &&tail)
	noexcept(NOEXCEPT(concat(optional{::std::forward<Head>(head)}, ::std::forward<Tail>(tail))))
{
	return concat(optional{::std::forward<Head>(head)}, ::std::forward<Tail>(tail));
}

template<class Init, class Last> inline constexpr auto push_back(Init &&init, Last &&last)
	noexcept(NOEXCEPT(concat(::std::forward<Init>(init), optional{::std::forward<Last>(last)})))
{
	return concat(::std::forward<Init>(init), optional{::std::forward<Last>(last)});
}

template<class Predicate, class Ctr> struct filter {
	using This = filter;

	using iterator = filter_iterator<Predicate &, iterator_type_t<Ctr>, end_type_t<Ctr>>;
	using const_iterator = filter_iterator<Predicate const &, const_iterator_type_t<Ctr>, end_type_t<::std::add_const_t<Ctr>>>;

	constexpr filter() = default;
	constexpr filter(Predicate &&predicate, Ctr &&ctr) noexcept(::std::is_nothrow_move_constructible_v<Predicate> && ::std::is_nothrow_move_constructible_v<Ctr>)
		: predicate(::std::forward<Predicate>(predicate)), ctr(::std::forward<Ctr>(ctr)) {}
	constexpr filter(filter &&) = default;
	constexpr filter(filter const &) = default;

	constexpr iterator begin() noexcept(NOEXCEPT(iterator(predicate, adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr)))) {
		return {predicate, adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr)};
	}
	constexpr const_iterator begin() const noexcept(NOEXCEPT(const_iterator(predicate, adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr)))) {
		return {predicate, adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr)};
	}
	constexpr const_iterator cbegin() const noexcept(NOEXCEPT(const_iterator(predicate, adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr)))) {
		return {predicate, adl_or_std::begin::call(ctr), adl_or_std::end::call(ctr)};
	}

	constexpr auto end() const noexcept { return sentinel{}; }
	constexpr auto cend() const noexcept { return sentinel{}; }
private:
	Predicate predicate;
	Ctr ctr;
};
template<class Predicate, class Ctr> filter(Predicate &&, Ctr &&) -> filter<Predicate, Ctr>;

template<class Predicate, class Ctr> struct take_while {
	using This = take_while;

	using iterator = take_while_iterator<Predicate &, iterator_type_t<Ctr>>;
	using const_iterator = take_while_iterator<Predicate const &, const_iterator_type_t<Ctr>>;

	constexpr take_while() = default;
	constexpr take_while(Predicate &&predicate, Ctr &&ctr) noexcept(::std::is_nothrow_move_constructible_v<Predicate> && ::std::is_nothrow_move_constructible_v<Ctr>)
		: predicate(::std::forward<Predicate>(predicate)), ctr(::std::forward<Ctr>(ctr)) {}

	constexpr iterator begin() noexcept(NOEXCEPT(iterator(predicate, adl_or_std::begin::call(ctr)))) { return {predicate, adl_or_std::begin::call(ctr)}; }
	constexpr const_iterator begin() const noexcept(NOEXCEPT(const_iterator(predicate, adl_or_std::begin::call(ctr)))) {
		return {predicate, adl_or_std::begin::call(ctr)};
	}
	constexpr auto end() noexcept(NOEXCEPT(adl_or_std::end::call(ctr))) { return adl_or_std::end::call(ctr); }
	constexpr auto end() const noexcept(NOEXCEPT(adl_or_std::end::call(ctr))) { return adl_or_std::end::call(ctr); }
private:
	Predicate predicate;
	Ctr ctr;
};
template<class Predicate, class Ctr> take_while(Predicate &&, Ctr &&) -> take_while<Predicate, Ctr>;

template<class Predicate, class Ctr> struct drop_while {
	using Cache = optional<decltype(*::std::declval<iterator_type_t<Ctr>>())>;
public:
	using This = drop_while;

	constexpr drop_while() = default;
	constexpr drop_while(Predicate &&predicate, Ctr &&ctr) noexcept(::std::is_nothrow_move_constructible_v<Predicate> && ::std::is_nothrow_move_constructible_v<Ctr>)
		: predicate(::std::forward<Predicate>(predicate)), ctr(::std::forward<Ctr>(ctr)) {}

	constexpr auto begin() {
		auto stop{adl_or_std::end::call(ctr)};
		find(stop);
		return concat_iterator(cache.begin(), cache.end(), range(*second, stop));
	}
	constexpr auto begin() const {
		auto stop{adl_or_std::end::call(ctr)};
		const_cast<This *>(this)->find(stop);
		return concat_iterator(cache.begin(), cache.end(), range(*second, stop));
	}
	constexpr auto cbegin() const {
		auto stop{adl_or_std::end::call(ctr)};
		const_cast<This *>(this)->find(stop);
		return concat_iterator(cache.begin(), cache.end(), range(*second, stop));
	}

	constexpr auto end() const noexcept { return sentinel{}; }
	constexpr auto cend() const noexcept { return sentinel{}; }
private:
	constexpr void find(end_type_t<Ctr> stop) {
		if(second.empty()) {
			auto retVal{adl_or_std::begin::call(ctr)};
			while(retVal != stop && ::std::invoke(predicate, cache.emplace(*retVal))) ++retVal;
			if(retVal == stop) cache.reset();
			else ++retVal;
			second.emplace(::std::move(retVal));
		}
	}
	Predicate predicate;
	Ctr ctr;
	Cache cache;
	optional<iterator_type_t<Ctr>> second;
};
template<class Predicate, class Ctr> drop_while(Predicate &&, Ctr &&) -> drop_while<Predicate, Ctr>;

template<class Ctr> struct take {
	using This = take;

	using iterator = take_iterator<iterator_type_t<Ctr>>;
	using const_iterator = take_iterator<const_iterator_type_t<Ctr>>;

	constexpr take() = default;
	constexpr take(::std::size_t n, Ctr &&ctr) noexcept(::std::is_nothrow_move_constructible_v<Ctr>): n(n), ctr(::std::forward<Ctr>(ctr)) {}

	constexpr iterator begin() noexcept(NOEXCEPT(iterator(n, adl_or_std::begin::call(ctr)))) { return {n, adl_or_std::begin::call(ctr)}; }
	constexpr const_iterator begin() const noexcept(NOEXCEPT(const_iterator(n, adl_or_std::begin::call(ctr)))) { return {n, adl_or_std::begin::call(ctr)}; }
	constexpr const_iterator cbegin() const noexcept(NOEXCEPT(const_iterator(n, adl_or_std::begin::call(ctr)))) { return {n, adl_or_std::begin::call(ctr)}; }

	constexpr auto end() noexcept(NOEXCEPT(adl_or_std::end::call(ctr))) { return adl_or_std::end::call(ctr); }
	constexpr auto end() const noexcept(NOEXCEPT(adl_or_std::end::call(ctr))) { return adl_or_std::end::call(ctr); }
	constexpr auto cend() const noexcept(NOEXCEPT(adl_or_std::end::call(ctr))) { return adl_or_std::end::call(ctr); }
private:
	::std::size_t n;
	Ctr ctr;
};
template<class Ctr> take(::std::size_t, Ctr &&) -> take<Ctr>;

template<class T, ::std::size_t n> class ring_array {
	using Impl = ::std::array<T, n + 1>;
	static constexpr ::std::size_t modulo(::std::size_t i) noexcept { return i % (n + 1); }
	static constexpr ::std::size_t modN(::std::size_t i) noexcept { return i < n - 1? modulo(i) : n + 1 - modulo(-i); }
	static constexpr ::std::size_t leftover = modulo(max_size_v) + 1;

	static constexpr ::std::size_t add(::std::size_t left, ::std::size_t right) noexcept {
		::std::size_t retVal{left + right};
		if(retVal < left) retVal += leftover;
		return modulo(retVal);
	}
	static constexpr ::std::size_t sub(::std::size_t left, ::std::size_t right) noexcept {
		::std::size_t retVal{left - right};
		if(retVal > left) retVal -= leftover;
		return modulo(retVal);
	}

	template<class Base, class Ref, class Ptr> struct Iterator {
		using This = Iterator;
		using iterator_category = ::std::random_access_iterator_tag;
		using value_type = T;
		using difference_type = ::std::size_t;
		using pointer = Ptr;
		using reference = Ref;

		constexpr Iterator() = default;
		constexpr Iterator(This &&) = default;
		constexpr Iterator(This const &) = default;

		constexpr This &operator=(This &&) = default;
		constexpr This &operator=(This const &) = default;

		constexpr This &operator+=(difference_type m) noexcept { return i = add(i, m), *this; }
		constexpr This operator+(difference_type m) const noexcept { return This(add(i, m), parent), *this; }
		constexpr This &operator-=(difference_type m) noexcept { return i = sub(i, m), *this; }
		constexpr This operator-(difference_type m) const noexcept { return This(sub(i, m), parent), *this; }

		constexpr difference_type operator-(This const &that) const noexcept {
			return sub(i, that.i);
		}

		constexpr reference operator[](difference_type m) const noexcept { return parent->impl[add(i, m)]; }

		constexpr bool operator<(This const &that) const noexcept {
			return i >= parent->base? i < that.i || that.i < parent->base : i < that.i && that.i < parent->base;
		}
		constexpr bool operator>(This const &that) const noexcept { return that < *this; }
		constexpr bool operator<=(This const &that) const noexcept { return !(that < *this); }
		constexpr bool operator>=(This const &that) const noexcept { return !(*this < that); }

		constexpr This &operator--() noexcept { return (i? --i : i = n), *this; }
		constexpr This operator--(int) noexcept { This retVal(*this); return (i? --i : i = n), retVal; }

		constexpr This &operator++() noexcept { return i < n? ++i : i = 0, *this; }
		constexpr This operator++(int) noexcept { This retVal(*this); return i < n? ++i : i = 0, retVal; }

		constexpr bool operator==(This const &that) const noexcept { return modulo(i - that.i) == 0; }
		constexpr bool operator!=(This const &that) const noexcept { return modulo(i - that.i) != 0; }

		constexpr reference operator*() const noexcept { return parent->impl[i]; }
		constexpr pointer operator->() const noexcept { return &parent->impl[i]; }
	private:
		friend class ring_array;
		constexpr Iterator(difference_type i, Base *parent) noexcept: i(i), parent(parent) {}
		difference_type i;
		Base *parent;
	};
public:
	using This = ring_array;
	using value_type = typename Impl::value_type;
	using size_type = typename Impl::size_type;
	using difference_type = typename Impl::difference_type;
	using reference = typename Impl::reference;
	using const_reference = typename Impl::const_reference;
	using pointer = typename Impl::pointer;
	using const_pointer = typename Impl::const_pointer;
	using iterator = Iterator<This, reference, pointer>;
	using const_iterator = Iterator<const This, const_reference, const_pointer>;

	constexpr ring_array() noexcept(::std::is_nothrow_constructible_v<Impl>): impl{} {}
	ring_array(ring_array &&that) noexcept(noexcept(ring_array(::std::move(that), ::std::make_index_sequence<n>{})))
		: ring_array(::std::move(that), ::std::make_index_sequence<n>{}) {}
	ring_array(ring_array const &that) noexcept(noexcept(ring_array(that, ::std::make_index_sequence<n>{}))): ring_array(that, ::std::make_index_sequence<n>{}) {}

	template<class... Us> ring_array(Us &&...us) noexcept(::std::is_nothrow_constructible_v<Impl, Us...>): impl{::std::forward<Us>(us)...} {}

	ring_array &operator=(ring_array &&) = default;
	ring_array &operator=(ring_array const &) = default;

	constexpr reference at(size_type pos) noexcept { return impl[modulo(pos + base)]; }
	constexpr const_reference at(size_type pos) const noexcept { return impl[modulo(pos + base)]; }
	constexpr reference operator[](size_type pos) noexcept { return impl[modulo(pos + base)]; }
	constexpr const_reference operator[](size_type pos) const noexcept { return impl[modulo(pos + base)]; }

	constexpr reference front() noexcept { return impl[base]; }
	constexpr const_reference front() const noexcept { return impl[base]; }
	constexpr reference back() noexcept { return impl[prev? prev - 1 : n]; }
	constexpr const_reference back() const noexcept { return impl[prev? prev - 1 : n]; }

	constexpr iterator begin() noexcept { return iterator(base, this); }
	constexpr const_iterator begin() const noexcept { return const_iterator(base, this); }
	constexpr const_iterator cbegin() const noexcept { return const_iterator(base, this); }
	constexpr iterator end() noexcept { return iterator(prev, this); }
	constexpr const_iterator end() const noexcept { return const_iterator(prev, this); }
	constexpr const_iterator cend() const noexcept { return const_iterator(prev, this); }

	constexpr bool empty() const noexcept { return !n; }
	constexpr size_type size() const noexcept { return n; }
	constexpr size_type max_size() const noexcept { return n; }

	constexpr void fill(T const &value) noexcept(noexcept(impl.fill(value))) { impl.fill(value); }
	constexpr void swap(This &that) noexcept(noexcept(impl.swap(that.impl))) { impl.swap(that.impl), ::std::swap(base, that.base), ::std::swap(prev, that.prev); }

	constexpr void push_back(T t) noexcept(::std::is_nothrow_move_assignable_v<T>) {
		if(base < n) {
			if(prev < n) impl[prev] = ::std::move(t), ++prev;
			else impl[n] = ::std::move(t), prev = 0;
			++base;
		}
		else {
			impl[n - 1] = ::std::move(t);
			base = 0;
			prev = n;
		}
	}
	template<typename... Args> constexpr reference emplace_back(Args &&...args)
		noexcept(::std::is_nothrow_constructible_v<T, Args...> && ::std::is_nothrow_move_assignable_v<T>)
	{
		reference retVal = impl[prev];
		if(base < n) {
			if(prev < n) {
				impl[prev] = T(::std::forward<Args>(args)...);
				++prev, ++base;
			}
			else {
				impl[n] = T(::std::forward<Args>(args)...);
				prev = 0, base = 1;
			}
		}
		else {
			impl[n - 1] = T(::std::forward<Args>(args)...);
			base = 0;
			prev = n;
		}
		return retVal;
	}

	template<::std::size_t i> constexpr T &&get() && noexcept { return impl[modulo(base + i)]; }
	template<::std::size_t i> constexpr T const &&get() const && noexcept { return impl[modulo(base + i)]; }
	template<::std::size_t i> constexpr T &get() & noexcept { return impl[modulo(base + i)]; }
	template<::std::size_t i> constexpr T const &get() const & noexcept { return impl[modulo(base + i)]; }
private:
	template<typename Tp, ::std::size_t... is> constexpr ring_array(Tp &&t, ::std::index_sequence<is...>): impl{nstd::get<is>(::std::forward<Tp>(t))...} {}

	Impl impl;
	size_type base = 0;
	size_type prev = n;
};
template<typename H, typename... Ts> ring_array(H &&, Ts &&...) -> ring_array<H, sizeof...(Ts) + 1>;
ring_array() -> ring_array<void, 0>;

template<typename T, ::std::size_t n, typename Itr> constexpr typename ring_array<T, n>::template Iterator<Itr> operator+(
	typename ring_array<T, n>::template Iterator<Itr>::difference_type m, typename ring_array<T, n>::template Iterator<Itr> const &i) noexcept(NOEXCEPT(i + m))
{
	return i + m;
}
template<typename T, ::std::size_t n, typename Itr> constexpr typename ring_array<T, n>::template Iterator<Itr> operator-(
	typename ring_array<T, n>::template Iterator<Itr>::difference_type m, typename ring_array<T, n>::template Iterator<Itr> const &i) noexcept(NOEXCEPT(i - m))
{
	return i - m;
}

template<class T, ::std::size_t n> struct tuple_impl::is_tuple<ring_array<T, n>>: ::std::true_type {};
template<class T, ::std::size_t n> struct tuple_impl::tuple_size<ring_array<T, n>>: size_constant<n> {};
template<::std::size_t i, class T, ::std::size_t n> struct tuple_impl::tuple_element<i, ring_array<T, n>>: box<T> {};

namespace unsafe {

template<class T, ::std::size_t n> struct getter<ring_array<T, n>> {
	template<::std::size_t i> static constexpr decltype(auto) call(ring_array<T, n> &&t) noexcept { return t[i]; }
	template<::std::size_t i> static constexpr decltype(auto) call(ring_array<T, n> const &&t) noexcept { return t[i]; }
	template<::std::size_t i> static constexpr decltype(auto) call(ring_array<T, n> &t) noexcept { return t[i]; }
	template<::std::size_t i> static constexpr decltype(auto) call(ring_array<T, n> const &t) noexcept { return t[i]; }
};

}

template<class F, class T, ::std::size_t n, bool terminated = pack::apply_v<::std::is_nothrow_invocable, F, pack::replicate_t<n, T>>>
struct recurrence_relation {
	using This = recurrence_relation;
	constexpr recurrence_relation() = default;
	constexpr recurrence_relation(This &&) = default;
	constexpr recurrence_relation(This const &) = default;

	template<typename... Ts> constexpr recurrence_relation(F &&f, Ts &&...ts)
		noexcept(::std::is_nothrow_move_constructible_v<F> && ::std::is_nothrow_constructible_v<wave, Ts...>)
		: f(::std::forward<F>(f)), wave(::std::forward<Ts>(ts)...) {}

	constexpr decltype(auto) operator*() const noexcept(NOEXCEPT(**lastRun) && NOEXCEPT(wave.front())) { return lastRun? **lastRun : wave.front(); }

	This &operator++() noexcept(noexcept(++lastRun) && noexcept(lastRun = ++wave.cbegin())) {
		if(lastRun) ++lastRun;
		else {
			try { wave.push_back(apply(f, wave)); }
			catch(...) { lastRun = ++wave.cbegin(); }
		}
		return *this;
	}

	constexpr This &begin() noexcept { return *this; }
	constexpr This &begin() const noexcept { return *const_cast<This *>(this); }
	constexpr This &cbegin() const noexcept { return *const_cast<This *>(this); }

	constexpr sentinel end() noexcept { return {}; }
	constexpr sentinel end() const noexcept { return {}; }
	constexpr sentinel cend() const noexcept { return {}; }

	constexpr bool operator==(sentinel) const noexcept { return lastRun == wave.cend(); }
	constexpr bool operator!=(sentinel) const noexcept { return lastRun.empty() || lastRun != wave.cend(); }
private:
	F f;
	ring_array<T, n> wave;
	optional<typename ring_array<T, n>::const_iterator> lastRun;
};
template<typename F, typename T, typename... Ts> recurrence_relation(F &&, T &&, Ts &&...) -> recurrence_relation<F, T, sizeof...(Ts) + 1>;

template<typename F, typename T, ::std::size_t n> struct recurrence_relation<F, T, n, false> {
	using This = recurrence_relation;

	constexpr recurrence_relation() = default;
	constexpr recurrence_relation(This &&) = default;
	constexpr recurrence_relation(This const &) = default;

	template<typename... Ts> constexpr recurrence_relation(F &&f, Ts &&...ts)
		noexcept(::std::is_nothrow_move_constructible_v<F> && ::std::is_nothrow_constructible_v<wave, Ts...>)
		: f(::std::forward<F>(f)), wave(::std::forward<Ts>(ts)...) {}

	constexpr decltype(auto) operator*() const noexcept(NOEXCEPT(wave.front())) { return wave.front(); }

	constexpr This &operator++() noexcept(NOEXCEPT(wave.push_back(apply(f, wave)))) { return wave.push_back(apply(f, wave)), *this; }

	constexpr This &begin() noexcept { return *this; }
	constexpr This &begin() const noexcept { return *const_cast<This *>(this); }
	constexpr This &cbegin() const noexcept { return *const_cast<This *>(this); }

	constexpr infinity end() noexcept { return {}; }
	constexpr infinity end() const noexcept { return {}; }
	constexpr infinity cend() const noexcept { return {}; }
private:
	F f;
	ring_array<T, n> wave;
};
template<typename F, typename T, typename... Ts> recurrence_relation(F &&, T &&, Ts &&...) -> recurrence_relation<F, T, sizeof...(Ts) + 1>;

}

#ifndef SHOW_NOTHING

#include "show.h"

namespace nstd {

SHOW_NSTD_ROOT(1, range);
SHOW_NSTD_ROOT(1, zip);
SHOW_NSTD_ROOT(1, mzip);
SHOW_NSTD_ROOT(1, transform);
SHOW_NSTD_ROOT(1, flatten);
SHOW_NSTD_ROOT(1, concat);
SHOW_NSTD_ROOT(1, filter);
SHOW_NSTD_ROOT(1, take_while);
SHOW_NSTD_ROOT(1, drop_while);
SHOW_NSTD_ROOT(1, take);
SHOW_NSTD_ROOT(12, ring_array);
SHOW_NSTD_ROOT(112, recurrence_relation);

}

#endif

#endif
