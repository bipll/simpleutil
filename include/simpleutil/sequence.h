/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__SEQUENCE_H_3e4bd85e1c1d89457ad7bceb4dd68f1959f25990
#define SIMPLEUTIL__SEQUENCE_H_3e4bd85e1c1d89457ad7bceb4dd68f1959f25990

#include <type_traits>
#include <utility>
#include <iterator>
#include <vector>
#include "iterator.h"
#include "operator.h"
#include "typeutil.h"
#include "functional.h"
#include "functor.h"
#include "polymorphic_object.h"

namespace nstd {

struct stopped {
	template<class Itr> constexpr bool operator()(Itr &&itr) const noexcept(noexcept(bool(itr == sentinel{}))) { return itr == sentinel{}; }
};

struct value_deref {
	template<class Itr> constexpr auto operator()(Itr &&itr) const noexcept(NOEXCEPT(*::std::forward<Itr>(itr))) { return *::std::forward<Itr>(itr); }
};

template<typename Element> struct forward_iterator: polymorphic_object<typename functor<optional<Element>>::iterator, mute<preinc>, value_deref, stopped> {
	using This = forward_iterator;
	using Parent = polymorphic_object<typename functor<optional<Element>>::iterator, mute<preinc>, value_deref, stopped>;

        using iterator_category = ::std::forward_iterator_tag;
        using value_type = Element;
        using difference_type = ::std::ptrdiff_t;
        using pointer = Element *;
        using reference = Element &;

	constexpr forward_iterator() = default;
	constexpr forward_iterator(forward_iterator &&that) = default;
	constexpr forward_iterator(forward_iterator const &that) = default;

	template<typename Itr> explicit constexpr forward_iterator(Itr &&that)
		noexcept(::std::is_nothrow_constructible_v<Parent, Itr, mute<preinc>, value_deref, stopped>)
		: Parent(::std::forward<Itr>(that), mute<preinc>{}, value_deref{}, stopped{}) {}

	constexpr forward_iterator &operator=(forward_iterator &&) = default;
	constexpr forward_iterator &operator=(forward_iterator const &) = default;
	template<typename Itr> constexpr forward_iterator &operator=(Itr &&that) noexcept(noexcept(Parent::operator=(::std::forward<Itr>(that)))) {
		return Parent::operator=(::std::forward<Itr>(that)), *this;
	}

	constexpr forward_iterator &operator++() { return Parent::template invoke<0>(), *this; }
	constexpr forward_iterator operator++(int) {
		forward_iterator retVal(::std::as_const(*this));
		return Parent::template invoke<0>(), retVal;
	}

	constexpr decltype(auto) operator*() { return Parent::template invoke<1>(); }
	constexpr decltype(auto) operator*() const { return Parent::template invoke<1>(); }

	constexpr bool operator==(sentinel) const { return Parent::template invoke<2>(); }
	constexpr bool operator!=(sentinel) const { return !Parent::template invoke<2>(); }
};
template<typename Itr> forward_iterator(Itr &&itr) -> forward_iterator<::std::decay_t<decltype(*itr)>>;

struct seq_begin {
	template<class Ctr> constexpr auto operator()(Ctr &&ctr) const noexcept(NOEXCEPT(forward_iterator(adl_or_std::begin::call(ctr)))) {
		return forward_iterator(adl_or_std::begin::call(ctr));
	}
};

template<typename Element> struct sequence: polymorphic_object<functor<optional<Element>>, seq_begin> {
	using This = sequence;
	using Parent = polymorphic_object<functor<optional<Element>>, seq_begin>;

	using Parent::invoke;

	constexpr sequence() = default;
	constexpr sequence(sequence &&) = default;
	constexpr sequence(sequence const &) = default;

	template<typename Ctr> constexpr sequence(functor<Ctr> &&ctr) noexcept(::std::is_nothrow_constructible_v<Parent, functor<Ctr>>): Parent(::std::move(ctr)) {}
	template<typename Ctr> constexpr sequence(functor<Ctr> const &ctr) noexcept(::std::is_nothrow_constructible_v<Parent, functor<Ctr> const &>): Parent(ctr) {}
	template<typename Ctr> constexpr sequence(Ctr &&ctr) noexcept(::std::is_nothrow_constructible_v<This, funtctor<Ctr>>)
		: sequence(functor(::std::forward<Ctr>(ctr))) {}
	constexpr sequence(::std::initializer_list<Element> elements) noexcept(::std::is_nothrow_constructible_v<This, ::std::vector<Element>>)
		: sequence(::std::vector(elements)) {}

	template<typename Ctr> constexpr sequence &operator=(Ctr &&that) noexcept(noexcept(Parent::operator=(::std::forward<Ctr>(that)), *this)) {
		return Parent::operator=(::std::forward<Ctr>(that)), *this;
	}
	constexpr sequence &operator=(sequence &&) = default;
	constexpr sequence &operator=(sequence const &) = default;

	constexpr auto begin() noexcept(NOEXCEPT(this->Parent::template invoke<0>())) { return Parent::template invoke<0>(); }
	constexpr auto begin() const noexcept(NOEXCEPT(this->Parent::template invoke<0>())) { return Parent::template invoke<0>(); }
	constexpr auto cbegin() const noexcept(NOEXCEPT(this->Parent::template invoke<0>())) { return Parent::template invoke<0>(); }

	constexpr sentinel end() const noexcept { return {}; }
	constexpr sentinel cend() const noexcept { return {}; }
};
template<typename Ctr> using element_type = ::std::decay_t<decltype(*adl_or_std::begin::call(::std::declval<Ctr>()))>;
template<typename Ctr> sequence(Ctr &&) -> sequence<element_type<Ctr>>;

template<typename Element, typename Sequence = sequence<Element>, typename Cache = ::std::vector<Element>> struct track;
template<typename Element> class track<Element, sequence<Element>, ::std::vector<Element>> {
	using Sequence = sequence<Element>;
	using Cache = ::std::vector<Element>;
	template<typename Tracked> struct Iterator {
		using This = Iterator;
		using iterator_category = ::std::input_iterator_tag;
		using value_type = Element;
		using difference_type = ::std::ptrdiff_t;
		using pointer = Element *;
		using reference = Element &;

		constexpr Iterator() = default;
		constexpr Iterator(Iterator &&) = default;
		constexpr Iterator(Iterator const &) = default;

		constexpr Iterator &operator=(Iterator &&) = default;
		constexpr Iterator &operator=(Iterator const &) = default;

		constexpr decltype(auto) operator*() noexcept(noexcept(follow()) && noexcept((*cache)[n])) { return follow(), (*cache)[n]; }
		constexpr decltype(auto) operator*() const noexcept(noexcept(follow()) && noexcept((*cache)[n])) { return follow(), (*cache)[n]; }

		constexpr This &operator++() noexcept { return ++n, *this; }
		constexpr This operator++(int) noexcept(::std::is_nothrow_copy_constructible_v<This> && ::std::is_nothrow_move_constructible_v<This>) {
			This retVal(*this);
			if constexpr(::std::is_nothrow_move_constructible_v<This>) return ++n, retVal;
			else try { return ++n, retVal; }
			catch(...) { --n; throw; }
		}

		constexpr This &operator--() noexcept { return --n, *this; }
		constexpr This operator--(int) noexcept(::std::is_nothrow_copy_constructible_v<This> && ::std::is_nothrow_move_constructible_v<This>) {
			This retVal(*this);
			if constexpr(::std::is_nothrow_move_constructible_v<This>) return --n, retVal;
			else try { return --n, retVal; }
			catch(...) { ++n; throw; }
		}

		constexpr bool operator==(sentinel) const noexcept(noexcept(follow())) { follow(); return n >= cache->size(); }
		constexpr bool operator!=(sentinel) const noexcept(noexcept(follow())) { follow(); return n < cache->size(); }
	private:
		friend class track;
		constexpr Iterator(Cache &cache, Tracked &itr) noexcept: cache(&cache), itr(&itr) {}

		//constexpr void follow() { while(n >= cache->size() && *itr != sentinel{}) cache->push_back(**itr), ++*itr; }
		constexpr void follow() const noexcept(noexcept(bool(*itr != sentinel{}), cache->push_back(**itr), ++*itr)) {
			while(n >= cache->size() && *itr != sentinel{}) cache->push_back(**itr), ++*itr;
		}

		Cache *cache;
		::std::size_t n = 0;
		Tracked *itr;
	};
	mutable Cache cache;
	Sequence tail;
	iterator_type_t<Sequence> theOnly = tail.begin();
	mutable const_iterator_type_t<Sequence> theOnlyTrue = tail.cbegin();
public:
	using This = track;
	using iterator = Iterator<iterator_type_t<Sequence>>;
	using const_iterator = Iterator<const_iterator_type_t<Sequence>>;

	constexpr track() noexcept(pack::all_v<::std::is_nothrow_constructible, Cache, Sequence, decltype(theOnly), decltype(theOnlyTrue)>) : theOnly(), theOnlyTrue() {}
	constexpr track(This &&) = default;
	constexpr track(This const &that) noexcept(::std::is_nothrow_copy_constructible_v<Sequence>): tail(that.tail) {}
	template<typename Ctr> constexpr track(Ctr &&ctr) noexcept(::std::is_nothrow_constructible_v<Sequence, Ctr>): tail(::std::forward<Ctr>(ctr)) {}

	constexpr track &operator=(This that) noexcept(are_nothrow_move_assignable_v<Cache, Sequence, decltype(theOnly), decltype(theOnlyTrue)>) {
		return value_assign(cache, ::std::move(that.cache), tail, ::std::move(that.tail)
				    , theOnly, ::std::move(that.theOnly), theOnlyTrue, ::std::move(that.theOnlyTrue))
			, *this;
	}

	template<typename Ctr> constexpr track &operator=(Ctr &&ctr)
		noexcept(NOEXCEPT(cache.clear(), tail = ::std::forward<Ctr>(ctr), theOnly = tail.begin(), theOnlyTrue = tail.cbegin(), *this))
	{
		tail = ::std::forward<Ctr>(ctr);
		cache.clear();
		return value_assign(theOnly, tail.begin(), theOnlyTrue, tail.cbegin()), *this;
	}

	constexpr iterator begin() noexcept(NOEXCEPT(iterator(cache, theOnly))) { return iterator(cache, theOnly); }
	constexpr const_iterator begin() const noexcept(NOEXCEPT(const_iterator(cache, theOnlyTrue))) { return const_iterator(cache, theOnlyTrue); }
	constexpr const_iterator cbegin() const noexcept(NOEXCEPT(const_iterator(cache, theOnlyTrue))) { return const_iterator(cache, theOnlyTrue); }

	constexpr sentinel end() noexcept { return {}; }
	constexpr sentinel end() const noexcept { return {}; }
	constexpr sentinel cend() const noexcept { return {}; }
};

}

#ifndef SHOW_NOTHING

#include "show.h"

namespace nstd {

SHOW_NSTD_ROOT(1, forward_iterator);
SHOW_NSTD_ROOT(1, sequence);
SHOW_NSTD_ROOT(1, track);

}

#endif

#endif
