/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__OPERATOR_H_545c769dcdd16871a6d0430177d0685a28c059a3
#define SIMPLEUTIL__OPERATOR_H_545c769dcdd16871a6d0430177d0685a28c059a3

#include <utility>
#include <type_traits>
#include "typeutil.h"
#include "tuple.h"

namespace nstd {

template<class Operator> struct mute {
	template<class... Args> static constexpr void call(Args &&...args) noexcept(noexcept(Operator::call(::std::forward<Args>(args)...))) {
		Operator::call(::std::forward<Args>(args)...);
	}
	template<class... Args> static constexpr void spread(Args &&...args) noexcept(noexcept(Operator::call(::std::forward<Args>(args)...))) {
		Operator::call(::std::forward<Args>(args)...);
	}
	template<class... Args> constexpr void operator()(Args &&...args) const noexcept(noexcept(Operator::call(::std::forward<Args>(args)...))) {
		Operator::call(::std::forward<Args>(args)...);
	}
};

template<template<class> class Operator> class unary_operator: protected sfinae {
	using sfinae::can;
	template<class Operand> static constexpr decltype(std::declval<Operator<Operand>>(), Yes{}) can(Operand &&);
public:
	static constexpr ::std::size_t arity = 1;
	template<class Operand> static constexpr bool compiles = is_yes_v<decltype(can(::std::declval<Operand>()))>;
};

template<template<class, class> class Operator> class binary_operator: protected sfinae {
	using sfinae::can;
	template<class Operand1, class Operand2> static constexpr decltype(std::declval<Operator<Operand1, Operand2>>(), Yes{}) can(Operand1 &&, Operand2 &&);
public:
	static constexpr ::std::size_t arity = 2;
	template<class Operand1, class Operand2> static constexpr bool compiles
		= is_yes_v<decltype(can(::std::declval<Operand1>(), ::std::declval<Operand2>()))>;
};

template<template<class, class, class> class Operator> class ternary_operator: protected sfinae {
	using sfinae::can;
	template<class Operand1, class Operand2, class Operand3>
		static constexpr decltype(std::declval<Operator<Operand1, Operand2, Operand3>>(), Yes{}) can(Operand1 &&, Operand2 &&, Operand3 &&);
public:
	static constexpr ::std::size_t arity = 3;
	template<class Operand1, class Operand2, class Operand3> static constexpr bool compiles
		= is_yes_v<decltype(can(::std::declval<Operand1>(), ::std::declval<Operand2>(), ::std::declval<Operand3>()))>;
};

class call_impl: private_namespace {
	template<class Op, class T, std::size_t... is> static decltype(auto) call(T &&t, std::index_sequence<is...>) noexcept(noexcept(Op::call(get<is>(t)...))) {
		return Op::call(get<is>(t)...);
	}
	template<class Op, class T> friend decltype(auto) call(T &&t)
		noexcept(noexcept(call_impl::call<Op>(::std::forward<T>(t), ::std::make_index_sequence<tuple_size_v<std::decay_t<T>>>{})));
};
template<class Op, class T> inline decltype(auto) call(T &&t)
	noexcept(noexcept(call_impl::call<Op>(std::forward<T>(t), std::make_index_sequence<tuple_size_v<std::decay_t<T>>>{})))
{
	return call_impl::call<Op>(std::forward<T>(t), std::make_index_sequence<tuple_size_v<std::decay_t<T>>>{});
}

class spread_impl: private_namespace {
	template<class Op, class T, std::size_t... is> static decltype(auto) spread(T &&t, std::index_sequence<is...>) noexcept(noexcept(Op::spread(std::get<is>(t)...))) {
		return Op::spread(std::get<is>(t)...);
	}
	template<class Op, class T> friend decltype(auto) spread(T &&t)
		noexcept(noexcept(spread_impl::spread<Op>(std::forward<T>(t), std::make_index_sequence<tuple_size_v<std::decay_t<T>>>{})));
};
template<class Op, class T> inline decltype(auto) spread(T &&t)
	noexcept(noexcept(spread_impl::spread<Op>(std::forward<T>(t), std::make_index_sequence<tuple_size_v<std::decay_t<T>>>{})))
{
	return spread_impl::spread<Op>(std::forward<T>(t), std::make_index_sequence<tuple_size_v<std::decay_t<T>>>{});
}

template<class Op, class... T> inline constexpr bool compiles() { return Op::template compiles<T...>; }
template<class Op, class... T> inline constexpr bool compiles(T &&...) { return Op::template compiles<T...>; }

template<class Operand> using postincrement = POSTFIX_TYPE(Operand, ++);
struct postinc: unary_operator<postincrement> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept((noexcept(ts++) && ...)) {
		return ((ts++), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype(ts++)...>(ts++...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept((noexcept(ts++) && ...)) {
		return ((ts++), ...);
	}
};

template<class Operand> using postdecrement = POSTFIX_TYPE(Operand, --);
struct postdec: unary_operator<postdecrement> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept((noexcept(ts--) && ...)) {
		return ((ts--), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype(ts--)...>(ts--...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept((noexcept(ts--) && ...)) {
		return ((ts--), ...);
	}
};

template<class T> class typecast: sfinae {
	template<class Operand> static constexpr decltype(T(std::declval<Operand>()), Yes{}) can(Operand &&);
public:
	static constexpr ::std::size_t arity = 1;
	template<class Operand> static constexpr bool compiles = is_yes_v<decltype(can(::std::declval<Operand>()))>;

	template<class... Us> static constexpr decltype(auto) call(Us &&...us) noexcept((noexcept(T(std::forward<Us>(us))) && ...)) {
		return (T(std::forward<Us>(us)), ...);
	}
	template<class... Us> static constexpr decltype(auto) spread(Us &&...us) {
		return std::make_tuple(T(std::forward<Us>(us))...);
	}
	template<class... Us> constexpr decltype(auto) operator()(Us &&...us) const noexcept((noexcept(T(std::forward<Us>(us))) && ...)) {
		return (T(std::forward<Us>(us)), ...);
	}
};

struct funcall: sfinae {
	template<class F, class... Args> static constexpr decltype(std::declval<F>(std::declval<Args>()...), Yes{}) can(F &&, Args &&...);
public:
	static constexpr ::std::size_t arity = max_size_v;
	template<class F, class... Args> static constexpr bool compiles = is_yes_v<decltype(can(::std::declval<F>(), ::std::declval<Args>()...))>;

	template<class F, class... Args> static constexpr decltype(auto) call(F &&f, Args &&...args) noexcept(noexcept(std::forward<F>(f)(std::forward<Args>(args)...)))
	{
		return std::forward<F>(f)(std::forward<Args>(args)...);
	}
	template<class F, class... Args> static constexpr decltype(auto) spread(F &&f, Args &&...args) noexcept(noexcept(std::forward<F>(f)(std::forward<Args>(args)...)))
	{
		return std::forward<F>(f)(std::forward<Args>(args)...);
	}
	template<class F, class... Args> constexpr decltype(auto) operator()(F &&f, Args &&...args) const
		noexcept(noexcept(std::forward<F>(f)(std::forward<Args>(args)...)))
	{
		return std::forward<F>(f)(std::forward<Args>(args)...);
	}
};

template<class Container, class Index> using subscript_type = decltype(::std::declval<Container>()[::std::declval<Index>()]);
struct subscript: binary_operator<subscript_type> {
	template<class L, class I> static constexpr decltype(auto) call(L &&l, I &&i) noexcept(noexcept(std::forward<L>(l)[std::forward<I>(i)])) {
		return std::forward<L>(l)[std::forward<I>(i)];
	}
	template<class L, class I> static constexpr decltype(auto) spread(L &&l, I &&i) noexcept(noexcept(std::forward<L>(l)[std::forward<I>(i)])) {
		return std::forward<L>(l)[std::forward<I>(i)];
	}
	template<class L, class I> constexpr decltype(auto) operator()(L &&l, I &&i) const noexcept(noexcept(std::forward<L>(l)[std::forward<I>(i)])) {
		return std::forward<L>(l)[std::forward<I>(i)];
	}
};

template<class Operand> using arrow_type = POSTFIX_TYPE(Operand, .operator->());
struct arrow: unary_operator<arrow_type> {
	template<class... Cs> static constexpr decltype(auto) call(Cs &&...cs) noexcept((noexcept(std::forward<Cs>(cs).operator->()) && ...)) {
		return (std::forward<Cs>(cs).operator->(), ...);
	}
	template<class... Cs> static constexpr decltype(auto) spread(Cs &&...cs) {
		return tuple<decltype(std::forward<Cs>(cs).operator->())...>(std::forward<Cs>(cs).operator->()...);
	}
	template<class... Cs> constexpr decltype(auto) operator()(Cs &&...cs) const noexcept((noexcept(std::forward<Cs>(cs).operator->()) && ...)) {
		return (std::forward<Cs>(cs).operator->(), ...);
	}
};

template<class Operand> using preincrement = PREFIX_TYPE(++, Operand);
struct preinc: unary_operator<preincrement> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept((noexcept(++std::forward<Ts>(ts)) && ...)) {
		return (++std::forward<Ts>(ts), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype(++std::forward<Ts>(ts))...>(++std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept((noexcept(++std::forward<Ts>(ts)) && ...)) {
		return (++std::forward<Ts>(ts), ...);
	}
};

template<class Operand> using predecrement = PREFIX_TYPE(--, Operand);
struct predec: unary_operator<predecrement> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept((noexcept(--std::forward<Ts>(ts)) && ...)) {
		return (--std::forward<Ts>(ts), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype(--std::forward<Ts>(ts))...>(--std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept((noexcept(--std::forward<Ts>(ts)) && ...)) {
		return (--std::forward<Ts>(ts), ...);
	}
};

template<class Operand> using unary_plus = PREFIX_TYPE(+, Operand);
struct un_plus: unary_operator<unary_plus> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept((noexcept(+std::forward<Ts>(ts)) && ...)) {
		return (+std::forward<Ts>(ts), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype(+std::declval<Ts>())...>(+std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept((noexcept(+std::forward<Ts>(ts)) && ...)) {
		return (+std::forward<Ts>(ts), ...);
	}
};

template<class Operand> using unary_minus = PREFIX_TYPE(-, Operand);
struct un_minus: unary_operator<unary_minus> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept((noexcept(-std::forward<Ts>(ts)) && ...)) {
		return (-std::forward<Ts>(ts), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype(-std::declval<Ts>())...>(-std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept((noexcept(-std::forward<Ts>(ts)) && ...)) {
		return (-std::forward<Ts>(ts), ...);
	}
};

template<class Operand> using exclamation_mark = PREFIX_TYPE(!, Operand);
struct bool_not: unary_operator<exclamation_mark> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept((noexcept(!std::forward<Ts>(ts)) && ...)) {
		return (!std::forward<Ts>(ts), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype(!std::declval<Ts>())...>(!std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept((noexcept(!std::forward<Ts>(ts)) && ...)) {
		return (!std::forward<Ts>(ts), ...);
	}
};

template<class Operand> using tilde = PREFIX_TYPE(~, Operand);
struct bit_not: unary_operator<tilde> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept((noexcept(~std::forward<Ts>(ts)) && ...)) {
		return (~std::forward<Ts>(ts), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype(~std::declval<Ts>())...>(~std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept((noexcept(~std::forward<Ts>(ts)) && ...)) {
		return (~std::forward<Ts>(ts), ...);
	}
};

template<typename T> struct old_cast: sfinae {
	template<class Operand> static constexpr decltype((T)std::declval<Operand>(), Yes{}) can(Operand &&);
public:
	static constexpr ::std::size_t arity = 1;
	template<class Operand> static constexpr bool compiles = is_yes_v<decltype(can(::std::declval<Operand>()))>;

	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept((noexcept((T)std::forward<Ts>(ts)) && ...)) {
		return ((T)std::forward<Ts>(ts), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype((T)std::forward<Ts>(ts))...>((T)std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept((noexcept((T)std::forward<Ts>(ts)) && ...)) {
		return ((T)std::forward<Ts>(ts), ...);
	}
};

template<class Operand> using asterisk = PREFIX_TYPE(*, Operand);
struct dereference: unary_operator<asterisk> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept((noexcept(*std::forward<Ts>(ts)) && ...)) {
		return (*std::forward<Ts>(ts), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype(*std::forward<Ts>(ts))...>(*std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept((noexcept(*std::forward<Ts>(ts)) && ...)) {
		return (*std::forward<Ts>(ts), ...);
	}
};

template<class Operand> using ampersand = PREFIX_TYPE(&, Operand);
struct address_of: unary_operator<ampersand> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept((noexcept(&std::forward<Ts>(ts)) && ...)) {
		return (&std::forward<Ts>(ts), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype(&std::forward<Ts>(ts))...>(&std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept((noexcept(&std::forward<Ts>(ts)) && ...)) {
		return (&std::forward<Ts>(ts), ...);
	}
};

template<class Operand> using sizeof_type = PREFIX_TYPE(sizeof, Operand);
struct size_of: unary_operator<sizeof_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept {
		return (sizeof(ts), ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) {
		return tuple<decltype(sizeof(Ts))...>(sizeof(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept {
		return (sizeof(ts), ...);
	}
};

template<class C> struct neuf: sfinae {
	template<class... Args> static constexpr decltype(new C(std::declval<Args>()...), Yes{}) can(Args &&...);
public:
	static constexpr ::std::size_t arity = max_size_v;
	template<class... Args> static constexpr bool compiles = is_yes_v<decltype(can(::std::declval<Args>()...))>;

	template<class... Args> static constexpr decltype(auto) call(Args &&... args) { return new C(std::forward<Args>(args)...); }
	template<class... Args> static constexpr decltype(auto) spread(Args &&... args) { return new C(std::forward<Args>(args)...); }
	template<class... Args> constexpr decltype(auto) operator()(Args &&... args) const { return new C(std::forward<Args>(args)...); }
};

template<class C> struct placement_neuf: sfinae {
	template<class... Args> static constexpr decltype(new(std::declval<void *>()) C(std::declval<Args>()...), Yes{}) can(Args &&...);
public:
	static constexpr ::std::size_t arity = max_size_v;
	template<class... Args> static constexpr bool compiles = is_yes_v<decltype(can(::std::declval<Args>()...))>;

	template<class... Args> static constexpr decltype(auto) call(void *p, Args &&... args) noexcept(noexcept(C(std::forward<Args>(args)...))) {
		return new(p) C(std::forward<Args>(args)...);
	}
	template<class... Args> static constexpr decltype(auto) spread(void *p, Args &&... args) noexcept(noexcept(C(std::forward<Args>(args)...))) {
		return new(p) C(std::forward<Args>(args)...);
	}
	template<class... Args> constexpr decltype(auto) operator()(void *p, Args &&... args) const noexcept(noexcept(C(std::forward<Args>(args)...))) {
		return new(p) C(std::forward<Args>(args)...);
	}
};

template<class Operand> using delete_type = PREFIX_TYPE(delete, Operand);
template<class C> struct effacer: unary_operator<delete_type> {
	static constexpr void call(C const *c) noexcept(noexcept(delete c)) { delete c; }
	static constexpr void spread(C const *c) noexcept(noexcept(delete c)) { delete c; }
	constexpr void operator()(C const *c) const noexcept(noexcept(delete c)) { delete c; }
};


template<class Operand1, class Operand2> using arrow_asterisk = INFIX_TYPE(Operand1, ->*, Operand2);
struct member: binary_operator<arrow_asterisk> {
	template<class C, class M> static constexpr decltype(auto) call(C &&c, M &&m) noexcept(noexcept(std::forward<C>(c)->*std::forward<M>(m))) {
		return std::forward<C>(c)->*std::forward<M>(m);
	}
	template<class C, class M> static constexpr decltype(auto) spread(C &&c, M &&m) noexcept(noexcept(std::forward<C>(c)->*std::forward<M>(m))) {
		return std::forward<C>(c)->*std::forward<M>(m);
	}
	template<class C, class M> constexpr decltype(auto) operator()(C &&c, M &&m) const noexcept(noexcept(std::forward<C>(c)->*std::forward<M>(m))) {
		return std::forward<C>(c)->*std::forward<M>(m);
	}
};

template<class Operand1, class Operand2> using bin_asterisk = INFIX_TYPE(Operand1, *, Operand2);
struct mul: binary_operator<bin_asterisk> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) * ...))) {
		return (std::forward<Ts>(ts) * ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) * ...))) {
		return (std::forward<Ts>(ts) * ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) * ...))) {
		return (std::forward<Ts>(ts) * ...);
	}
};

template<class Operand1, class Operand2> using slash = INFIX_TYPE(Operand1, /, Operand2);
struct div: binary_operator<slash> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) / ...))) {
		return (std::forward<Ts>(ts) / ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) / ...))) {
		return (std::forward<Ts>(ts) / ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) / ...))) {
		return (std::forward<Ts>(ts) / ...);
	}
};

template<class Operand1, class Operand2> using percent = INFIX_TYPE(Operand1, %, Operand2);
struct mod: binary_operator<percent> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) % ...))) {
		return (std::forward<Ts>(ts) % ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) % ...))) {
		return (std::forward<Ts>(ts) % ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) % ...))) {
		return (std::forward<Ts>(ts) % ...);
	}
};

template<class Operand1, class Operand2> using binary_plus = INFIX_TYPE(Operand1, +, Operand2);
struct add: binary_operator<binary_plus> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) + ...))) {
		return (std::forward<Ts>(ts) + ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) + ...))) {
		return (std::forward<Ts>(ts) + ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) + ...))) {
		return (std::forward<Ts>(ts) + ...);
	}
};

template<class Operand1, class Operand2> using binary_minus = INFIX_TYPE(Operand1, -, Operand2);
struct sub: binary_operator<binary_minus> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) - ...))) {
		return (std::forward<Ts>(ts) - ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) - ...))) {
		return (std::forward<Ts>(ts) - ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) - ...))) {
		return (std::forward<Ts>(ts) - ...);
	}
};

template<typename Left, typename Right> using shift_left = INFIX_TYPE(Left, <<, Right);
struct shl: binary_operator<shift_left> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) << ...))) {
		return (std::forward<Ts>(ts) << ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) << ...))) {
		return (std::forward<Ts>(ts) << ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) << ...))) {
		return (std::forward<Ts>(ts) << ...);
	}
};

template<class Operand1, class Operand2> using shift_right = INFIX_TYPE(Operand1, >>, Operand2);
struct shr: binary_operator<shift_right> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) >> ...))) {
		return (std::forward<Ts>(ts) >> ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) >> ...))) {
		return (std::forward<Ts>(ts) >> ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) >> ...))) {
		return (std::forward<Ts>(ts) >> ...);
	}
};

template<class Operand1, class Operand2> using less_than = INFIX_TYPE(Operand1, <, Operand2);
class lt: public binary_operator<less_than> {
	template<class... Ts> struct impl;
public:
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
};
template<class T1, class T2, class... Ts> struct lt::impl<T1, T2, Ts...> {
	template<class... Others> friend struct lt::impl;
	static constexpr decltype(auto) call(T1 &&t1, T2 &&t2, Ts &&...ts)
		noexcept(noexcept(std::forward<T1>(t1) < t2) && noexcept(lt::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...)))
	{
		return std::forward<T1>(t1) < t2 && lt::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...);
	}
};
template<class T1> struct lt::impl<T1> {
	static constexpr bool call(T1 &&) noexcept { return true; }
};
template<> struct lt::impl<> {
	static constexpr bool call() noexcept { return true; }
};

template<class Operand1, class Operand2> using greater_than = INFIX_TYPE(Operand1, >, Operand2);
class gt: public binary_operator<greater_than> {
	template<class... Ts> struct impl;
public:
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
};
template<class T1, class T2, class... Ts> struct gt::impl<T1, T2, Ts...> {
	template<class... Others> friend struct gt::impl;
	static constexpr decltype(auto) call(T1 &&t1, T2 &&t2, Ts &&...ts)
		noexcept(noexcept(std::forward<T1>(t1) > t2) && noexcept(gt::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...)))
	{
		return std::forward<T1>(t1) > t2 && gt::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...);
	}
};
template<class T1> struct gt::impl<T1> {
	static constexpr bool call(T1 &&) noexcept { return true; }
};
template<> struct gt::impl<> {
	static constexpr bool call() noexcept { return true; }
};

template<class Operand1, class Operand2> using less_or_equal = INFIX_TYPE(Operand1, <=, Operand2);
class le: public binary_operator<less_or_equal> {
	template<class... Ts> struct impl;
public:
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
};
template<class T1, class T2, class... Ts> struct le::impl<T1, T2, Ts...> {
	template<class... Others> friend struct le::impl;
	static constexpr decltype(auto) call(T1 &&t1, T2 &&t2, Ts &&...ts)
		noexcept(noexcept(std::forward<T1>(t1) <= t2) && noexcept(le::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...)))
	{
		return std::forward<T1>(t1) <= t2 && le::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...);
	}
};
template<class T1> struct le::impl<T1> {
	static constexpr bool call(T1 &&) noexcept { return true; }
};
template<> struct le::impl<> {
	static constexpr bool call() noexcept { return true; }
};

template<class Operand1, class Operand2> using greater_or_equal = INFIX_TYPE(Operand1, >=, Operand2);
class ge: public binary_operator<greater_or_equal> {
	template<class... Ts> struct impl;
public:
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
};
template<class T1, class T2, class... Ts> struct ge::impl<T1, T2, Ts...> {
	template<class... Others> friend struct ge::impl;
	static constexpr decltype(auto) call(T1 &&t1, T2 &&t2, Ts &&...ts)
		noexcept(noexcept(std::forward<T1>(t1) >= t2) && noexcept(ge::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...)))
	{
		return std::forward<T1>(t1) >= t2 && ge::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...);
	}
};
template<class T1> struct ge::impl<T1> {
	static constexpr bool call(T1 &&) noexcept { return true; }
};
template<> struct ge::impl<> {
	static constexpr bool call() noexcept { return true; }
};

template<class Operand1, class Operand2> using eq_eq = INFIX_TYPE(Operand1, ==, Operand2);
class eq: public binary_operator<eq_eq> {
	template<class... Ts> struct impl;
public:
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
};
template<class T1, class T2, class... Ts> struct eq::impl<T1, T2, Ts...> {
	template<class... Others> friend struct eq::impl;
	static constexpr decltype(auto) call(T1 &&t1, T2 &&t2, Ts &&...ts)
		noexcept(noexcept(std::forward<T1>(t1) == t2) && noexcept(eq::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...)))
	{
		return std::forward<T1>(t1) == t2 && eq::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...);
	}
};
template<class T1> struct eq::impl<T1> {
	static constexpr bool call(T1 &&) noexcept { return true; }
};
template<> struct eq::impl<> {
	static constexpr bool call() noexcept { return true; }
};

template<class Operand1, class Operand2> using exclam_eq = INFIX_TYPE(Operand1, !=, Operand2);
class ne: public binary_operator<exclam_eq> {
	template<class... Ts> struct impl;
public:
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
};
template<class T1, class T2, class... Ts> struct ne::impl<T1, T2, Ts...> {
	template<class... Others> friend struct ne::impl;
	static constexpr decltype(auto) call(T1 &&t1, T2 &&t2, Ts &&...ts)
		noexcept(noexcept(std::forward<T1>(t1) != t2) && noexcept(ne::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...)))
	{
		// TODO: generally quadratic, more specializations needed for linear cases
		return t1 != t2 && ne::impl<T1, std::add_lvalue_reference_t<Ts>...>::call(std::forward<T1>(t1), ts...)
			&& ne::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...);
	}
};
template<class T1> struct ne::impl<T1> {
	static constexpr bool call(T1 &&) noexcept { return true; }
};
template<> struct ne::impl<> {
	static constexpr bool call() noexcept { return true; }
};

template<class Operand1, class Operand2> using binary_ampersand = INFIX_TYPE(Operand1, &, Operand2);
struct bit_and: binary_operator<binary_ampersand> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) & ...))) {
		return (std::forward<Ts>(ts) & ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) & ...))) {
		return (std::forward<Ts>(ts) & ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) & ...))) {
		return (std::forward<Ts>(ts) & ...);
	}
};

template<class Operand1, class Operand2> using binary_pipe = INFIX_TYPE(Operand1, |, Operand2);
struct bit_or: binary_operator<binary_pipe> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) | ...))) {
		return (std::forward<Ts>(ts) | ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) | ...))) {
		return (std::forward<Ts>(ts) | ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) | ...))) {
		return (std::forward<Ts>(ts) | ...);
	}
};

template<class Operand1, class Operand2> using rooftop = INFIX_TYPE(Operand1, ^, Operand2);
struct bit_xor: binary_operator<rooftop> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) ^ ...))) {
		return (std::forward<Ts>(ts) ^ ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) ^ ...))) {
		return (std::forward<Ts>(ts) ^ ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) ^ ...))) {
		return (std::forward<Ts>(ts) ^ ...);
	}
};

template<class Operand1, class Operand2> using double_ampersand = INFIX_TYPE(Operand1, &&, Operand2);
struct bool_and: binary_operator<double_ampersand> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) && ...))) {
		return (std::forward<Ts>(ts) && ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) && ...))) {
		return (std::forward<Ts>(ts) && ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) && ...))) {
		return (std::forward<Ts>(ts) && ...);
	}
};

template<class Operand1, class Operand2> using double_pipe = INFIX_TYPE(Operand1, ||, Operand2);
struct bool_or: binary_operator<double_pipe> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) || ...))) {
		return (std::forward<Ts>(ts) || ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) || ...))) {
		return (std::forward<Ts>(ts) || ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) || ...))) {
		return (std::forward<Ts>(ts) || ...);
	}
};

template<class Operand1, class Operand2> using something_not_of_this_language = decltype(bool(::std::declval<Operand1>()) + bool(::std::declval<Operand2>()) == 1);
struct bool_xor: binary_operator<something_not_of_this_language> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((bool(std::forward<Ts>(ts)) + ...))) {
		return (bool(std::forward<Ts>(ts)) + ...) == 1;
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((bool(std::forward<Ts>(ts)) + ...))) {
		return (bool(std::forward<Ts>(ts)) + ...) == 1;
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((bool(std::forward<Ts>(ts)) + ...))) {
		return (bool(std::forward<Ts>(ts)) + ...) == 1;
	}
};

template<class Operand1, class Operand2> using assign_type = INFIX_TYPE(Operand1, =, Operand2);
struct assign: binary_operator<assign_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) = ...))) {
		return (std::forward<Ts>(ts) = ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) = ...))) {
		return (std::forward<Ts>(ts) = ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) = ...))) {
		return (std::forward<Ts>(ts) = ...);
	}
};

template<class Operand1, class Operand2> using assign_add_type = INFIX_TYPE(Operand1, +=, Operand2);
struct assign_add: binary_operator<assign_add_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) += ...))) {
		return (std::forward<Ts>(ts) += ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) += ...))) {
		return (std::forward<Ts>(ts) += ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) += ...))) {
		return (std::forward<Ts>(ts) += ...);
	}
};

template<class Operand1, class Operand2> using assign_sub_type = INFIX_TYPE(Operand1, -=, Operand2);
struct assign_sub: binary_operator<assign_sub_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) -= ...))) {
		return (std::forward<Ts>(ts) -= ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) -= ...))) {
		return (std::forward<Ts>(ts) -= ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) -= ...))) {
		return (std::forward<Ts>(ts) -= ...);
	}
};

template<class Operand1, class Operand2> using assign_mul_type = INFIX_TYPE(Operand1, *=, Operand2);
struct assign_mul: binary_operator<assign_mul_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) *= ...))) {
		return (std::forward<Ts>(ts) *= ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) *= ...))) {
		return (std::forward<Ts>(ts) *= ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) *= ...))) {
		return (std::forward<Ts>(ts) *= ...);
	}
};

template<class Operand1, class Operand2> using assign_div_type = INFIX_TYPE(Operand1, /=, Operand2);
struct assign_div: binary_operator<assign_div_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) /= ...))) {
		return (std::forward<Ts>(ts) /= ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) /= ...))) {
		return (std::forward<Ts>(ts) /= ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) /= ...))) {
		return (std::forward<Ts>(ts) /= ...);
	}
};

template<class Operand1, class Operand2> using assign_mod_type = INFIX_TYPE(Operand1, %=, Operand2);
struct assign_mod: binary_operator<assign_mod_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) %= ...))) {
		return (std::forward<Ts>(ts) %= ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) %= ...))) {
		return (std::forward<Ts>(ts) %= ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) %= ...))) {
		return (std::forward<Ts>(ts) %= ...);
	}
};

template<class Operand1, class Operand2> using assign_shl_type = INFIX_TYPE(Operand1, <<=, Operand2);
struct assign_shl: binary_operator<assign_shl_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) <<= ...))) {
		return (std::forward<Ts>(ts) <<= ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) <<= ...))) {
		return (std::forward<Ts>(ts) <<= ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) <<= ...))) {
		return (std::forward<Ts>(ts) <<= ...);
	}
};

template<class Operand1, class Operand2> using assign_shr_type = INFIX_TYPE(Operand1, >>=, Operand2);
struct assign_shr: binary_operator<assign_shr_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) >>= ...))) {
		return (std::forward<Ts>(ts) >>= ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) >>= ...))) {
		return (std::forward<Ts>(ts) >>= ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) >>= ...))) {
		return (std::forward<Ts>(ts) >>= ...);
	}
};

template<class Operand1, class Operand2> using assign_and_type = INFIX_TYPE(Operand1, &=, Operand2);
struct assign_and: binary_operator<assign_and_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) &= ...))) {
		return (std::forward<Ts>(ts) &= ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) &= ...))) {
		return (std::forward<Ts>(ts) &= ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) &= ...))) {
		return (std::forward<Ts>(ts) &= ...);
	}
};

template<class Operand1, class Operand2> using assign_xor_type = INFIX_TYPE(Operand1, ^=, Operand2);
struct assign_xor: binary_operator<assign_xor_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) ^= ...))) {
		return (std::forward<Ts>(ts) ^= ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) ^= ...))) {
		return (std::forward<Ts>(ts) ^= ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) ^= ...))) {
		return (std::forward<Ts>(ts) ^= ...);
	}
};

template<class Operand1, class Operand2> using assign_or_type = INFIX_TYPE(Operand1, |=, Operand2);
struct assign_or: binary_operator<assign_or_type> {
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) |= ...))) {
		return (std::forward<Ts>(ts) |= ...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept((std::forward<Ts>(ts) |= ...))) {
		return (std::forward<Ts>(ts) |= ...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept((std::forward<Ts>(ts) |= ...))) {
		return (std::forward<Ts>(ts) |= ...);
	}
};

template<class Operand1, class Operand2> using assign_bool_and_type = decltype(::std::declval<Operand1>() = ::std::declval<Operand1>() && ::std::declval<Operand2>());
class assign_bool_and: public binary_operator<assign_bool_and_type> {
	template<class... Ts> struct impl;
public:
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
};
template<class T1, class T2, class... Ts> struct assign_bool_and::impl<T1, T2, Ts...> {
	static constexpr decltype(auto) call(T1 &&t1, T2 &&t2, Ts &&...ts)
		noexcept(noexcept(t1 = t1 && assign_bool_and::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...)))
	{
		return t1 = t1 && assign_bool_and::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...);
	}
};
template<class T1> struct assign_bool_and::impl<T1> { static constexpr decltype(auto) call(T1 &&t1) noexcept { return std::forward<T1>(t1); } };
template<> struct assign_bool_and::impl<> { static constexpr bool call() noexcept { return true; } };

template<class Operand1, class Operand2> using assign_bool_or_type = decltype(::std::declval<Operand1>() = ::std::declval<Operand1>() || ::std::declval<Operand2>());
class assign_bool_or: public binary_operator<assign_bool_or_type> {
	template<class... Ts> struct impl;
public:
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
};
template<class T1, class T2, class... Ts> struct assign_bool_or::impl<T1, T2, Ts...> {
	static constexpr decltype(auto) call(T1 &&t1, T2 &&t2, Ts &&...ts)
		noexcept(noexcept(t1 = t1 || assign_bool_or::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...)))
	{
		return t1 = t1 || assign_bool_or::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...);
	}
};
template<class T1> struct assign_bool_or::impl<T1> { static constexpr decltype(auto) call(T1 &&t1) noexcept { return std::forward<T1>(t1); } };
template<> struct assign_bool_or::impl<> {
	static constexpr bool call() noexcept { return false; }
};

template<class Operand1, class Operand2> using assign_bool_xor_type = decltype(::std::declval<Operand1>() = ::std::declval<Operand1>() != ::std::declval<Operand2>());
class assign_bool_xor: public binary_operator<assign_bool_xor_type> {
	template<class... Ts> struct impl;
public:
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) const noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
};
template<class T1, class T2, class... Ts> struct assign_bool_xor::impl<T1, T2, Ts...> {
	static constexpr decltype(auto) call(T1 &&t1, T2 &&t2, Ts &&...ts)
		noexcept(noexcept(t1 = t1 != assign_bool_xor::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...)))
	{
		return t1 = t1 != assign_bool_xor::impl<T2, Ts...>::call(std::forward<T2>(t2), std::forward<Ts>(ts)...);
	}
};
template<class T1> struct assign_bool_xor::impl<T1> { static constexpr decltype(auto) call(T1 &&t1) noexcept { return std::forward<T1>(t1); } };
template<> struct assign_bool_xor::impl<> {
	static constexpr bool call() noexcept { return false; }
};

}

#ifndef SHOW_NOTHING

#include "show.h"

namespace nstd {

SHOW_NSTD_ROOT(1, mute);
SHOW_NSTD_SIMPLE(postinc);
SHOW_NSTD_SIMPLE(postdec);
SHOW_NSTD_ROOT(1, typecast);
SHOW_NSTD_SIMPLE(funcall);
SHOW_NSTD_SIMPLE(subscript);
SHOW_NSTD_SIMPLE(arrow);
SHOW_NSTD_SIMPLE(preinc);
SHOW_NSTD_SIMPLE(predec);
SHOW_NSTD_SIMPLE(un_plus);
SHOW_NSTD_SIMPLE(un_minus);
SHOW_NSTD_SIMPLE(bool_not);
SHOW_NSTD_SIMPLE(bit_not);
SHOW_NSTD_ROOT(1, old_cast);
SHOW_NSTD_SIMPLE(dereference);
SHOW_NSTD_SIMPLE(address_of);
SHOW_NSTD_SIMPLE(size_of);
SHOW_NSTD_SIMPLE(member);
SHOW_NSTD_SIMPLE(mul);
SHOW_NSTD_SIMPLE(div);
SHOW_NSTD_SIMPLE(mod);
SHOW_NSTD_SIMPLE(add);
SHOW_NSTD_SIMPLE(sub);
SHOW_NSTD_SIMPLE(shl);
SHOW_NSTD_SIMPLE(shr);
SHOW_NSTD_SIMPLE(lt);
SHOW_NSTD_SIMPLE(gt);
SHOW_NSTD_SIMPLE(le);
SHOW_NSTD_SIMPLE(ge);
SHOW_NSTD_SIMPLE(eq);
SHOW_NSTD_SIMPLE(ne);
SHOW_NSTD_SIMPLE(bit_and);
SHOW_NSTD_SIMPLE(bit_or);
SHOW_NSTD_SIMPLE(bit_xor);
SHOW_NSTD_SIMPLE(bool_and);
SHOW_NSTD_SIMPLE(bool_or);
SHOW_NSTD_SIMPLE(bool_xor);
SHOW_NSTD_SIMPLE(assign);
SHOW_NSTD_SIMPLE(assign_add);
SHOW_NSTD_SIMPLE(assign_sub);
SHOW_NSTD_SIMPLE(assign_mul);
SHOW_NSTD_SIMPLE(assign_div);
SHOW_NSTD_SIMPLE(assign_mod);
SHOW_NSTD_SIMPLE(assign_shl);
SHOW_NSTD_SIMPLE(assign_shr);
SHOW_NSTD_SIMPLE(assign_and);
SHOW_NSTD_SIMPLE(assign_xor);
SHOW_NSTD_SIMPLE(assign_or);
SHOW_NSTD_SIMPLE(assign_bool_and);
SHOW_NSTD_SIMPLE(assign_bool_or);
SHOW_NSTD_SIMPLE(assign_bool_xor);

}

#endif

#endif
