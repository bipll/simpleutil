/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__PRIVATE_NAMESPACE_H_0d248746c769a247c9335edc0e12e391797c9a1d
#define SIMPLEUTIL__PRIVATE_NAMESPACE_H_0d248746c769a247c9335edc0e12e391797c9a1d

namespace nstd {

class private_namespace {
	private_namespace() = delete;
	private_namespace(private_namespace &&) = delete;
	private_namespace(private_namespace const &) = delete;
	template<class T> private_namespace(::std::initializer_list<T>) = delete;
	template<class... Args> private_namespace(Args &&...) = delete;
};

struct private_ctor_tag_t {};

}

#endif
