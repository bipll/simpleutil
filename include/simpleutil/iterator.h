/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__ITERATOR_H_16d06f2069fb7654c4009930f6c923911f42b4b2
#define SIMPLEUTIL__ITERATOR_H_16d06f2069fb7654c4009930f6c923911f42b4b2

#include <iterator>
#include <utility>
#include <type_traits>
#include <algorithm>
#include <iostream>
#include <type_traits>
#include <array>
#include <functional>
#include "typeutil.h"
#include "operator.h"
#include "optional.h"
#include "adl_or_std.h"
#include "tuple.h"

namespace nstd {

ADL_OR_STD(begin);

ADL_OR_STD(end);

ADL_OR_STD(empty);

template<typename Ctr> using iterator_type_t = ::std::invoke_result_t<adl_or_std::begin, Ctr>;
template<typename Ctr> using const_iterator_type_t = ::std::invoke_result_t<adl_or_std::begin, ::std::add_const_t<Ctr>>;
template<typename Ctr> using end_type_t = ::std::invoke_result_t<adl_or_std::end, Ctr>;
template<typename Ctr> using const_end_type_t = ::std::invoke_result_t<adl_or_std::end, ::std::add_const_t<Ctr>>;

template<typename Itr> using head_type_t = ::std::invoke_result_t<dereference, Itr>;
template<typename Ctr> using element_type_t = head_type_t<iterator_type_t<Ctr>>;
template<typename Ctr> using const_element_type_t = head_type_t<const_iterator_type_t<Ctr>>;

template<class... Ends> struct stop_iteration: tuple<Ends...> {
	using This = stop_iteration;
	using Parent = tuple<Ends...>;
	Parent &itrs = *this;

	using Parent::Parent;
	constexpr stop_iteration(This &&that) noexcept(::std::is_nothrow_move_constructible_v<Parent>): Parent(::std::move(that)) {}
	constexpr stop_iteration(This const &that) noexcept(::std::is_nothrow_copy_constructible_v<Parent>): Parent(that) {}

	constexpr stop_iteration &operator=(stop_iteration &&that) noexcept(::std::is_nothrow_move_assignable_v<Parent>) {
		return Parent::operator=(::std::move(that)), *this;
	}
	constexpr stop_iteration &operator=(stop_iteration const &that) noexcept(::std::is_nothrow_copy_assignable_v<Parent>) {
		return Parent::operator=(that), *this;
	}
};
template<class... Ends> stop_iteration(Ends &&...) -> stop_iteration<Ends...>;
template<class End> struct stop_iteration<End> {
	End itr;

	template<typename... Args> constexpr stop_iteration(Args &&...args) noexcept(::std::is_nothrow_constructible_v<End, Args...>)
		: itr(::std::forward<Args>(args)...) {}
};
template<> struct stop_iteration<> { constexpr bool operator==(stop_iteration<>) const noexcept { return true; } };
using sentinel = stop_iteration<>;

struct infinity {};

template<typename T> constexpr bool operator==(T &&, infinity) noexcept { return false; }
template<typename T> constexpr bool operator==(infinity, T &&) noexcept { return false; }
constexpr bool operator==(infinity, infinity) noexcept { return true; }

template<typename T> constexpr bool operator!=(T &&, infinity) noexcept { return true; }
template<typename T> constexpr bool operator!=(infinity, T &&) noexcept { return true; }
constexpr bool operator!=(infinity, infinity) noexcept { return false; }

template<typename Itr> inline constexpr bool is_inexhaustible_v = ::std::is_same_v<Itr, infinity>;
template<typename Ctr> inline constexpr bool is_infinite_v = is_inexhaustible_v<end_type_t<Ctr>>;

namespace iterator {

template<class Iterator> class level;
template<class Iterator> inline constexpr auto level_v = level<Iterator>::value;

template<class Category> struct level;

template<> struct level<::std::input_iterator_tag>: std::integral_constant<::std::size_t, 0> {};
template<> struct level<::std::output_iterator_tag>: std::integral_constant<::std::size_t, 1> {};
template<> struct level<::std::forward_iterator_tag>: std::integral_constant<::std::size_t, 2> {};
template<> struct level<::std::bidirectional_iterator_tag>: std::integral_constant<::std::size_t, 3> {};
template<> struct level<::std::random_access_iterator_tag>: std::integral_constant<::std::size_t, 4> {};

template<class I> static constexpr bool is_output_v = std::is_same_v<typename ::std::iterator_traits<I>::iterator_category, ::std::output_iterator_tag>;
template<class Iterator> using category_t = typename ::std::iterator_traits<Iterator>::iterator_category;
template<class Iterator> using value_t = typename ::std::iterator_traits<Iterator>::value_type;
template<class Iterator> using difference_t = typename ::std::iterator_traits<Iterator>::difference_type;
template<class Iterator> using pointer_t = typename ::std::iterator_traits<Iterator>::pointer;
template<class Iterator> using reference_t = typename ::std::iterator_traits<Iterator>::reference;

}

template<class Sink, class Comma> struct join_iterator;
template<class Sink, class Comma> constexpr void swap(join_iterator<Sink, Comma> &, join_iterator<Sink, Comma> &)
	noexcept(::std::is_same_v<Comma, void> || ::std::is_nothrow_swappable_v<Comma>);

template<class Sink, class Comma> struct join_iterator {
	using This = join_iterator;
	using sink_type = Sink;
	using comma_type = Comma;

	using iterator_category = std::output_iterator_tag;
	using value_type = void;
	using difference_type = void;
	using pointer = void;
	using reference = void;

	constexpr join_iterator() noexcept(std::is_nothrow_constructible_v<Comma>): s(), c() {}
	constexpr join_iterator(sink_type &sink, comma_type c) noexcept(::std::is_nothrow_move_constructible_v<Comma>): s(&sink), c(std::move(c)) {}
	constexpr join_iterator(join_iterator &&) noexcept(::std::is_nothrow_move_constructible_v<Comma>) = default;
	constexpr join_iterator(join_iterator const &) noexcept(::std::is_nothrow_copy_constructible_v<Comma>) = default;

	~join_iterator() noexcept(::std::is_nothrow_destructible_v<Comma>) = default;

	constexpr This &operator=(This &&that) = default;
	constexpr This &operator=(This const &that) = default;

	constexpr sink_type &sink() const noexcept { return *s; }
	constexpr comma_type comma() const noexcept(::std::is_nothrow_copy_constructible_v<comma_type>) { return c; }

	template<typename T> constexpr This &operator=(T &&value) noexcept(noexcept(post(*s, c)) && noexcept(post(*s, ::std::forward<T>(value)))) {
		if(next) post(*s, c);
		return post(*s, ::std::forward<T>(value)), next = true, *this;
	}

	template<typename T> constexpr This &operator<<(T &&value) noexcept(noexcept(post(*s, ::std::forward<T>(value)))) {
		return post(*s, ::std::forward<T>(value)), *this;
	}

	constexpr This &operator*() noexcept { return *this; }
	constexpr This &operator++() noexcept { return *this; }
	constexpr This &operator++(int) noexcept { return *this; }
	friend constexpr void swap<Sink, Comma>(join_iterator &, join_iterator &) noexcept(::std::is_nothrow_swappable_v<Comma>);
private:
	Sink *s;
	Comma c;
	bool next = false;
};
template<typename Sink, typename Comma> join_iterator(Sink &s, Comma) -> join_iterator<Sink, Comma>;

template<class Sink> struct join_iterator<Sink, void> {
	using This = join_iterator;
	using sink_type = Sink;
	using comma_type = void;

	using iterator_category = std::output_iterator_tag;
	using value_type = void;
	using difference_type = void;
	using pointer = void;
	using reference = void;

	constexpr join_iterator() noexcept: s() {}
	constexpr join_iterator(sink_type &sink) noexcept: s(&sink) {}
	constexpr join_iterator(join_iterator &&) noexcept = default;
	constexpr join_iterator(join_iterator const &) noexcept = default;

	~join_iterator() noexcept = default;

	constexpr This &operator=(This &&) noexcept = default;
	constexpr This &operator=(This const &) noexcept = default;

	constexpr sink_type &sink() const noexcept { return *s; }

	template<class T> This &operator=(T &&value) noexcept(NOEXCEPT(post(*s, std::forward<T>(value)))) { return post(*s, std::forward<T>(value)), *this; }

	template<class T> This &operator<<(T &&value) noexcept(NOEXCEPT(post(*s, std::forward<T>(value)))) { return post(*s, std::forward<T>(value)), *this; }

	constexpr This &operator*() noexcept { return *this; }
	constexpr This &operator++() noexcept { return *this; }
	constexpr This &operator++(int) noexcept { return *this; }
	friend constexpr void swap<Sink, void>(join_iterator &, join_iterator &) noexcept;
private:
	Sink *s;
};
template<typename Sink> join_iterator(Sink &s) -> join_iterator<Sink, void>;
template<typename Sink> inline constexpr join_iterator<Sink, void> compre(Sink &s) noexcept(NOEXCEPT(join_iterator(s))) { return join_iterator(s); }

template<class Sink, class Comma> inline constexpr void swap(join_iterator<Sink, Comma> &left, join_iterator<Sink, Comma> &right)
	noexcept(::std::is_same_v<Comma, void> || ::std::is_nothrow_swappable_v<Comma>)
{
	using ::std::swap;
	swap(left.s, right.s);
	if constexpr(!std::is_same_v<Comma, void>) {
		swap(left.c, right.c);
		swap(left.next, right.next);
	}
}

HAS_MEMBER_TYPE_P(value_type);
HAS_MEMBER_TYPE_P(difference_type);
HAS_MEMBER_TYPE_P(pointer);
HAS_MEMBER_TYPE_P(reference);

template<class... Itrs> class zip_iterator;
template<class... Itrs> class zip_iterator {
	using Zipper = tuple<Itrs...>;
	Zipper itrs;

	template<std::size_t... is> constexpr bool eq(zip_iterator const &that, ::std::index_sequence<is...>) const
		noexcept(noexcept(((::std::get<is>(itrs) == ::std::get<is>(that.itrs)) || ...)))
	{
		return ((::std::get<is>(itrs) == ::std::get<is>(that.itrs)) || ...);
	}
	template<std::size_t... is> constexpr bool ne(zip_iterator const &that, ::std::index_sequence<is...>) const
		noexcept(noexcept(((::std::get<is>(itrs) == ::std::get<is>(that.itrs)) || ...)))
	{
		return ((::std::get<is>(itrs) != ::std::get<is>(that.itrs)) && ...);
	}
public:
	static_assert(!(iterator::is_output_v<Itrs> || ...) || (iterator::is_output_v<Itrs> && ...));

	using This = zip_iterator;
	using iterator_category = min_by_rank_t<iterator::level, ::std::bidirectional_iterator_tag, iterator::category_t<Itrs>...>;
	using value_type = ::std::conditional_t<(has_member_type_value_type_v<Itrs> && ...), tuple<iterator::value_t<Itrs>...>, void>;
	using difference_type = ::std::conditional_t<(has_member_type_difference_type_v<Itrs> && ...)
		, pack::reduce_t<uncommon_type_t, ::std::ptrdiff_t, iterator::difference_t<Itrs>...>, void>;
	using pointer = void;
	using reference = ::std::conditional_t<(has_member_type_reference_v<Itrs> && ...), tuple<iterator::reference_t<Itrs>...>, value_type>;

	constexpr zip_iterator() noexcept(::std::is_nothrow_constructible_v<Zipper>) = default;
	constexpr zip_iterator(Itrs ...itrs) noexcept(::std::is_nothrow_constructible_v<Zipper, Itrs &&...>): itrs{std::move(itrs)...} {}

	constexpr zip_iterator(zip_iterator &&) noexcept(::std::is_nothrow_move_constructible_v<Zipper>) = default;
	constexpr zip_iterator(zip_iterator const &) noexcept(::std::is_nothrow_copy_constructible_v<Zipper>) = default;
	~zip_iterator() noexcept(::std::is_nothrow_destructible_v<Zipper>) = default;

	zip_iterator &operator=(zip_iterator &&that) noexcept(::std::is_nothrow_move_assignable_v<Zipper>) = default;
	zip_iterator &operator=(zip_iterator const &that) noexcept(::std::is_nothrow_copy_assignable_v<Zipper>) = default;

	constexpr decltype(auto) operator*() noexcept(noexcept(spread<dereference>(itrs))) { return spread<dereference>(itrs); }
	constexpr decltype(auto) operator*() const noexcept(noexcept(spread<dereference>(itrs))) { return spread<dereference>(itrs); }

	constexpr zip_iterator &operator++() noexcept(noexcept(call<preinc>(itrs))) { return call<preinc>(itrs), *this; }
	constexpr zip_iterator &operator--() noexcept(noexcept(call<predec>(itrs))) { return call<predec>(itrs), *this; }

	constexpr zip_iterator operator++(int) noexcept(::std::is_nothrow_copy_constructible_v<This> && noexcept(call<preinc>(itrs))) {
		zip_iterator retVal{*this};
		return call<preinc>(itrs), retVal;
	}
	constexpr zip_iterator operator--(int) noexcept(::std::is_nothrow_copy_constructible_v<This> && noexcept(call<predec>(itrs))) {
		zip_iterator retVal{*this};
		return call<predec>(itrs), retVal;
	}

	constexpr bool operator==(zip_iterator const &that) const noexcept {
		return eq(that, ::std::index_sequence_for<Itrs...>{});
	}
	constexpr bool operator!=(zip_iterator const &that) const noexcept {
		return ne(that, ::std::index_sequence_for<Itrs...>{});
	}
};

template<class Itr, class... Itrs> struct mzip_iterator {
	template<typename I> struct single_itr {
		I i;
		mutable bool alive = true;
		mutable long long int overdue = 0;
		mutable int allowance = 0;
	public:
		using This = single_itr;
		using reference = optional<decltype(*i)>;
		using const_reference = optional<::std::add_const_t<decltype(*i)>>;

		constexpr single_itr() noexcept(::std::is_nothrow_constructible_v<I>) = default;
		constexpr single_itr(I i) noexcept(::std::is_nothrow_move_constructible_v<I>): i(std::move(i)) {}
		constexpr bool eq(int displacement, single_itr const &that) const noexcept(noexcept(std::declval<I>() == std::declval<I>())) {
			if(i == that.i) return allowance = displacement, alive = false, true;
			return false;
		};
		constexpr void operator++() noexcept(noexcept(++std::declval<I>())) { if(alive || ++overdue <= allowance) ++i; }
		constexpr void operator--() noexcept(noexcept(--std::declval<I>())) { if(alive || --overdue < allowance) --i; }
		constexpr void operator++(int) noexcept(noexcept(++std::declval<I>())) { if(alive || ++overdue <= allowance) ++i; }
		constexpr void operator--(int) noexcept(noexcept(--std::declval<I>())) { if(alive || --overdue < allowance) --i; }
		constexpr const_reference operator*() const noexcept(noexcept(reference(*i)) && noexcept(const_reference())) {
			if(alive || overdue < allowance) return *i;
			return {};
		}
		constexpr reference operator*() noexcept(noexcept(reference(*i)) && noexcept(reference())) {
			if(alive || overdue < allowance) return *i;
			return {};
		}
	};
	using Zipper = tuple<single_itr<Itr>, single_itr<Itrs>...>;

	Zipper itrs;
	mutable ::std::array<bool, tuple_size_v<Zipper>> those_exhausted = {};
	int allowance = 0;
public:
	static_assert(!(iterator::is_output_v<Itr> || ... || iterator::is_output_v<Itrs>) || (iterator::is_output_v<Itr> && ... && iterator::is_output_v<Itrs>));

	using This = mzip_iterator;
	using iterator_category = min_by_rank_t<iterator::level, ::std::bidirectional_iterator_tag, iterator::category_t<Itrs>...>;
	using value_type = ::std::conditional_t<(has_member_type_value_type_v<Itr> && ... && has_member_type_value_type_v<Itrs>)
		, tuple<optional<iterator::value_t<Itr>>, optional<iterator::value_t<Itrs>>...>
		, void>;
	using difference_type = ::std::conditional_t<(has_member_type_difference_type_v<Itr> && ... && has_member_type_difference_type_v<Itrs>)
		, pack::reduce_t<uncommon_type_t, iterator::difference_t<Itr>, iterator::difference_t<Itrs>...>
		, void>;
	using pointer = void;
	using reference = ::std::conditional_t<(has_member_type_reference_v<Itr> && ... && has_member_type_reference_v<Itrs>)
		, tuple<optional<iterator::reference_t<Itr>>, optional<iterator::reference_t<Itrs>>...>
		, value_type>;

	constexpr mzip_iterator() = default;
	constexpr mzip_iterator(Itr itr, Itrs ...itrs) noexcept(::std::is_nothrow_constructible_v<Zipper, Itr &&, Itrs &&...>)
		: itrs{std::move(itr), std::move(itrs)...} {}

	constexpr mzip_iterator(mzip_iterator &&) = default;
	constexpr mzip_iterator(mzip_iterator const &) = default;
	~mzip_iterator() noexcept(::std::is_nothrow_destructible_v<Zipper>) = default;

	constexpr mzip_iterator &operator=(mzip_iterator &&that) = default;
	constexpr mzip_iterator &operator=(mzip_iterator const &that) = default;

	constexpr decltype(auto) operator*() noexcept(noexcept(spread<dereference>(itrs))) { return spread<dereference>(itrs); }
	constexpr decltype(auto) operator*() const noexcept(noexcept(spread<dereference>(itrs))) { return spread<dereference>(itrs); }

	constexpr mzip_iterator &operator++() noexcept(noexcept(call<preinc>(itrs))) { return call<preinc>(itrs), *this; }
	constexpr mzip_iterator &operator--() noexcept(noexcept(call<predec>(itrs))) { return call<predec>(itrs), ++allowance, *this; }

	constexpr mzip_iterator operator++(int) noexcept(::std::is_nothrow_copy_constructible_v<This> && noexcept(call<preinc>(itrs))) {
		mzip_iterator retVal{*this};
		return call<preinc>(itrs), retVal;
	}
	constexpr mzip_iterator operator--(int) noexcept(::std::is_nothrow_copy_constructible_v<This> && noexcept(call<predec>(itrs))) {
		mzip_iterator retVal{*this};
		return call<predec>(itrs), retVal;
	}

	constexpr bool operator==(mzip_iterator const &that) const noexcept {
		constexpr pack::integer_split_t<::std::make_index_sequence<sizeof...(Itrs) + 1>> indices;
		const int displacement = that.allowance;
		::std::size_t exhausted = tuple_reduce([this, &that, displacement](::std::size_t accum, auto index) constexpr noexcept {
			constexpr auto i = value_v<decltype(index)>;
			if(those_exhausted[i]) return accum + 1;
			if(::std::get<i>(itrs).eq(displacement, ::std::get<i>(that.itrs))) return those_exhausted[i] = true, accum + 1;
			return accum;
		}, 0, indices);
		return exhausted == those_exhausted.size();
	}

	constexpr bool operator!=(mzip_iterator const &that) const noexcept {
		constexpr pack::integer_split_t<std::make_index_sequence<sizeof...(Itrs) + 1>> indices;
		const int displacement = that.allowance;
		::std::size_t exhausted = tuple_reduce([this, &that, displacement](::std::size_t accum, auto index) constexpr noexcept {
			constexpr auto i = value_v<decltype(index)>;
			if(those_exhausted[i]) return accum + 1;
			if(::std::get<i>(itrs).eq(displacement, ::std::get<i>(that.itrs))) return those_exhausted[i] = true, accum + 1;
			return accum;
		}, 0, indices);
		return exhausted != those_exhausted.size();
	}
};

template<class F, class ...Ctrs> class can_transform: sfinae {
	using sfinae::can;
	template<class G, class... Itrs> static constexpr auto ican(G &&g, Itrs &&...itrs)
		-> decltype(::std::invoke(::std::forward<G>(g), *::std::forward<Itrs>(itrs)...), Yes{});
	No ican(...);
	template<class G, class... Dtrs> static constexpr auto ccan(G &&g, Dtrs &&...dtrs)
		-> decltype(ican(::std::forward<G>(g), adl_or_std::begin::call(::std::forward<Dtrs>(dtrs))...));
public:
	static constexpr bool value = is_yes_v<decltype(ccan(::std::declval<F>(), ::std::declval<Ctrs>()...))>;
};
template<class F, class ...Ctrs> inline constexpr bool can_transform_v = value_v<can_transform<F, Ctrs...>>;

template<class F, class... Itrs> class transform_iterator {
	using Zipper = tuple<Itrs...>;
	function_object_t<F, head_type_t<Itrs>...> f;
	Zipper itrs;

	template<std::size_t... is> constexpr bool eq(transform_iterator const &that, ::std::index_sequence<is...>) const
		noexcept(noexcept(((::std::get<is>(itrs) == ::std::get<is>(that.itrs)) || ...)))
	{
		return ((::std::get<is>(itrs) == ::std::get<is>(that.itrs)) || ...);
	}
	template<std::size_t... is> constexpr bool ne(transform_iterator const &that, ::std::index_sequence<is...>) const
		noexcept(noexcept(((::std::get<is>(itrs) == ::std::get<is>(that.itrs)) || ...)))
	{
		return ((::std::get<is>(itrs) != ::std::get<is>(that.itrs)) && ...);
	}

	template<typename... Jtrs, std::size_t... is> constexpr bool eq(stop_iteration<Jtrs...> const &that, ::std::index_sequence<is...>) const
		noexcept(noexcept(((::std::get<is>(itrs) == ::std::get<is>(that.itrs)) || ...)))
	{
		return ((::std::get<is>(itrs) == ::std::get<is>(that.itrs)) || ...);
	}
	template<typename... Jtrs, std::size_t... is> constexpr bool ne(stop_iteration<Jtrs...> const &that, ::std::index_sequence<is...>) const
		noexcept(noexcept(((::std::get<is>(itrs) != ::std::get<is>(that.itrs)) || ...)))
	{
		return ((::std::get<is>(itrs) != ::std::get<is>(that.itrs)) && ...);
	}

	template<::std::size_t... is> constexpr auto apply(::std::index_sequence<is...>) noexcept(noexcept(::std::invoke(f, *::std::get<is>(itrs)...))) {
		return ::std::invoke(f, *::std::get<is>(itrs)...);
	}
	template<::std::size_t... is> constexpr auto apply(::std::index_sequence<is...>) const noexcept(noexcept(::std::invoke(f, *::std::get<is>(itrs)...))) {
		return ::std::invoke(f, *::std::get<is>(itrs)...);
	}
public:
	using This = transform_iterator;
	using iterator_category = ::std::input_iterator_tag;
	using value_type = ::std::invoke_result_t<F, decltype(*::std::declval<Itrs>())...>;
	using difference_type = ::std::conditional_t<(has_member_type_difference_type_v<Itrs> && ...)
		, pack::reduce_t<uncommon_type_t, ::std::ptrdiff_t, iterator::difference_t<Itrs>...>, void>;
	using pointer = void;
	using reference = value_type;

	constexpr transform_iterator() = default;
	constexpr transform_iterator(F &&f, Itrs... itrs) noexcept(::std::is_nothrow_constructible_v<F, F &&> && ::std::is_nothrow_constructible_v<Zipper, Itrs &&...>)
		: f(::std::forward<F>(f)), itrs{std::move(itrs)...} {}

	constexpr transform_iterator(This &&) = default;
	constexpr transform_iterator(This const &src) = default;

	transform_iterator &operator=(This that) noexcept(are_nothrow_move_assignable_v<F, Zipper>) {
		return value_assign(f, ::std::move(that.f), itrs, ::std::move(that.itrs)), *this;
	}

	constexpr auto operator*() noexcept(NOEXCEPT(apply(::std::index_sequence_for<Itrs...>{}))) { return apply(::std::index_sequence_for<Itrs...>{}); }
	constexpr auto operator*() const noexcept(NOEXCEPT(apply(::std::index_sequence_for<Itrs...>{}))) { return apply(::std::index_sequence_for<Itrs...>{}); }

	constexpr This &operator++() noexcept(noexcept(call<preinc>(itrs))) { return call<preinc>(itrs), *this; }
	constexpr This &operator--() noexcept(noexcept(call<predec>(itrs))) { return call<predec>(itrs), *this; }

	constexpr This operator++(int) noexcept(::std::is_nothrow_copy_constructible_v<This> && noexcept(call<preinc>(itrs))) {
		This retVal{*this};
		return call<preinc>(itrs), retVal;
	}
	constexpr This operator--(int) noexcept(::std::is_nothrow_copy_constructible_v<This> && noexcept(call<predec>(itrs))) {
		This retVal{*this};
		return call<predec>(itrs), retVal;
	}

	constexpr bool operator==(This const &that) const noexcept { return eq(that, ::std::index_sequence_for<Itrs...>{}); }
	template<class... Jtrs> constexpr bool operator==(stop_iteration<Jtrs...> const &that) const noexcept { return eq(that, ::std::index_sequence_for<Itrs...>{}); }

	constexpr bool operator!=(This const &that) const noexcept { return ne(that, ::std::index_sequence_for<Itrs...>{}); }
	template<class... Jtrs> constexpr bool operator!=(stop_iteration<Jtrs...> const &that) const noexcept { return ne(that, ::std::index_sequence_for<Itrs...>{}); }
};
template<class F, class... Itrs> transform_iterator(F &&, Itrs &&...) -> transform_iterator<F, Itrs...>;

template<class F, class Itr> struct transform_iterator<F, Itr> {
	function_object_t<F, head_type_t<Itr>> f;
	Itr itr;
public:
	using This = transform_iterator;
	using iterator_category = ::std::input_iterator_tag;
	using value_type = ::std::invoke_result_t<F, decltype(*::std::declval<Itr>())>;
	using difference_type = ::std::conditional_t<has_member_type_difference_type_v<Itr>, uncommon_type_t<::std::ptrdiff_t, iterator::difference_t<Itr>>, void>;
	using pointer = void;
	using reference = value_type;

	constexpr transform_iterator() = default;
	constexpr transform_iterator(F &&f, Itr itr) noexcept(::std::is_nothrow_constructible_v<F, F &&> && ::std::is_nothrow_move_constructible_v<Itr>)
		: f(::std::forward<F>(f)), itr(std::move(itr)) {}

	constexpr transform_iterator(This &&src) noexcept(::std::is_nothrow_move_constructible_v<F> && ::std::is_nothrow_copy_constructible_v<Itr>)
		: f(::std::forward<decltype(f)>(src.f)), itr(::std::forward<Itr>(src.itr)) {}
	constexpr transform_iterator(This const &src) noexcept(::std::is_nothrow_copy_constructible_v<F> && ::std::is_nothrow_copy_constructible_v<Itr>)
		: f(src.f), itr(src.itr) {}

	~transform_iterator() noexcept(::std::is_nothrow_destructible_v<F> && ::std::is_nothrow_destructible_v<Itr>) = default;

	This &operator=(This that) noexcept(are_nothrow_move_assignable_v<F, Itr>) { return value_assign(f, ::std::move(that.f), itr, ::std::move(that.itr)), *this; }

	constexpr auto operator*() noexcept(noexcept(::std::invoke(f, *itr))) { return ::std::invoke(f, *itr); }
	constexpr auto operator*() const noexcept(noexcept(::std::invoke(f, *itr)))  { return ::std::invoke(f, *itr); }

	constexpr This &operator++() noexcept(noexcept(++itr)) { return ++itr, *this; }
	constexpr This &operator--() noexcept(noexcept(--itr)) { return --itr, *this; }

	constexpr This operator++(int) noexcept(is_nothrow_return_v<This, ::std::is_nothrow_copy_constructible_v<This>> && noexcept(++itr)) {
		This retVal{*this};
		return ++itr, retVal;
	}
	constexpr This operator--(int) noexcept(is_nothrow_return_v<This, ::std::is_nothrow_copy_constructible_v<This>> && noexcept(--itr)) {
		This retVal{*this};
		return --itr, retVal;
	}

	constexpr bool operator==(This const &that) const noexcept { return itr == that.itr; }
	template<typename Jtr> constexpr bool operator==(stop_iteration<Jtr> const &that) const noexcept { return itr == that.itr; }
	constexpr bool operator!=(This const &that) const noexcept { return itr != that.itr; }
	template<typename Jtr> constexpr bool operator!=(stop_iteration<Jtr> const &that) const noexcept { return itr != that.itr; }
};

template<class Ctr, class F, class... Fs> inline constexpr auto flatmap(Ctr &&ctr, F &&f, Fs &&...fs);

template<class Left, class Right, class F, class... Fs> class flat_iterator {
	Left left;
	Right right;

	using ElemType = decltype(*left);

	static constexpr auto continuation(F f, Fs... fs) {
		if constexpr(sizeof...(Fs) > 0) return [f = ::std::move(f), fs...](ElemType &&e) {
								return flatmap(::std::invoke(f, ::std::forward<ElemType>(e)), fs...);
							};
		else return f;
	}

	//using Continuation = decltype(continuation(::std::declval<F>(), ::std::declval<Fs>()...));
	using Continuation = ::std::invoke_result_t<decltype(continuation), F, Fs...>;
	Continuation f;

	using Range = decltype(::std::invoke(f, *left));
	Range range;

	using SubItr = decltype(adl_or_std::begin::call(range));
	SubItr elem;
public:
	using This = flat_iterator;
	using iterator_category = ::std::input_iterator_tag;
	using value_type = decltype(*adl_or_std::begin::call(::std::invoke(f, *left)));
	using difference_type = ::std::conditional_t<has_member_type_difference_type_v<Left>, uncommon_type_t<::std::ptrdiff_t, iterator::difference_t<Left>>, void>;
	using pointer = void;
	using reference = value_type;
private:
	constexpr SubItr findNext() {
		if constexpr(is_inexhaustible_v<Right>) {
			for(range = ::std::invoke(f, *left); adl_or_std::empty::call(range); range = ::std::invoke(f, *++left));
			return adl_or_std::begin::call(range);
		}
		else {
			while(!(left == right)) {
				range = ::std::invoke(f, *left);
				if(!adl_or_std::empty::call(range)) return adl_or_std::begin::call(range);
				++left;
			}
			return SubItr();
		}
	}
public:
	constexpr flat_iterator()  {}
	template<typename Fpack> constexpr flat_iterator(Left left, Right right, Fpack fs)
		: left(::std::move(left)), right(::std::move(right)), f(apply(continuation, ::std::move<Fpack>(fs))), elem(findNext()) {}
	constexpr flat_iterator(This &&) = default;
	constexpr flat_iterator(This const &that)
		: left(that.left), right(that.right), f(that.f), range(that.range), elem(left == right? SubItr() : adl_or_std::begin::call(range))
	{
		if constexpr(is_inexhaustible_v<Right>) ::std::advance(elem, ::std::distance(adl_or_std::begin::call(that.range), that.elem));
		else if(!(left == right)) ::std::advance(elem, ::std::distance(adl_or_std::begin::call(that.range), that.elem));
	}

	constexpr flat_iterator& operator=(This that) noexcept(are_nothrow_move_assignable_v<F, Left, Right, Range, SubItr>) {
		return value_assign(f, ::std::move(that.f)
			      , left, ::std::move(that.left)
			      , right, ::std::move(that.right)
			      , range, ::std::move(that.range)
			      , elem, ::std::move(that.elem))
			, *this;
	}

	constexpr decltype(auto) operator*() const noexcept(noexcept(*elem)) { return *elem; }
	constexpr decltype(auto) operator*() noexcept(noexcept(*elem)) { return *elem; }

	This &operator++() noexcept(noexcept(++elem == adl_or_std::end::call(range), ++left, elem = findNext())) {
		if(++elem == adl_or_std::end::call(range)) ++left, elem = findNext();
		return *this;
	}
	This operator++(int) noexcept(::std::is_nothrow_copy_constructible_v<This> && noexcept(*this)) {
		This retVal{*this};
		return ++*this, retVal;
	}

	constexpr bool operator==(This const &that) const noexcept(noexcept(left == right? left == that.left : left == that.left && elem == that.elem)) {
		return left == right? left == that.left : left == that.left && elem == that.elem;
	}
	template<typename Itr> constexpr bool operator==(stop_iteration<Itr> const &that) const noexcept(is_inexhaustible_v<Right> || noexcept(left == right)) {
		if constexpr(is_inexhaustible_v<Right>) return false;
		else return left == that;
	}
	constexpr bool operator!=(This const &that) const noexcept(noexcept(left == right? left == that.left : left == that.left && elem == that.elem)) {
		return left == right? left != that.left : left != that.left || elem != that.elem;
	}
	template<typename Itr> constexpr bool operator!=(stop_iteration<Itr> const &that) const noexcept(is_inexhaustible_v<Right> || noexcept(left != right)) {
		if constexpr(is_inexhaustible_v<Right>) return true;
		else return left != that;
	}
};

template<class Itr, class End, class Aftermath> struct concat_iterator {
	using This = concat_iterator;

	using iterator_category = std::forward_iterator_tag;
	using reference = decltype(*::std::declval<Itr>());
	using value_type = ::std::remove_reference_t<reference>;
	using difference_type = ::std::ptrdiff_t;
	using pointer = value_type *;

	constexpr concat_iterator(): exhausted{true} {}
	constexpr concat_iterator(Itr &&itr, End &&stop, Aftermath &&aftermath)
		: concat_iterator{fresh_iterator(::std::forward<Itr>(itr), ::std::forward<End>(stop), ::std::forward<Aftermath>(aftermath))} {}

	constexpr decltype(auto) operator*() { return exhausted? *continuation : *itr; }
	constexpr decltype(auto) operator*() const { return exhausted? *continuation : *itr; }
	constexpr This &operator++() {
		if constexpr(is_inexhaustible_v<End>) ++itr;
		else {
			if(exhausted) ++continuation;
			else if((exhausted = ++itr == stop)) continuation = postbegin(), fullStop = postend();
			return *this;
		}
	}
	constexpr This operator++(int) { This retVal(*this); return ++*this, retVal; }

	constexpr bool operator==(sentinel) const noexcept(is_inexhaustible_v<End> || noexcept(continuation == fullStop)) {
		if constexpr(is_inexhaustible_v<End>) return false;
		else return exhausted && continuation == fullStop;
	}

	constexpr bool operator!=(sentinel) const noexcept(is_inexhaustible_v<End> || noexcept(continuation != fullStop)) {
		if constexpr(is_inexhaustible_v<End>) return true;
		else return !exhausted || continuation != fullStop;
	}
private:
	template<typename Jtr, typename Dtr, typename Bfter> friend class concat_iterator;
	using Continuation = iterator_type_t<Aftermath>;
	using FullStop = end_type_t<Aftermath>;

	constexpr Continuation postbegin() noexcept(NOEXCEPT(adl_or_std::begin::call<Aftermath &>(aftermath))) {
		return adl_or_std::begin::call<Aftermath &>(aftermath);
	}
	constexpr FullStop postend() noexcept(NOEXCEPT(adl_or_std::end::call<Aftermath &>(aftermath))) { return adl_or_std::end::call<Aftermath &>(aftermath); }

	constexpr concat_iterator(private_ctor_tag_t, Itr &&itr, End &&stop, Aftermath &&aftermath)
		: itr(::std::forward<Itr>(itr)), stop(::std::forward<End>(stop)), aftermath(::std::forward<Aftermath>(aftermath)) {}
	constexpr concat_iterator(Aftermath &&amath)
		: exhausted{true}
		, aftermath(::std::forward<Aftermath>(amath)), continuation(postbegin()), fullStop(postend()) {}

	static constexpr This fresh_iterator(Itr &&itr, End &&stop, Aftermath &&aftermath) {
		if constexpr(!is_inexhaustible_v<End>) {
			if(itr == stop) {
				if constexpr(::std::is_same_v<This, Continuation>) return adl_or_std::begin::call(::std::forward<Aftermath>(aftermath));
				else return This(::std::forward<Aftermath>(aftermath));
			}
		}
		return This(private_ctor_tag_t{}, ::std::forward<Itr>(itr), ::std::forward<End>(stop), ::std::forward<Aftermath>(aftermath));
	}

	Itr itr;
	End stop;
	bool exhausted = false;
	default_constructible_t<Aftermath> aftermath;
	Continuation continuation;
	FullStop fullStop;
};
template<class Itr, class End, class Aftermath> concat_iterator(Itr &&, End &&, Aftermath &&) -> concat_iterator<Itr, End, Aftermath>;

template<class Predicate, class Itr, class End> struct filter_iterator {
	using This = filter_iterator;

	using iterator_category = ::std::input_iterator_tag;
	using reference = decltype(*::std::declval<Itr>());
	using value_type = ::std::remove_reference_t<reference>;
	using difference_type = ::std::ptrdiff_t;
	using pointer = value_type *;

	constexpr filter_iterator() = default;
	constexpr filter_iterator(Predicate &&predicate, Itr &&itr, End &&end)
		noexcept(::std::is_nothrow_move_constructible_v<Predicate> && ::std::is_nothrow_move_constructible_v<Itr> && ::std::is_nothrow_move_constructible_v<End>)
		: predicate(::std::forward<Predicate>(predicate)), itr(::std::forward<Itr>(itr)), end(::std::forward<End>(end)) {}

	constexpr decltype(auto) operator*() noexcept(noexcept(tap()) && NOEXCEPT(deref())) { tap(); return deref(); }
	constexpr decltype(auto) operator*() const noexcept(noexcept(tap()) && NOEXCEPT(deref())) { tap(); return deref(); }

	constexpr This &operator++() noexcept(noexcept(++itr, cache.reset())) { return ++itr, cache.reset(), *this; }
	constexpr This operator++(int)
		noexcept(::std::is_nothrow_copy_constructible_v<This> && noexcept(++itr, cache.reset()) && ::std::is_nothrow_move_constructible_v<This>)
	{
		This retVal(*this);
		return ++itr, cache.reset(), retVal;
	}

	constexpr bool operator==(sentinel) const noexcept(noexcept(tap()) && noexcept(itr == end)) { return itr == end || (tap(), itr == end); }
	constexpr bool operator!=(sentinel) const noexcept(noexcept(tap()) && noexcept(itr != end)) { return itr != end && (tap(), itr != end); }
private:
	constexpr reference deref() const noexcept(noexcept(cache.emplace(*itr)) && NOEXCEPT(cache)) { return cache.has_value()? *cache : cache.emplace(*itr); }
	constexpr void tap() const { if(!::std::invoke(predicate, deref())) while(++itr != end && !::std::invoke(predicate, cache.emplace(*itr))); }

	Predicate predicate;
	mutable Itr itr;
	End end;
	mutable optional<reference> cache;
};
template<class Predicate, class Itr, class End> filter_iterator(Predicate &&, Itr &&, End &&) -> filter_iterator<Predicate, Itr, End>;

template<class Predicate, class Itr> struct take_while_iterator {
	using This = take_while_iterator;

	using iterator_category = ::std::input_iterator_tag;
	using reference = decltype(*::std::declval<Itr>());
	using value_type = ::std::remove_reference_t<reference>;
	using difference_type = ::std::ptrdiff_t;
	using pointer = value_type *;

	constexpr take_while_iterator() = default;
	constexpr take_while_iterator(Predicate &&predicate, Itr &&itr) noexcept(pack::all_v<::std::is_nothrow_move_constructible, Predicate, Itr>)
		: predicate(::std::forward<Predicate>(predicate)), itr(::std::forward<Itr>(itr)) {}
	constexpr take_while_iterator(take_while_iterator &&) = default;
	constexpr take_while_iterator(take_while_iterator const &) = default;

	constexpr decltype(auto) operator*() noexcept(NOEXCEPT(deref())) { return deref(); }
	constexpr decltype(auto) operator*() const noexcept(NOEXCEPT(deref())) { return deref(); }

	constexpr This &operator++() noexcept(NOEXCEPT(++itr)) { return ++itr, cache.reset(), *this; }
	constexpr This operator++(int) {
		This retVal(*this);
		return ++itr, cache.reset(), retVal;
	}

	template<typename That> constexpr bool operator==(That const &that)
		noexcept((is_inexhaustible_v<That> && noexcept(!::std::invoke(predicate, deref()))) || noexcept(itr == that || !::std::invoke(predicate, deref())))
	{
		return itr == that || !::std::invoke(predicate, deref());
	}
	template<typename That> constexpr bool operator!=(That const &that)
		noexcept((is_inexhaustible_v<That> && noexcept(::std::invoke(predicate, deref()))) || noexcept(itr != that && ::std::invoke(predicate, deref())))
	{
		return itr != that && ::std::invoke(predicate, deref());
	}

	constexpr auto break_point() const noexcept(NOEXCEPT(itr)) { return itr; }
private:
	constexpr reference deref() noexcept(NOEXCEPT(cache.empty()? cache = *itr : cache)) { return cache.empty()? cache = *itr : cache; }

	Predicate predicate;
	Itr itr;
	optional<reference> cache;
};
template<class Predicate, class Itr> take_while_iterator(Predicate &&, Itr &&) -> take_while_iterator<Predicate, Itr>;

template<class Itr> struct take_iterator {
	using This = take_iterator;

	using iterator_category = ::std::input_iterator_tag;
	using reference = decltype(*::std::declval<Itr>());
	using value_type = ::std::remove_reference_t<reference>;
	using difference_type = ::std::ptrdiff_t;
	using pointer = value_type *;

	constexpr take_iterator() = default;
	constexpr take_iterator(::std::size_t n, Itr &&itr) noexcept(::std::is_nothrow_move_constructible_v<Itr>): n(n), itr(::std::forward<Itr>(itr)) {}
	constexpr take_iterator(take_iterator &&) = default;
	constexpr take_iterator(take_iterator const &) = default;

	constexpr decltype(auto) operator*() noexcept(NOEXCEPT(*itr)) { return *itr; }
	constexpr decltype(auto) operator*() const noexcept(NOEXCEPT(*itr)) { return *itr; }

	constexpr This &operator++() noexcept(NOEXCEPT(++itr)) { return ++itr, --n, *this; }
	constexpr This operator++(int) {
		This retVal(*this);
		return ++itr, --n, retVal;
	}

	template<typename That> constexpr bool operator==(That &&that) const noexcept(noexcept(itr == that)) { return !n || itr == that; }
	template<typename That> constexpr bool operator!=(That &&that) const noexcept(noexcept(itr != that)) { return n && itr != that; }

	constexpr auto break_point() const noexcept(NOEXCEPT(itr)) { return itr; }
private:
	::std::size_t n;
	Itr itr;
};
template<class Itr> take_iterator(::std::size_t, Itr &&) -> take_iterator<Itr>;

}

#ifndef SHOW_NOTHING

#include "show.h"

namespace nstd {

SHOW_NSTD_SIMPLE(infinity);

SHOW_NSTD_ROOT(1, stop_iteration);
SHOW_NSTD_ROOT(1, join_iterator);
SHOW_NSTD_ROOT(1, zip_iterator);
SHOW_NSTD_ROOT(1, mzip_iterator);
SHOW_NSTD_ROOT(1, transform_iterator);
SHOW_NSTD_ROOT(1, flat_iterator);
SHOW_NSTD_ROOT(1, concat_iterator);
SHOW_NSTD_ROOT(1, filter_iterator);
SHOW_NSTD_ROOT(1, take_while_iterator);
SHOW_NSTD_ROOT(1, take_iterator);

}

#endif

#endif
