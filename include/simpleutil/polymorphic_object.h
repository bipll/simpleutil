/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__DUCK_STORAGE_H_8631777a0bed2ae7c1dbc8f332d9fc22f1bd1957
#define SIMPLEUTIL__DUCK_STORAGE_H_8631777a0bed2ae7c1dbc8f332d9fc22f1bd1957

#include <any>
#include <type_traits>
#include <functional>
#include "typeutil.h"
#include "tuple.h"

namespace nstd {

template<class Proto, class... Interface> struct polymorphic_object: ::std::any {
	using This = polymorphic_object;
	using Parent = any;

	constexpr polymorphic_object() noexcept(::std::is_nothrow_constructible_v<Slots>): clone([](polymorphic_object &) constexpr { return Slots{}; }) {}
	constexpr polymorphic_object(polymorphic_object &&that)
		: any(::std::forward<any>(that)), vtable{::std::move(that.vtable)}, clone{::std::move(that.clone)}, slots{clone(*this)} {}
	constexpr polymorphic_object(polymorphic_object const &that)
		: any(static_cast<any const &>(that)), vtable{that.vtable}, clone{that.clone}, slots{clone(*this)} {}

	template<class Qroto, class... Jnterface> constexpr polymorphic_object(Qroto &&proto, Jnterface &&...interface)
		: any(::std::forward<Qroto>(proto))
		, vtable{::std::forward<Jnterface>(interface)...}
		, clone{[](polymorphic_object &fresh) constexpr {
				return fresh.make_slots(fresh.as<Qroto>(), ::std::index_sequence_for<Interface...>{});
			}}
		, slots{make_slots(as<Qroto>(), ::std::index_sequence_for<Interface...>{})}
	{}

	constexpr polymorphic_object &operator=(polymorphic_object &&that) {
		slots = {};
		return any::operator=(::std::forward<any>(that)), vtable = ::std::move(that.vtable), clone = ::std::move(that.clone), slots = clone(*this), *this;
	}
	constexpr polymorphic_object &operator=(polymorphic_object const &that) {
		slots = {};
		return any::operator=(static_cast<any const &>(that)), vtable = that.vtable, clone = that.clone, slots = that.clone(*this), *this;
	}
	template<class Object> constexpr polymorphic_object &operator=(Object &&that) {
		slots = {};
		return any::operator=(::std::forward<Object>(that))
			, clone = [](polymorphic_object &fresh) constexpr {
					return fresh.make_slots(fresh.as<Object>(), ::std::index_sequence_for<Interface...>{});
				}
			, slots = make_slots(as<Object>(), ::std::index_sequence_for<Interface...>{})
			, *this;
	}

	template<class Method> decltype(auto) invoke(Method &&m) {
		if constexpr(::std::disjunction_v<::std::is_same<Method, Interface>...>) return ::std::get<Slot<Method>>(slots)();
		else return ::std::invoke(m, *this);
	}
	template<class Method> decltype(auto) invoke(Method &&m) const {
		if constexpr(::std::disjunction_v<::std::is_same<Method, Interface>...>) return ::std::get<Slot<Method>>(slots)();
		else return ::std::invoke(m, *this);
	}

	template<std::size_t i> decltype(auto) invoke() { return ::std::get<i>(slots)(); }
	template<std::size_t i> decltype(auto) invoke() const { return ::std::get<i>(slots)(); }
protected:
	using VTable = tuple<Interface...>;
	template<class Method> using Slot = ::std::function<::std::invoke_result_t<Method, Proto>()>;
	using Slots = tuple<Slot<Interface>...>;
	using Clone = ::std::function<Slots(polymorphic_object &)>;

	template<class Value> Value &as() noexcept { return *::std::any_cast<::std::decay_t<Value>>(this); }
	template<class Value> Value const &as() const noexcept { return *::std::any_cast<::std::decay_t<Value>>(this); }

	template<::std::size_t i, class Object> auto make_slot(Object &object) {
		if constexpr(::std::is_invocable_v<tuple_element_t<i, VTable>, ::std::add_lvalue_reference_t<Object>>) {
			return ::std::bind(::std::get<i>(vtable), ::std::ref(object));
		}
		else return ::std::bind(::std::get<i>(vtable), ::std::ref(::std::as_const(object)));
	}
	template<class Object, ::std::size_t... is> Slots make_slots(Object &object, ::std::index_sequence<is...>)
		noexcept(NOEXCEPT(Slots{make_slot<is>(object)...}))
	{
		return {make_slot<is>(object)...};
	}

	VTable vtable;
	Clone clone;
	Slots slots;
};
template<class Proto, class... Interface> polymorphic_object(Proto &&, Interface &&...) -> polymorphic_object<Proto, Interface...>;

template<class F, class Proto, class... Interface> inline constexpr decltype(auto) invoke(F &&f, polymorphic_object<Proto, Interface...> &polymorphic) {
	return polymorphic.invoke(::std::forward<F>(f));
}
template<class F, class Proto, class... Interface> inline constexpr decltype(auto) invoke(F &&f, polymorphic_object<Proto, Interface...> const  &polymorphic) {
	return polymorphic.invoke(::std::forward<F>(f));
}

}

#ifndef SHOW_NOTHING

#include "show.h"

namespace nstd {

SHOW_NSTD_ROOT(1, polymorphic_object);

}

#endif

#endif
