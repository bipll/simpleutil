/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__STRING_H_7eb3742376866c664c310ef599726c377f4f0b04
#define __SHOW__STRING_H_7eb3742376866c664c310ef599726c377f4f0b04

#if !defined SHOW_NO_STD || defined SHOW_STD_STRING

#ifndef SIMPLEUTIL____SHOW__STRING_H_479f96fd742a3a3f4d537833b70658a41d4b9d84
#define SIMPLEUTIL____SHOW__STRING_H_479f96fd742a3a3f4d537833b70658a41d4b9d84

#include <string>

namespace nstd {

SHOW_STD_ROOT(1, char_traits);
SHOW_STD_ROOT(1, basic_string);
SHOW_STD_SIMPLE(string);
SHOW_STD_SIMPLE(wstring);
SHOW_STD_SIMPLE(u16string);
SHOW_STD_SIMPLE(u32string);

}

#endif

#endif

#endif
