/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__CHARCONV_H_c838deeccf465de1f02e16cc2393fdbd0bd97d23
#define __SHOW__CHARCONV_H_c838deeccf465de1f02e16cc2393fdbd0bd97d23

#if !defined SHOW_NO_STD || defined SHOW_STD_CHARCONV

#ifndef SIMPLEUTIL____SHOW__CHARCONV_H_45cb43308991421c7d1c52b423a7ff37e6a593b7
#define SIMPLEUTIL____SHOW__CHARCONV_H_45cb43308991421c7d1c52b423a7ff37e6a593b7

#if __has_include(<charconv>)

#include <charconv>

namespace nstd {

// SHOW_STD_SIMPLE(chars_format);

}

#endif

#endif

#endif

#endif
