/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__QUEUE_H_14431fa5d1afcd5d7181f8ff85e13c46c3cb1ebf
#define __SHOW__QUEUE_H_14431fa5d1afcd5d7181f8ff85e13c46c3cb1ebf

#if !defined SHOW_NO_STD || defined SHOW_STD_QUEUE

#ifndef SIMPLEUTIL____SHOW__QUEUE_H_36362413e9988d585fc5be592dbb9693839d45e6
#define SIMPLEUTIL____SHOW__QUEUE_H_36362413e9988d585fc5be592dbb9693839d45e6

#include <queue>

namespace nstd {

SHOW_STD_ROOT(1, queue);
SHOW_STD_ROOT(1, priority_queue);

}

#endif

#endif

#endif
