/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__CSTDDEF_H_c656e701948c13bba8f65fb8cb8be7db2a760caa
#define __SHOW__CSTDDEF_H_c656e701948c13bba8f65fb8cb8be7db2a760caa

#if !defined SHOW_NO_STD || defined SHOW_STD_CSTDDEF

#ifndef SIMPLEUTIL____SHOW__CSTDDEF_H_3e3acf8b946cec169eb86eccc76fd5508569a5d1
#define SIMPLEUTIL____SHOW__CSTDDEF_H_3e3acf8b946cec169eb86eccc76fd5508569a5d1

#include <cstddef>

namespace nstd {

SHOW_STD_SIMPLE(max_align_t);
SHOW_STD_SIMPLE(byte);

}

#endif

#endif

#endif
