/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__FUNCTIONAL_H_5fbdfaca2e956975614f27693035721457966548
#define __SHOW__FUNCTIONAL_H_5fbdfaca2e956975614f27693035721457966548

#if !defined SHOW_NO_STD || defined SHOW_STD_FUNCTIONAL

#ifndef SIMPLEUTIL____SHOW__FUNCTIONAL_H_05f9ab82e39243d83008af2c21d10bb328652175
#define SIMPLEUTIL____SHOW__FUNCTIONAL_H_05f9ab82e39243d83008af2c21d10bb328652175

#include <functional>

namespace nstd {

SHOW_STD_ROOT(1, function);
SHOW_STD_SIMPLE(bad_function_call);
SHOW_STD_ROOT(1, is_bind_expression);
SHOW_STD_ROOT(1, is_placeholder);
SHOW_STD_ROOT(1, reference_wrapper);
SHOW_STD_ROOT(1, plus);
SHOW_STD_ROOT(1, minus);
SHOW_STD_ROOT(1, multiplies);
SHOW_STD_ROOT(1, divides);
SHOW_STD_ROOT(1, modulus);
SHOW_STD_ROOT(1, negate);
SHOW_STD_ROOT(1, equal_to);
SHOW_STD_ROOT(1, not_equal_to);
SHOW_STD_ROOT(1, greater);
SHOW_STD_ROOT(1, less);
SHOW_STD_ROOT(1, greater_equal);
SHOW_STD_ROOT(1, less_equal);
SHOW_STD_ROOT(1, logical_and);
SHOW_STD_ROOT(1, logical_or);
SHOW_STD_ROOT(1, logical_not);
SHOW_STD_ROOT(1, bit_and);
SHOW_STD_ROOT(1, bit_or);
SHOW_STD_ROOT(1, bit_xor);
SHOW_STD_ROOT(1, bit_not);
/*
SHOW_STD_ROOT(1, default_searcher);
SHOW_STD_ROOT(1, boyer_moore_searcher);
SHOW_STD_ROOT(1, boyer_moore_horspool_searcher);
*/

}

#endif

#endif

#endif
