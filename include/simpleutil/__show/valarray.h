/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__VALARRAY_H_f1abf3ce63e5316e959c717497b5c003d12d2d9e
#define __SHOW__VALARRAY_H_f1abf3ce63e5316e959c717497b5c003d12d2d9e

#if !defined SHOW_NO_STD || defined SHOW_STD_VALARRAY

#ifndef SIMPLEUTIL____SHOW__VALARRAY_H_aae5e710d5402fa41caff2a66d9c6bf24307456c
#define SIMPLEUTIL____SHOW__VALARRAY_H_aae5e710d5402fa41caff2a66d9c6bf24307456c

#include <valarray>

namespace nstd {

SHOW_STD_ROOT(1, valarray);
SHOW_STD_SIMPLE(slice);
SHOW_STD_ROOT(1, slice_array);
SHOW_STD_SIMPLE(gslice);
SHOW_STD_ROOT(1, gslice_array);
SHOW_STD_ROOT(1, mask_array);
SHOW_STD_ROOT(1, indirect_array);

}

#endif

#endif

#endif
