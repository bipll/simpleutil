/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__REGEX_H_7be4b1d70b5374115db5bfabf5c3dc125bc2dfb0
#define __SHOW__REGEX_H_7be4b1d70b5374115db5bfabf5c3dc125bc2dfb0

#if !defined SHOW_NO_STD || defined SHOW_STD_REGEX

#ifndef SIMPLEUTIL____SHOW__REGEX_H_3495ff65666917287678bb0b8e0d6dc0780c1c83
#define SIMPLEUTIL____SHOW__REGEX_H_3495ff65666917287678bb0b8e0d6dc0780c1c83

#include <regex>

namespace nstd {

SHOW_STD_SIMPLE(regex_error);
SHOW_STD_ROOT(1, regex_traits);
SHOW_STD_ROOT(1, basic_regex);
SHOW_STD_ROOT(1, sub_match);
SHOW_STD_ROOT(1, match_results);
SHOW_STD_ROOT(1, regex_iterator);
SHOW_STD_ROOT(1, regex_token_iterator);

}

#endif

#endif

#endif
