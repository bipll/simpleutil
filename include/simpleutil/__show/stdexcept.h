/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__STDEXCEPT_H_050184f533f23b47032be2768996abacb1bba4cb
#define __SHOW__STDEXCEPT_H_050184f533f23b47032be2768996abacb1bba4cb

#if !defined SHOW_NO_STD || defined SHOW_STD_STDEXCEPT

#ifndef SIMPLEUTIL____SHOW__STDEXCEPT_H_b955ae1dbd8fd841e96c0596e4346440d598635f
#define SIMPLEUTIL____SHOW__STDEXCEPT_H_b955ae1dbd8fd841e96c0596e4346440d598635f

#include <stdexcept>

namespace nstd {

SHOW_STD_SIMPLE(logic_error);
SHOW_STD_SIMPLE(invalid_argument);
SHOW_STD_SIMPLE(domain_error);
SHOW_STD_SIMPLE(length_error);
SHOW_STD_SIMPLE(out_of_range);
SHOW_STD_SIMPLE(runtime_error);
SHOW_STD_SIMPLE(range_error);
SHOW_STD_SIMPLE(overflow_error);
SHOW_STD_SIMPLE(underflow_error);

}

#endif

#endif

#endif
