/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__ARRAY_H_4d8c7a78e3fbcc5fa603dd78df154288ce7e491e
#define __SHOW__ARRAY_H_4d8c7a78e3fbcc5fa603dd78df154288ce7e491e

#if !defined SHOW_NO_STD || defined SHOW_STD_ARRAY

#ifndef __SHOW__ARRAY_H_1ed28b3104897d9005d25dd05d0c59c4f33a3ac0
#define __SHOW__ARRAY_H_1ed28b3104897d9005d25dd05d0c59c4f33a3ac0

#include <array>

namespace nstd {

SHOW_STD_ROOT(12, array);

}

#endif

#endif

#endif
