/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__TUPLE_H_a220622f5073609811585214dbf65df6d73b75e7
#define __SHOW__TUPLE_H_a220622f5073609811585214dbf65df6d73b75e7

#if !defined SHOW_NO_STD || defined SHOW_STD_TUPLE

#ifndef SIMPLEUTIL____SHOW__TUPLE_H_e863c5a387c82b79a8f09185e08481bb4860c1ab
#define SIMPLEUTIL____SHOW__TUPLE_H_e863c5a387c82b79a8f09185e08481bb4860c1ab

#include <tuple>

namespace nstd {

SHOW_STD_ROOT(1, tuple);
SHOW_STD_ROOT(1, tuple_size);
SHOW_STD_ROOT(21, tuple_element);

}

#endif

#endif

#endif
