/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__FUTURE_H_4eb31e07f78b172674b868860ce109a395164723
#define __SHOW__FUTURE_H_4eb31e07f78b172674b868860ce109a395164723

#if !defined SHOW_NO_STD || defined SHOW_STD_FUTURE

#ifndef SIMPLEUTIL____SHOW__FUTURE_H_a247321400dfda73f3dc5b276e5e2d8bc9295b88
#define SIMPLEUTIL____SHOW__FUTURE_H_a247321400dfda73f3dc5b276e5e2d8bc9295b88

#include <future>

namespace nstd {

SHOW_STD_SIMPLE(future_errc);
SHOW_STD_SIMPLE(launch);
SHOW_STD_SIMPLE(future_status);
SHOW_STD_SIMPLE(future_error);
SHOW_STD_ROOT(1, promise);
SHOW_STD_ROOT(1, future);
SHOW_STD_ROOT(1, shared_future);
SHOW_STD_ROOT(1, packaged_task);

}

#endif

#endif

#endif
