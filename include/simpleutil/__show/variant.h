/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__VARIANT_H_ac4d51d9e77617587fb4c7388d56940012888474
#define __SHOW__VARIANT_H_ac4d51d9e77617587fb4c7388d56940012888474

#if !defined SHOW_NO_STD || defined SHOW_STD_VARIANT

#ifndef SIMPLEUTIL____SHOW__VARIANT_H_e314ad104af52505ea18980b7018dfee5a293518
#define SIMPLEUTIL____SHOW__VARIANT_H_e314ad104af52505ea18980b7018dfee5a293518

#include <variant>

namespace nstd {

SHOW_STD_ROOT(1, variant);
SHOW_STD_ROOT(1, variant_size);
SHOW_STD_ROOT(21, variant_alternative);
SHOW_STD_SIMPLE(monostate);
SHOW_STD_SIMPLE(bad_variant_access);

}

#endif

#endif

#endif
