/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__ITERATOR_H_7f33bc290907bc5f7a44582c9e089ebc43ec99cd
#define __SHOW__ITERATOR_H_7f33bc290907bc5f7a44582c9e089ebc43ec99cd

#if !defined SHOW_NO_STD || defined SHOW_STD_ITERATOR

#ifndef SIMPLEUTIL____SHOW__ITERATOR_H_9a05f17c8b3d9dffe3b8c974fd428e66b0034d37
#define SIMPLEUTIL____SHOW__ITERATOR_H_9a05f17c8b3d9dffe3b8c974fd428e66b0034d37

#include <iterator>

namespace nstd {

SHOW_STD_ROOT(1, iterator_traits);
SHOW_STD_ROOT(1, iterator);
SHOW_STD_SIMPLE(input_iterator_tag);
SHOW_STD_SIMPLE(output_iterator_tag);
SHOW_STD_SIMPLE(forward_iterator_tag);
SHOW_STD_SIMPLE(bidirectional_iterator_tag);
SHOW_STD_SIMPLE(random_access_iterator_tag);
SHOW_STD_ROOT(1, reverse_iterator);
SHOW_STD_ROOT(1, back_insert_iterator);
SHOW_STD_ROOT(1, front_insert_iterator);
SHOW_STD_ROOT(1, insert_iterator);
SHOW_STD_ROOT(1, move_iterator);
SHOW_STD_ROOT(1, istream_iterator);
SHOW_STD_ROOT(1, ostream_iterator);
SHOW_STD_ROOT(1, istreambuf_iterator);
SHOW_STD_ROOT(1, ostreambuf_iterator);

}

#endif

#endif

#endif
