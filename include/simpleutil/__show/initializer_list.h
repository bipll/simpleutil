/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__INITIALIZER_LIST_H_0650e6f117aeafea9e02c2c705a64481a0c83c4b
#define __SHOW__INITIALIZER_LIST_H_0650e6f117aeafea9e02c2c705a64481a0c83c4b

#if !defined SHOW_NO_STD || defined SHOW_STD_INITIALIZER_LIST

#ifndef SIMPLEUTIL____SHOW__INITIALIZER_LIST_H_0a48ac11ce833867f0f2ff269e152223b4881544
#define SIMPLEUTIL____SHOW__INITIALIZER_LIST_H_0a48ac11ce833867f0f2ff269e152223b4881544

#include <initializer_list>

namespace nstd {

SHOW_STD_ROOT(1, initializer_list);

}

#endif

#endif

#endif
