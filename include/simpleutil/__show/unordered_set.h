/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__UNORDERED_SET_H_7f08f83892c8045f89d65d6cbbd4a89ccd4e8063
#define __SHOW__UNORDERED_SET_H_7f08f83892c8045f89d65d6cbbd4a89ccd4e8063

#if !defined SHOW_NO_STD || defined SHOW_STD_UNORDERED_SET

#ifndef SIMPLEUTIL____SHOW__UNORDERED_SET_H_1c8016faa6bdfc7d9a5b1ad55ca4e3d7ebba432f
#define SIMPLEUTIL____SHOW__UNORDERED_SET_H_1c8016faa6bdfc7d9a5b1ad55ca4e3d7ebba432f

#include <unordered_set>

namespace nstd {

SHOW_STD_ROOT(1, unordered_set);
SHOW_STD_ROOT(1, unordered_multiset);

}

#endif

#endif

#endif
