/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__FSTREAM_H_f2d338b21399da997a8143cae0f2de0d19dc8697
#define __SHOW__FSTREAM_H_f2d338b21399da997a8143cae0f2de0d19dc8697

#if !defined SHOW_NO_STD || defined SHOW_STD_FSTREAM

#ifndef SIMPLEUTIL____SHOW__FSTREAM_H_f7ebacd3fe86efa08ea62c87d47b682538928316
#define SIMPLEUTIL____SHOW__FSTREAM_H_f7ebacd3fe86efa08ea62c87d47b682538928316

#include <fstream>

namespace nstd {

SHOW_STD_ROOT(1, basic_filebuf);
SHOW_STD_ROOT(1, basic_ifstream);
SHOW_STD_ROOT(1, basic_ofstream);
SHOW_STD_ROOT(1, basic_fstream);

}

#endif

#endif

#endif
