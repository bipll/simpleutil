/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__DEQUE_H_e3e0da7909ccb2d3efc62e789ea6aae498ed30e2
#define __SHOW__DEQUE_H_e3e0da7909ccb2d3efc62e789ea6aae498ed30e2

#if !defined SHOW_NO_STD || defined SHOW_STD_DEQUE

#ifndef SIMPLEUTIL____SHOW__DEQUE_H_1354e66a7049e6d8907aae1d5f2c5e168eee85cf
#define SIMPLEUTIL____SHOW__DEQUE_H_1354e66a7049e6d8907aae1d5f2c5e168eee85cf

#include <deque>

namespace nstd {

SHOW_STD_ROOT(1, deque);

}

#endif

#endif

#endif
