/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__TYPE_TRAITS_H_e56ad6a3087f525e3e559624190786fcd487ad69
#define __SHOW__TYPE_TRAITS_H_e56ad6a3087f525e3e559624190786fcd487ad69

#if !defined SHOW_NO_STD || defined SHOW_STD_TYPE_TRAITS

#ifndef SIMPLEUTIL____SHOW__TYPE_TRAITS_H_289eece77482e39140bdf47049d880a11b9e5946
#define SIMPLEUTIL____SHOW__TYPE_TRAITS_H_289eece77482e39140bdf47049d880a11b9e5946

#include <type_traits>

namespace nstd {

SHOW_STD_ROOT(1, add_const);
SHOW_STD_ROOT(1, add_cv);
SHOW_STD_ROOT(1, add_lvalue_reference);
SHOW_STD_ROOT(1, add_pointer);
SHOW_STD_ROOT(1, add_rvalue_reference);
SHOW_STD_ROOT(1, add_volatile);
SHOW_STD_ROOT(2, aligned_storage);
//SHOW_STD_ROOT(21, aligned_union);
SHOW_STD_ROOT(1, alignment_of);
SHOW_STD_ROOT(1, common_type);
SHOW_STD_ROOT(21, conditional);
SHOW_STD_ROOT(1, conjunction);
SHOW_STD_ROOT(1, decay);
SHOW_STD_ROOT(1, disjunction);
SHOW_STD_ROOT(21, enable_if);
SHOW_STD_ROOT(12, extent);
SHOW_STD_ROOT(1, has_unique_object_representations);
SHOW_STD_ROOT(1, has_virtual_destructor);
SHOW_STD_ROOT(12, integral_constant);
SHOW_STD_ROOT(1, invoke_result);
SHOW_STD_ROOT(1, is_abstract);
SHOW_STD_ROOT(1, is_aggregate);
SHOW_STD_ROOT(1, is_arithmetic);
SHOW_STD_ROOT(1, is_array);
SHOW_STD_ROOT(1, is_assignable);
SHOW_STD_ROOT(1, is_base_of);
SHOW_STD_ROOT(1, is_class);
SHOW_STD_ROOT(1, is_compound);
SHOW_STD_ROOT(1, is_const);
SHOW_STD_ROOT(1, is_constructible);
SHOW_STD_ROOT(1, is_convertible);
SHOW_STD_ROOT(1, is_copy_assignable);
SHOW_STD_ROOT(1, is_copy_constructible);
SHOW_STD_ROOT(1, is_default_constructible);
SHOW_STD_ROOT(1, is_destructible);
SHOW_STD_ROOT(1, is_empty);
SHOW_STD_ROOT(1, is_enum);
SHOW_STD_ROOT(1, is_final);
SHOW_STD_ROOT(1, is_floating_point);
SHOW_STD_ROOT(1, is_function);
SHOW_STD_ROOT(1, is_fundamental);
SHOW_STD_ROOT(1, is_integral);
SHOW_STD_ROOT(1, is_invocable);
SHOW_STD_ROOT(1, is_invocable_r);
SHOW_STD_ROOT(1, is_lvalue_reference);
SHOW_STD_ROOT(1, is_member_function_pointer);
SHOW_STD_ROOT(1, is_member_object_pointer);
SHOW_STD_ROOT(1, is_member_pointer);
SHOW_STD_ROOT(1, is_move_assignable);
SHOW_STD_ROOT(1, is_move_constructible);
SHOW_STD_ROOT(1, is_nothrow_assignable);
SHOW_STD_ROOT(1, is_nothrow_constructible);
SHOW_STD_ROOT(1, is_nothrow_copy_assignable);
SHOW_STD_ROOT(1, is_nothrow_copy_constructible);
SHOW_STD_ROOT(1, is_nothrow_default_constructible);
SHOW_STD_ROOT(1, is_nothrow_destructible);
SHOW_STD_ROOT(1, is_nothrow_invocable);
SHOW_STD_ROOT(1, is_nothrow_invocable_r);
SHOW_STD_ROOT(1, is_nothrow_move_assignable);
SHOW_STD_ROOT(1, is_nothrow_move_constructible);
SHOW_STD_ROOT(1, is_nothrow_swappable);
SHOW_STD_ROOT(1, is_nothrow_swappable_with);
SHOW_STD_ROOT(1, is_null_pointer);
SHOW_STD_ROOT(1, is_object);
SHOW_STD_ROOT(1, is_pod);
SHOW_STD_ROOT(1, is_pointer);
SHOW_STD_ROOT(1, is_polymorphic);
SHOW_STD_ROOT(1, is_reference);
SHOW_STD_ROOT(1, is_rvalue_reference);
SHOW_STD_ROOT(1, is_same);
SHOW_STD_ROOT(1, is_scalar);
SHOW_STD_ROOT(1, is_signed);
SHOW_STD_ROOT(1, is_standard_layout);
SHOW_STD_ROOT(1, is_swappable);
SHOW_STD_ROOT(1, is_swappable_with);
SHOW_STD_ROOT(1, is_trivial);
SHOW_STD_ROOT(1, is_trivially_assignable);
SHOW_STD_ROOT(1, is_trivially_constructible);
SHOW_STD_ROOT(1, is_trivially_copy_assignable);
SHOW_STD_ROOT(1, is_trivially_copy_constructible);
SHOW_STD_ROOT(1, is_trivially_copyable);
SHOW_STD_ROOT(1, is_trivially_default_constructible);
SHOW_STD_ROOT(1, is_trivially_destructible);
SHOW_STD_ROOT(1, is_trivially_move_assignable);
SHOW_STD_ROOT(1, is_trivially_move_constructible);
SHOW_STD_ROOT(1, is_union);
SHOW_STD_ROOT(1, is_unsigned);
SHOW_STD_ROOT(1, is_void);
SHOW_STD_ROOT(1, is_volatile);
SHOW_STD_ROOT(1, make_signed);
SHOW_STD_ROOT(1, make_unsigned);
SHOW_STD_ROOT(1, negation);
SHOW_STD_ROOT(1, rank);
SHOW_STD_ROOT(1, remove_all_extents);
SHOW_STD_ROOT(1, remove_const);
SHOW_STD_ROOT(1, remove_cv);
SHOW_STD_ROOT(1, remove_extent);
SHOW_STD_ROOT(1, remove_pointer);
SHOW_STD_ROOT(1, remove_reference);
SHOW_STD_ROOT(1, remove_volatile);
SHOW_STD_ROOT(1, result_of);
SHOW_STD_ROOT(1, underlying_type);

}

#endif

#endif

#endif
