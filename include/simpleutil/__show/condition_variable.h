/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__CONDITION_VARIABLE_H_580c34218f1cfad8bec3d963bc5db32732b9eaf5
#define __SHOW__CONDITION_VARIABLE_H_580c34218f1cfad8bec3d963bc5db32732b9eaf5

#if !defined SHOW_NO_STD || defined SHOW_STD_CONDITION_VARIABLE

#ifndef SIMPLEUTIL____SHOW__CONDITION_VARIABLE_H_e0219d38a2cf229c0c11445b136b38b682a770c6
#define SIMPLEUTIL____SHOW__CONDITION_VARIABLE_H_e0219d38a2cf229c0c11445b136b38b682a770c6

#include <condition_variable>

namespace nstd {

SHOW_STD_SIMPLE(condition_variable);
SHOW_STD_SIMPLE(condition_variable_any);
SHOW_STD_SIMPLE(cv_status);

}

#endif

#endif

#endif
