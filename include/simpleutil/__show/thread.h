/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__THREAD_H_29c75bed82788ac6bc7d5528f6f8aab89b4d1e1d
#define __SHOW__THREAD_H_29c75bed82788ac6bc7d5528f6f8aab89b4d1e1d

#if !defined SHOW_NO_STD || defined SHOW_STD_THREAD

#ifndef SIMPLEUTIL____SHOW__THREAD_H_5f503471af41beea941df134ebe524bb72dae53f
#define SIMPLEUTIL____SHOW__THREAD_H_5f503471af41beea941df134ebe524bb72dae53f

#include <thread>

namespace nstd {

SHOW_STD_SIMPLE(thread);

}

#endif

#endif

#endif
