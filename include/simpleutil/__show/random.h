/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__RANDOM_H_6d69caa704014dd46e221a258e2713ce7cd1757d
#define __SHOW__RANDOM_H_6d69caa704014dd46e221a258e2713ce7cd1757d

#if !defined SHOW_NO_STD || defined SHOW_STD_RANDOM

#ifndef SIMPLEUTIL____SHOW__RANDOM_H_1c5a4f40bdebeec60abfd8b1e7ac356b55e8889f
#define SIMPLEUTIL____SHOW__RANDOM_H_1c5a4f40bdebeec60abfd8b1e7ac356b55e8889f

#include <random>

namespace nstd {

SHOW_STD_ROOT(12, subtract_with_carry_engine);
SHOW_STD_ROOT(12, discard_block_engine);
SHOW_STD_ROOT(121, independent_bits_engine);
SHOW_STD_ROOT(12, shuffle_order_engine);
SHOW_STD_SIMPLE(random_device);
SHOW_STD_SIMPLE(seed_seq);
SHOW_STD_ROOT(1, uniform_int_distribution);
SHOW_STD_ROOT(1, uniform_real_distribution);
SHOW_STD_SIMPLE(bernoulli_distribution);
SHOW_STD_ROOT(1, binomial_distribution);
SHOW_STD_ROOT(1, geometric_distribution);
SHOW_STD_ROOT(1, negative_binomial_distribution);
SHOW_STD_ROOT(1, poisson_distribution);
SHOW_STD_ROOT(1, exponential_distribution);
SHOW_STD_ROOT(1, gamma_distribution);
SHOW_STD_ROOT(1, weibull_distribution);
SHOW_STD_ROOT(1, extreme_value_distribution);
SHOW_STD_ROOT(1, normal_distribution);
SHOW_STD_ROOT(1, lognormal_distribution);
SHOW_STD_ROOT(1, chi_squared_distribution);
SHOW_STD_ROOT(1, cauchy_distribution);
SHOW_STD_ROOT(1, fisher_f_distribution);
SHOW_STD_ROOT(1, student_t_distribution);
SHOW_STD_ROOT(1, discrete_distribution);
SHOW_STD_ROOT(1, piecewise_constant_distribution);
SHOW_STD_ROOT(1, piecewise_linear_distribution);

}

#endif

#endif

#endif
