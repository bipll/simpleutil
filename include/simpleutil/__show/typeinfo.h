/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__TYPEINFO_H_d97c98aab01b6b2fdda1c56052027eec876e96f7
#define __SHOW__TYPEINFO_H_d97c98aab01b6b2fdda1c56052027eec876e96f7

#if !defined SHOW_NO_STD || defined SHOW_STD_TYPEINFO

#ifndef SIMPLEUTIL____SHOW__TYPEINFO_H_e74dc4de74fb015f20f5baceaeb7ac9fc56ef9fb
#define SIMPLEUTIL____SHOW__TYPEINFO_H_e74dc4de74fb015f20f5baceaeb7ac9fc56ef9fb

#include <typeinfo>

namespace nstd {

SHOW_STD_SIMPLE(type_info);
SHOW_STD_SIMPLE(bad_cast);
SHOW_STD_SIMPLE(bad_typeid);

}

#endif

#endif

#endif
