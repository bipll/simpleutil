/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__LIMITS_H_0c06be8d184650b0eb325cfb96d7138c04b680d5
#define __SHOW__LIMITS_H_0c06be8d184650b0eb325cfb96d7138c04b680d5

#if !defined SHOW_NO_STD || defined SHOW_STD_LIMITS

#ifndef SIMPLEUTIL____SHOW__LIMITS_H_86e1fd3033c7a184f7769c6c0edc9b1862378a69
#define SIMPLEUTIL____SHOW__LIMITS_H_86e1fd3033c7a184f7769c6c0edc9b1862378a69

#include <limits>

namespace nstd {

SHOW_STD_ROOT(1, numeric_limits);
SHOW_STD_SIMPLE(float_round_style);
SHOW_STD_SIMPLE(float_denorm_style);

}

#endif

#endif

#endif
