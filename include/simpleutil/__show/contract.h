/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__CONTRACT_H_7eb9ce7c536ab786d757b1dff06b4dc522e55206
#define __SHOW__CONTRACT_H_7eb9ce7c536ab786d757b1dff06b4dc522e55206

#if !defined SHOW_NO_STD || defined SHOW_STD_CONTRACT

#ifndef SIMPLEUTIL____SHOW__CONTRACT_H_d46dddda5b9d59ca5b4e0aaaa17919ce80c42561
#define SIMPLEUTIL____SHOW__CONTRACT_H_d46dddda5b9d59ca5b4e0aaaa17919ce80c42561

#if __has_include(<contract>)

#include <contract>

namespace nstd {

SHOW_STD_SIMPLE(contract_violation);

}

#endif

#endif

#endif

#endif
