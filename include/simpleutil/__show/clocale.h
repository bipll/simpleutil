/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__CLOCALE_H_38c10d63f82371a7fd6e85ff83f393cea3214366
#define __SHOW__CLOCALE_H_38c10d63f82371a7fd6e85ff83f393cea3214366

#if !defined SHOW_NO_STD || defined SHOW_STD_CLOCALE

#ifndef SIMPLEUTIL____SHOW__CLOCALE_H_1f4f78ef9fd1021991760a6bbf4d5f6a30007f43
#define SIMPLEUTIL____SHOW__CLOCALE_H_1f4f78ef9fd1021991760a6bbf4d5f6a30007f43

#include <clocale>

namespace nstd {

SHOW_STD_SIMPLE(lconv);

}

#endif

#endif

#endif
