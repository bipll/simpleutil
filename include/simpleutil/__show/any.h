/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__ANY_H_73e05c35dd2d0bbf5a3f2b3e5529246bd7f6b650
#define __SHOW__ANY_H_73e05c35dd2d0bbf5a3f2b3e5529246bd7f6b650

#if !defined SHOW_NO_STD || defined SHOW_STD_ANY

#ifndef __SHOW__ANY_H_1040487b73d2ef9c28aa5f3cc011bde06b321beb
#define __SHOW__ANY_H_1040487b73d2ef9c28aa5f3cc011bde06b321beb

#include <any>

namespace nstd {

SHOW_STD_SIMPLE(any);
SHOW_STD_SIMPLE(bad_any_cast);

}

#endif

#endif

#endif
