/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__ATOMIC_H_bf0f3e365aca6c771db7210237ea194d509a9e38
#define __SHOW__ATOMIC_H_bf0f3e365aca6c771db7210237ea194d509a9e38

#if !defined SHOW_NO_STD || defined SHOW_STD_ATOMIC

#ifndef SIMPLEUTIL____SHOW__ATOMIC_H_5db1263e6c8fb8d5ce454fb186017a5fe390ed96
#define SIMPLEUTIL____SHOW__ATOMIC_H_5db1263e6c8fb8d5ce454fb186017a5fe390ed96

#include <atomic>

namespace nstd {

#if __cplusplus > 201703L
SHOW_STD_ROOT(1, atomic_ref);
#endif

SHOW_STD_SIMPLE(memory_order);
SHOW_STD_ROOT(1, atomic);
SHOW_STD_SIMPLE(atomic_flag);

}

#endif

#endif

#endif
