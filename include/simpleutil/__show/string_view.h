/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__STRING_VIEW_H_02d945d957e8e040eef29eeb87d57e895fbdd96b
#define __SHOW__STRING_VIEW_H_02d945d957e8e040eef29eeb87d57e895fbdd96b

#if !defined SHOW_NO_STD || defined SHOW_STD_STRING_VIEW

#ifndef SIMPLEUTIL____SHOW__STRING_VIEW_H_58e2c734b746840e0889af1c59437483ecd6438a
#define SIMPLEUTIL____SHOW__STRING_VIEW_H_58e2c734b746840e0889af1c59437483ecd6438a

#include <string_view>

namespace nstd {

SHOW_STD_ROOT(1, basic_string_view);
SHOW_STD_SIMPLE(string_view);
SHOW_STD_SIMPLE(u16string_view);
SHOW_STD_SIMPLE(u32string_view);
SHOW_STD_SIMPLE(wstring_view);

}

#endif

#endif

#endif
