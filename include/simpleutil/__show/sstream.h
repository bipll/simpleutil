/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__SSTREAM_H_80c2553652ab225fa19ea2937bec4c4b941679b1
#define __SHOW__SSTREAM_H_80c2553652ab225fa19ea2937bec4c4b941679b1

#if !defined SHOW_NO_STD || defined SHOW_STD_SSTREAM

#ifndef SIMPLEUTIL____SHOW__SSTREAM_H_5233aa626f96783ad5d05cae557ef3c59077c5dd
#define SIMPLEUTIL____SHOW__SSTREAM_H_5233aa626f96783ad5d05cae557ef3c59077c5dd

#include <sstream>

namespace nstd {

SHOW_STD_ROOT(1, basic_stringbuf);
SHOW_STD_ROOT(1, basic_istringstream);
SHOW_STD_ROOT(1, basic_ostringstream);
SHOW_STD_ROOT(1, basic_stringstream);

}

#endif

#endif

#endif
