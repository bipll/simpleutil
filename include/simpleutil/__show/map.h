/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__MAP_H_2e3a6f261cf253489d9e78e3c3c498ecb19e074c
#define __SHOW__MAP_H_2e3a6f261cf253489d9e78e3c3c498ecb19e074c

#if !defined SHOW_NO_STD || defined SHOW_STD_MAP

#ifndef SIMPLEUTIL____SHOW__MAP_H_c7acde1f6beccfcacb33eafa66d520c1db0fe779
#define SIMPLEUTIL____SHOW__MAP_H_c7acde1f6beccfcacb33eafa66d520c1db0fe779

#include <map>

namespace nstd {

SHOW_STD_ROOT(1, map);
SHOW_STD_ROOT(1, multimap);

}

#endif

#endif

#endif
