/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__OPTIONAL_H_fb3c22ef8c96cf9130afa2cb5303b3ccc257bc62
#define __SHOW__OPTIONAL_H_fb3c22ef8c96cf9130afa2cb5303b3ccc257bc62

#if !defined SHOW_NO_STD || defined SHOW_STD_OPTIONAL

#ifndef SIMPLEUTIL____SHOW__OPTIONAL_H_ab46890c42cc6745e4f183fb8af1d36ba683677d
#define SIMPLEUTIL____SHOW__OPTIONAL_H_ab46890c42cc6745e4f183fb8af1d36ba683677d

#include <optional>

namespace nstd {

SHOW_STD_ROOT(1, optional);
SHOW_STD_SIMPLE(nullopt_t);
SHOW_STD_SIMPLE(bad_optional_access);

}

#endif

#endif

#endif
