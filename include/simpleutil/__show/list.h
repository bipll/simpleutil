/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__LIST_H_5c26858f055a31add92431f0f4022fc2c2280ace
#define __SHOW__LIST_H_5c26858f055a31add92431f0f4022fc2c2280ace

#if !defined SHOW_NO_STD || defined SHOW_STD_LIST

#ifndef SIMPLEUTIL____SHOW__LIST_H_e1f3703e6fc178a8fd3bc57fe05eca9cafc891ff
#define SIMPLEUTIL____SHOW__LIST_H_e1f3703e6fc178a8fd3bc57fe05eca9cafc891ff

#include <list>

namespace nstd {

SHOW_STD_ROOT(1, list);

}

#endif

#endif

#endif
