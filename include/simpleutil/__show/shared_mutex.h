/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__SHARED_MUTEX_H_c42ce2e124dbff8db7423091f48ce2c71aac163d
#define __SHOW__SHARED_MUTEX_H_c42ce2e124dbff8db7423091f48ce2c71aac163d

#if !defined SHOW_NO_STD || defined SHOW_STD_SHARED_MUTEX

#ifndef SIMPLEUTIL____SHOW__SHARED_MUTEX_H_006e651dc6f92d30c2ffbe11d2b2a393206e6f15
#define SIMPLEUTIL____SHOW__SHARED_MUTEX_H_006e651dc6f92d30c2ffbe11d2b2a393206e6f15

#include <shared_mutex>

namespace nstd {

SHOW_STD_SIMPLE(shared_mutex);
SHOW_STD_SIMPLE(shared_timed_mutex);
SHOW_STD_ROOT(1, shared_lock);

}

#endif

#endif

#endif
