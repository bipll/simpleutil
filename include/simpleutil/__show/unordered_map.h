/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__UNORDERED_MAP_H_5809292fa1d529f051e7665f91268b6b5231c7f2
#define __SHOW__UNORDERED_MAP_H_5809292fa1d529f051e7665f91268b6b5231c7f2

#if !defined SHOW_NO_STD || defined SHOW_STD_UNORDERED_MAP

#ifndef SIMPLEUTIL____SHOW__UNORDERED_MAP_H_d34891d903ca759c3714dcdeed4827f827653b2e
#define SIMPLEUTIL____SHOW__UNORDERED_MAP_H_d34891d903ca759c3714dcdeed4827f827653b2e

#include <unordered_map>

namespace nstd {

SHOW_STD_ROOT(1, unordered_map);
SHOW_STD_ROOT(1, unordered_multimap);

}

#endif

#endif

#endif
