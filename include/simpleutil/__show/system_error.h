/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__SYSTEM_ERROR_H_168084ca11871d0958ccfa3f811d7cd9b3be6721
#define __SHOW__SYSTEM_ERROR_H_168084ca11871d0958ccfa3f811d7cd9b3be6721

#if !defined SHOW_NO_STD || defined SHOW_STD_SYSTEM_ERROR

#ifndef SIMPLEUTIL____SHOW__SYSTEM_ERROR_H_8cb7ab4cd45917c692f18d11adbd8dd40e8e72b8
#define SIMPLEUTIL____SHOW__SYSTEM_ERROR_H_8cb7ab4cd45917c692f18d11adbd8dd40e8e72b8

#include <system_error>

namespace nstd {

SHOW_STD_SIMPLE(error_category);
SHOW_STD_SIMPLE(error_code);
SHOW_STD_SIMPLE(errc);
SHOW_STD_SIMPLE(error_condition);
SHOW_STD_SIMPLE(system_error);
SHOW_STD_ROOT(1, is_error_code_enum);
SHOW_STD_ROOT(1, is_error_condition_enum);

}

#endif

#endif

#endif
