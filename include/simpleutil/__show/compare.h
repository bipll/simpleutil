/*:`:`
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__COMPARE_H_8d10f92c144b7a8bb02904fc1d037f0c02d3f589
#define __SHOW__COMPARE_H_8d10f92c144b7a8bb02904fc1d037f0c02d3f589

#if !defined SHOW_NO_STD || defined SHOW_STD_COMPARE

#ifndef SIMPLEUTIL____SHOW__COMPARE_H_44d48d5273d2cdee545007cd5f5df86978b00f8e
#define SIMPLEUTIL____SHOW__COMPARE_H_44d48d5273d2cdee545007cd5f5df86978b00f8e

#if __has_include(<compare>)

#include <compare>

namespace nstd {

SHOW_STD_SIMPLE(weak_equality);
SHOW_STD_SIMPLE(strong_equality);
SHOW_STD_SIMPLE(partial_ordering);
SHOW_STD_SIMPLE(weak_ordering);
SHOW_STD_SIMPLE(strong_ordering);
SHOW_STD_ROOT(1, common_comparison_category);

}

#endif

#endif

#endif

#endif
