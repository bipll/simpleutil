/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__SCOPED_ALLOCATOR_H_fead7ea6f8faf41c2eb7d472a8597470386d436f
#define __SHOW__SCOPED_ALLOCATOR_H_fead7ea6f8faf41c2eb7d472a8597470386d436f

#if !defined SHOW_NO_STD || defined SHOW_STD_SCOPED_ALLOCATOR

#ifndef SIMPLEUTIL____SHOW__SCOPED_ALLOCATOR_H_9d4c302ff452a02088e9ea2101e9b84bae2fdaa8
#define SIMPLEUTIL____SHOW__SCOPED_ALLOCATOR_H_9d4c302ff452a02088e9ea2101e9b84bae2fdaa8

#include <scoped_allocator>

namespace nstd {

SHOW_STD_ROOT(1, scoped_allocator_adaptor);

}

#endif

#endif

#endif
