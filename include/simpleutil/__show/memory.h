/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__MEMORY_H_fe3ad82d896be7c0ba0c3dccedd514332ff04a2f
#define __SHOW__MEMORY_H_fe3ad82d896be7c0ba0c3dccedd514332ff04a2f

#if !defined SHOW_NO_STD || defined SHOW_STD_MEMORY

#ifndef SIMPLEUTIL____SHOW__MEMORY_H_66a9baf7880cdad52858ea7fc2e73b68216723fd
#define SIMPLEUTIL____SHOW__MEMORY_H_66a9baf7880cdad52858ea7fc2e73b68216723fd

#include <memory>

namespace nstd {

SHOW_STD_ROOT(1, pointer_traits);
SHOW_STD_SIMPLE(pointer_safety);
SHOW_STD_SIMPLE(allocator_arg_t);
SHOW_STD_ROOT(1, uses_allocator);
SHOW_STD_ROOT(1, allocator);
SHOW_STD_ROOT(1, allocator_traits);
SHOW_STD_ROOT(1, default_delete);
SHOW_STD_ROOT(1, unique_ptr);
SHOW_STD_SIMPLE(bad_weak_ptr);
SHOW_STD_ROOT(1, shared_ptr);
SHOW_STD_ROOT(1, weak_ptr);
SHOW_STD_ROOT(1, owner_less);
SHOW_STD_ROOT(1, enable_shared_from_this);

}

#endif

#endif

#endif
