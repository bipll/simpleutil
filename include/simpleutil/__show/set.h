/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__SET_H_2c110b30c2ec5d31cec8dc6d7af8349f10b3ffc0
#define __SHOW__SET_H_2c110b30c2ec5d31cec8dc6d7af8349f10b3ffc0

#if !defined SHOW_NO_STD || defined SHOW_STD_SET

#ifndef SIMPLEUTIL____SHOW__SET_H_232718f41b74f5fb6b1c814e4c977781ca391570
#define SIMPLEUTIL____SHOW__SET_H_232718f41b74f5fb6b1c814e4c977781ca391570

#include <set>

namespace nstd {

SHOW_STD_ROOT(1, set);
SHOW_STD_ROOT(1, multiset);

}

#endif

#endif

#endif
