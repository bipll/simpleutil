/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__MUTEX_H_f404a8938ea4401a4979cd26c1adbb027337378d
#define __SHOW__MUTEX_H_f404a8938ea4401a4979cd26c1adbb027337378d

#if !defined SHOW_NO_STD || defined SHOW_STD_MUTEX

#ifndef SIMPLEUTIL____SHOW__MUTEX_H_2daf35499a3b955cf78ddf60cb7cc8452b172c91
#define SIMPLEUTIL____SHOW__MUTEX_H_2daf35499a3b955cf78ddf60cb7cc8452b172c91

#include <mutex>

namespace nstd {

SHOW_STD_SIMPLE(mutex);
SHOW_STD_SIMPLE(recursive_mutex);
SHOW_STD_SIMPLE(timed_mutex);
SHOW_STD_SIMPLE(recursive_timed_mutex);
SHOW_STD_SIMPLE(defer_lock_t);
SHOW_STD_SIMPLE(try_to_lock_t);
SHOW_STD_SIMPLE(adopt_lock_t);
SHOW_STD_ROOT(1, lock_guard);
SHOW_STD_ROOT(1, unique_lock);
SHOW_STD_SIMPLE(once_flag);

}

#endif

#endif

#endif
