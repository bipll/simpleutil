/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__TYPEINDEX_H_8d3e4b7666c11531759a7a33defd28716fc747f5
#define __SHOW__TYPEINDEX_H_8d3e4b7666c11531759a7a33defd28716fc747f5

#if !defined SHOW_NO_STD || defined SHOW_STD_TYPEINDEX

#ifndef SIMPLEUTIL____SHOW__TYPEINDEX_H_159c82ba98e3e805c1459e721a32808caa4b10cd
#define SIMPLEUTIL____SHOW__TYPEINDEX_H_159c82ba98e3e805c1459e721a32808caa4b10cd

#include <typeindex>

namespace nstd {

SHOW_STD_SIMPLE(type_index);

}

#endif

#endif

#endif
