/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__LOCALE_H_0bc20a6d4d2d05255672940b7da52811903182ce
#define __SHOW__LOCALE_H_0bc20a6d4d2d05255672940b7da52811903182ce

#if !defined SHOW_NO_STD || defined SHOW_STD_LOCALE

#ifndef SIMPLEUTIL____SHOW__LOCALE_H_3e13374f23a8901cb5818ce805bc7066d9dc048b
#define SIMPLEUTIL____SHOW__LOCALE_H_3e13374f23a8901cb5818ce805bc7066d9dc048b

#include <locale>

namespace nstd {

SHOW_STD_SIMPLE(locale);
SHOW_STD_ROOT(1, wstring_convert);
SHOW_STD_ROOT(1, wbuffer_convert);
SHOW_STD_SIMPLE(ctype_base);
SHOW_STD_ROOT(1, ctype);
SHOW_STD_ROOT(1, ctype_byname);
SHOW_STD_SIMPLE(codecvt_base);
SHOW_STD_ROOT(1, codecvt);
SHOW_STD_ROOT(1, codecvt_byname);
SHOW_STD_ROOT(1, num_get);
SHOW_STD_ROOT(1, num_put);
SHOW_STD_ROOT(1, numpunct);
SHOW_STD_ROOT(1, numpunct_byname);
SHOW_STD_ROOT(1, collate);
SHOW_STD_ROOT(1, collate_byname);
SHOW_STD_SIMPLE(time_base);
SHOW_STD_ROOT(1, time_get);
SHOW_STD_ROOT(1, time_get_byname);
SHOW_STD_ROOT(1, time_put);
SHOW_STD_ROOT(1, time_put_byname);
SHOW_STD_SIMPLE(money_base);
SHOW_STD_ROOT(1, money_get);
SHOW_STD_ROOT(1, money_put);
SHOW_STD_ROOT(12, moneypunct);
SHOW_STD_ROOT(12, moneypunct_byname);
SHOW_STD_SIMPLE(messages_base);

}

#endif

#endif

#endif
