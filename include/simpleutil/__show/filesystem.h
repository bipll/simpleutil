/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__FILESYSTEM_H_7d78eef94623f0385341160acf83365afddc949d
#define __SHOW__FILESYSTEM_H_7d78eef94623f0385341160acf83365afddc949d

#if !defined SHOW_NO_STD || defined SHOW_STD_FILESYSTEM

#ifndef SIMPLEUTIL____SHOW__FILESYSTEM_H_1f719e2d8180588142087bf019e1ff6dae1b68a0
#define SIMPLEUTIL____SHOW__FILESYSTEM_H_1f719e2d8180588142087bf019e1ff6dae1b68a0

#if __has_include(<filesystem>)

#include <filesystem>

namespace nstd {

SHOW_STD_SIMPLE(filesystem::path);
SHOW_STD_SIMPLE(filesystem::filesystem_error);
SHOW_STD_SIMPLE(filesystem::directory_entry);
SHOW_STD_SIMPLE(filesystem::directory_iterator);
SHOW_STD_SIMPLE(filesystem::recursive_directory_iterator);
SHOW_STD_SIMPLE(filesystem::file_status);
SHOW_STD_SIMPLE(filesystem::space_info);
SHOW_STD_SIMPLE(filesystem::file_type);
SHOW_STD_SIMPLE(filesystem::perms);
SHOW_STD_SIMPLE(filesystem::perm_options);
SHOW_STD_SIMPLE(filesystem::copy_options);
SHOW_STD_SIMPLE(filesystem::directory_options);

}

#endif

#endif

#endif

#endif
