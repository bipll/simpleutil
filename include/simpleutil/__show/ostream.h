/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__OSTREAM_H_a357534a57263923c3ff7e1806a640e256d2f58c
#define __SHOW__OSTREAM_H_a357534a57263923c3ff7e1806a640e256d2f58c

#if !defined SHOW_NO_STD || defined SHOW_STD_OSTREAM

#ifndef SIMPLEUTIL____SHOW__OSTREAM_H_0d726a17bf3216a4444e971c1703c3b4b4312da3
#define SIMPLEUTIL____SHOW__OSTREAM_H_0d726a17bf3216a4444e971c1703c3b4b4312da3

#include <ostream>

namespace nstd {

SHOW_STD_ROOT(1, basic_ostream);

}

#endif

#endif

#endif
