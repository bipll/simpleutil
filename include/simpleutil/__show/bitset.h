/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__BITSET_H_362680690aeadbab5afb30a76fcf7784d5e9b1e8
#define __SHOW__BITSET_H_362680690aeadbab5afb30a76fcf7784d5e9b1e8

#if !defined SHOW_NO_STD || defined SHOW_STD_BITSET

#ifndef SIMPLEUTIL____SHOW__BITSET_H_af3bed320c81c5a47d2d91db64707a5ef9120ee4
#define SIMPLEUTIL____SHOW__BITSET_H_af3bed320c81c5a47d2d91db64707a5ef9120ee4

#include <bitset>

namespace nstd {

SHOW_STD_ROOT(2, bitset);

}

#endif

#endif

#endif
