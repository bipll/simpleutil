/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__RATIO_H_c9ca65a36bb615d27dede3b9335a93e5e1aa9b36
#define __SHOW__RATIO_H_c9ca65a36bb615d27dede3b9335a93e5e1aa9b36

#if !defined SHOW_NO_STD || defined SHOW_STD_RATIO

#ifndef SIMPLEUTIL____SHOW__RATIO_H_c1dd840a95e0d8a161f96d0caf0ac1ab4b1421ff
#define SIMPLEUTIL____SHOW__RATIO_H_c1dd840a95e0d8a161f96d0caf0ac1ab4b1421ff

#include <ratio>

namespace nstd {

SHOW_STD_ROOT(2, ratio);

}

#endif

#endif

#endif
