/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__CHRONO_H_229a766c3deb575fd289b7056336e0152deb4ac4
#define __SHOW__CHRONO_H_229a766c3deb575fd289b7056336e0152deb4ac4

#if !defined SHOW_NO_STD || defined SHOW_STD_CHRONO

#ifndef SIMPLEUTIL____SHOW__CHRONO_H_9ef599551d5417b4ffbc31e3b2459cd9f3008d72
#define SIMPLEUTIL____SHOW__CHRONO_H_9ef599551d5417b4ffbc31e3b2459cd9f3008d72

#include <chrono>

namespace nstd {

SHOW_STD_ROOT(1, chrono::treat_as_floating_point);
SHOW_STD_ROOT(1, chrono::duration_values);
SHOW_STD_ROOT(1, chrono::duration);
SHOW_STD_ROOT(1, chrono::time_point);
SHOW_STD_SIMPLE(chrono::system_clock);
SHOW_STD_SIMPLE(chrono::steady_clock);

}

#endif

#endif

#endif
