/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__VECTOR_H_2dce26e2c313fe6d128f56339553cc304524b57d
#define __SHOW__VECTOR_H_2dce26e2c313fe6d128f56339553cc304524b57d

#if !defined SHOW_NO_STD || defined SHOW_STD_VECTOR

#ifndef SIMPLEUTIL____SHOW__VECTOR_H_0ea3055db1f883a7575d3d13d08709cf0484f4b0
#define SIMPLEUTIL____SHOW__VECTOR_H_0ea3055db1f883a7575d3d13d08709cf0484f4b0

#include <vector>

namespace nstd {

SHOW_STD_ROOT(1, vector);

}

#endif

#endif

#endif
