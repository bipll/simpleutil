/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__STREAMBUF_H_a3650e5d46d90e89647fa177e66d9009a3e76410
#define __SHOW__STREAMBUF_H_a3650e5d46d90e89647fa177e66d9009a3e76410

#if !defined SHOW_NO_STD || defined SHOW_STD_STREAMBUF

#ifndef SIMPLEUTIL____SHOW__STREAMBUF_H_4a0cea0d893cbbe37635096567728137688a87dc
#define SIMPLEUTIL____SHOW__STREAMBUF_H_4a0cea0d893cbbe37635096567728137688a87dc

#include <streambuf>

namespace nstd {

SHOW_STD_ROOT(1, basic_streambuf);

}

#endif

#endif

#endif
