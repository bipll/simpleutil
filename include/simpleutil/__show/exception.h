/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__EXCEPTION_H_c7f2a030cfe4f06d772993ffdd8282361ee55866
#define __SHOW__EXCEPTION_H_c7f2a030cfe4f06d772993ffdd8282361ee55866

#if !defined SHOW_NO_STD || defined SHOW_STD_EXCEPTION

#ifndef SIMPLEUTIL____SHOW__EXCEPTION_H_12cd87da1dd5ffd75ff06076c133082b7915c5de
#define SIMPLEUTIL____SHOW__EXCEPTION_H_12cd87da1dd5ffd75ff06076c133082b7915c5de

#include <exception>

namespace nstd {

SHOW_STD_SIMPLE(exception);
SHOW_STD_SIMPLE(bad_exception);
SHOW_STD_SIMPLE(nested_exception);

}

#endif

#endif

#endif
