/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__COMPLEX_H_1955cafb60ee17578ec550a39807684f5807081b
#define __SHOW__COMPLEX_H_1955cafb60ee17578ec550a39807684f5807081b

#if !defined SHOW_NO_STD || defined SHOW_STD_COMPLEX

#ifndef SIMPLEUTIL____SHOW__COMPLEX_H_b70abae26908b3c38eb90b3cacdda8feb2cfefc4
#define SIMPLEUTIL____SHOW__COMPLEX_H_b70abae26908b3c38eb90b3cacdda8feb2cfefc4

#include <complex>

namespace nstd {

SHOW_STD_ROOT(1, complex);

}

#endif

#endif

#endif
