/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__FORWARD_LIST_H_ddaf22546d668ede18c948769a84f6335d7e5f46
#define __SHOW__FORWARD_LIST_H_ddaf22546d668ede18c948769a84f6335d7e5f46

#if !defined SHOW_NO_STD || defined SHOW_STD_FORWARD_LIST

#ifndef SIMPLEUTIL____SHOW__FORWARD_LIST_H_f18b177395563f483527377cc88c57d38079fb4f
#define SIMPLEUTIL____SHOW__FORWARD_LIST_H_f18b177395563f483527377cc88c57d38079fb4f

#include <forward_list>

namespace nstd {

SHOW_STD_ROOT(1, forward_list);

}

#endif

#endif

#endif
