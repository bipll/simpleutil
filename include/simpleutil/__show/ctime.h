/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__CTIME_H_5f112711d9d19490a867e0a93040f9f5c97fbdb7
#define __SHOW__CTIME_H_5f112711d9d19490a867e0a93040f9f5c97fbdb7

#if !defined SHOW_NO_STD || defined SHOW_STD_CTIME

#ifndef SIMPLEUTIL____SHOW__CTIME_H_09419a4fe5bafaeaf7ddaaf68cf50046cfe4de19
#define SIMPLEUTIL____SHOW__CTIME_H_09419a4fe5bafaeaf7ddaaf68cf50046cfe4de19

#include <ctime>

namespace nstd {

SHOW_STD_SIMPLE(tm);

}

#endif

#endif

#endif
