/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__SPAN_H_f3669afbd603633f31576ada228fe6f17b597d6c
#define __SHOW__SPAN_H_f3669afbd603633f31576ada228fe6f17b597d6c

#if !defined SHOW_NO_STD || defined SHOW_STD_SPAN

#ifndef SIMPLEUTIL____SHOW__SPAN_H_de53aa0845d7c6fa49f4582a89ed822459364f21
#define SIMPLEUTIL____SHOW__SPAN_H_de53aa0845d7c6fa49f4582a89ed822459364f21

#if __has_include(<span>)

#include <span>

namespace nstd {

SHOW_STD_ROOT(1, span);

}

#endif

#endif

#endif

#endif
