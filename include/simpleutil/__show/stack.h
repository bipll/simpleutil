/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__STACK_H_eef3c9e8b857370282ce6ab7111e643dfd14743b
#define __SHOW__STACK_H_eef3c9e8b857370282ce6ab7111e643dfd14743b

#if !defined SHOW_NO_STD || defined SHOW_STD_STACK

#ifndef SIMPLEUTIL____SHOW__STACK_H_dbc78a344d57e85b10e931272badc7468e08065d
#define SIMPLEUTIL____SHOW__STACK_H_dbc78a344d57e85b10e931272badc7468e08065d

#include <stack>

namespace nstd {

SHOW_STD_ROOT(1, stack);

}

#endif

#endif

#endif
