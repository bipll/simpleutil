/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__IOS_H_b814b40e04f4d0247aaadc878fddbaaf36fe2b16
#define __SHOW__IOS_H_b814b40e04f4d0247aaadc878fddbaaf36fe2b16

#if !defined SHOW_NO_STD || defined SHOW_STD_IOS

#ifndef SIMPLEUTIL____SHOW__IOS_H_21acabac831275cf54334a3cea818aa9f11b006f
#define SIMPLEUTIL____SHOW__IOS_H_21acabac831275cf54334a3cea818aa9f11b006f

#include <ios>

namespace nstd {

SHOW_STD_ROOT(1, fpos);
SHOW_STD_SIMPLE(ios_base);
SHOW_STD_ROOT(1, basic_ios);
SHOW_STD_SIMPLE(io_errc);

}

#endif

#endif

#endif
