/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SHOW__NEW_H_4e7540b2b48c8cfa3fe118c2483205d630b4d63c
#define __SHOW__NEW_H_4e7540b2b48c8cfa3fe118c2483205d630b4d63c

#if !defined SHOW_NO_STD || defined SHOW_STD_NEW

#ifndef SIMPLEUTIL____SHOW__NEW_H_cbc441af525906a0c4bb486c21cee8ad33b1d8f1
#define SIMPLEUTIL____SHOW__NEW_H_cbc441af525906a0c4bb486c21cee8ad33b1d8f1

#include <new>

namespace nstd {

SHOW_STD_SIMPLE(bad_alloc);
SHOW_STD_SIMPLE(bad_array_new_length);
SHOW_STD_SIMPLE(align_val_t);
SHOW_STD_SIMPLE(nothrow_t);

}

#endif

#endif

#endif
