/*
 * Copyright ©2018 Denis Trapeznikov
 *
 * This file is part of simpleutil.
 *
 * simpleutil is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * simpleutil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with simpleutil. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEUTIL__MAYBE_H_bf2d8483daa15a9559317dd4e2863c27d292a5a9
#define SIMPLEUTIL__MAYBE_H_bf2d8483daa15a9559317dd4e2863c27d292a5a9

#include <optional>
#include <functional>
#include <type_traits>
#include <cstddef>
#include <utility>
#include <ostream>
#include "typeutil.h"
#include "operator.h"

namespace nstd {

template<class T> class optional;

class optional_impl: private_namespace {
	template<class T> friend class ::nstd::optional;

	template<class A, class F> using tluser_ekovni = ::std::invoke_result<F, A>;
	template<class T, class... Fs> using result_t = pack::reduce_t<tluser_ekovni, T, Fs...>;

	template<class T> struct refopt {
		constexpr refopt() noexcept {}
		constexpr refopt(::std::nullopt_t n) noexcept {}
		constexpr refopt(refopt const &that) noexcept: ref{that.ref} {}
		constexpr refopt(refopt &&that) noexcept: ref{that.ref} {}
		template<class U> refopt(refopt<U> const &that) noexcept: ref(that.has_value()? *that : nullptr) {}
		template<class U> refopt(refopt<U> &&that) noexcept: ref(that.has_value()? *that : nullptr) {}
		template<class U> constexpr explicit refopt(::std::in_place_t, U &&u) noexcept: ref(&u) {}
		template<class U = T> refopt(U &that) noexcept: ref(&that) {}

		refopt &operator=(::std::nullopt_t n) noexcept { return ref = nullptr, *this; }
		refopt &operator=(refopt const &that) noexcept { return ref = that.ref, *this; }
		refopt &operator=(refopt &&that) noexcept { return ref = that.ref, *this; }
		template<class U = T> refopt &operator=(U &&value) {
			if constexpr(::std::is_const_v<T>) return ref = &value, *this;
			else return ref? (void)(*ref = ::std::forward<U>(value)) : (void)(ref = &value), *this;
		}
		template<class U> refopt &operator=(refopt<U> const &that) { return ref = that.has_value()? &*that : nullptr, *this; }
		template<class U> refopt &operator=(refopt<U> &&that) { return ref = that.has_value()? &*that : nullptr, *this; }

		constexpr T const *operator->() const { return ref; }
		constexpr T *operator->() { return ref; }
		constexpr T const &operator*() const & { return *ref; }
		constexpr T &operator*() & { return *ref; }

		constexpr explicit operator bool() const noexcept { return ref; }
		constexpr bool has_value() const noexcept { return ref; }

		constexpr T &value() & { if(ref) return *ref; throw ::std::bad_optional_access(); }
		constexpr T const & value() const & { if(ref) return *ref; throw ::std::bad_optional_access(); }
		constexpr T&& value() && { if(ref) return *ref; throw ::std::bad_optional_access(); }
		constexpr T const &&value() const && { if(ref) return *ref; throw ::std::bad_optional_access(); }

		template<class U> constexpr T const &value_or(U&& default_value) const & noexcept { return ref? *ref : default_value; }
		template<class U> constexpr T &value_or(U&& default_value) && noexcept { return ref? *ref : default_value; }

		void swap(refopt &that) { ::std::swap(ref, that.ref); }

		void reset() noexcept { ref = nullptr; }

		template<class U, class... Args> T &emplace(U &&u, Args &&...) { return *(ref = &u); }
	private:
		T *ref = nullptr;
	};
};

template<class U> struct opt_itr {
	using difference_type = ::std::ptrdiff_t;
	using value_type = ::std::decay_t<U>;
	using pointer = value_type *;
	using reference = value_type &;
	using iterator_category = ::std::random_access_iterator_tag;

	constexpr opt_itr() noexcept = default;
	friend constexpr void swap(opt_itr &u, opt_itr &t) noexcept { ::std::swap(u.ptr, t.ptr); ::std::swap(u.begin, t.begin); }
	constexpr reference operator*() const noexcept { return *ptr; }
	constexpr opt_itr &operator++() noexcept { return ptr = nullptr, *this; }

	constexpr pointer operator->() const noexcept { return ptr; }
	constexpr opt_itr operator++(int) noexcept { return opt_itr(begin, ::std::exchange(ptr, nullptr)); }

	constexpr opt_itr &operator--() noexcept { return ptr = begin, *this; }
	constexpr opt_itr &operator--(int) noexcept { return opt_itr(begin, ::std::exchange(ptr, begin)); }

	constexpr opt_itr &operator+=(difference_type n) noexcept {
		if(n) ptr = n > 0? nullptr : begin;
		return *this;
	}
	constexpr opt_itr operator+(difference_type n) const noexcept {
		if(n) return opt_itr(begin, n > 0? nullptr : begin);
		return *this;
	}
	friend constexpr opt_itr operator+(difference_type n, opt_itr i) noexcept {
		if(n) return opt_itr(i.begin, n > 0? nullptr : i.begin);
		return i;
	}
	constexpr opt_itr &operator-=(difference_type n) noexcept {
		if(n) ptr = n < 0? nullptr : begin;
		return *this;
	}
	constexpr opt_itr operator-(difference_type n) const noexcept {
		if(n) return opt_itr(begin, n < 0? nullptr : begin);
		return *this;
	}
	constexpr difference_type operator-(opt_itr const &that) const noexcept { return ptr - that.ptr; }
	constexpr reference operator[](difference_type) const noexcept { return *ptr; }
	constexpr bool operator<(opt_itr const &that) const noexcept { return ptr < that.ptr; }
	constexpr bool operator>(opt_itr const &that) const noexcept { return ptr > that.ptr; }
	constexpr bool operator>=(opt_itr const &that) const noexcept { return ptr >= that.ptr; }
	constexpr bool operator<=(opt_itr const &that) const noexcept { return ptr <= that.ptr; }

	constexpr bool operator==(opt_itr const &that) const noexcept { return ptr == that.ptr; }
	constexpr bool operator!=(opt_itr const &that) const noexcept { return ptr != that.ptr; }
protected:
	template<class T> friend class optional;
	constexpr opt_itr(U *begin) noexcept: begin(begin), ptr(begin) {}
	constexpr opt_itr(U *begin, U *ptr) noexcept: begin(begin), ptr(ptr) {}

	U *begin, *ptr;
};

template<class T> class optional: public ::std::conditional_t<::std::is_reference_v<T>, optional_impl::refopt<::std::remove_reference_t<T>>, ::std::optional<T>> {
	using Parent = ::std::conditional_t<::std::is_reference_v<T>, optional_impl::refopt<::std::remove_reference_t<T>>, ::std::optional<T>>;
public:
	using This = optional;
	using value_type = T;
	using size_type = ::std::size_t;

	template<class... Args> static constexpr This just(Args &&...args) noexcept(noexcept(This(::std::in_place, ::std::forward<Args>(args)...))) {
		return This(::std::in_place, ::std::forward<Args>(args)...);
	}
	static constexpr This nothing() noexcept { return {}; }

	using ::std::conditional_t<::std::is_reference_v<T>, optional_impl::refopt<::std::remove_reference_t<T>>, ::std::optional<T>>::conditional_t;
	constexpr optional(T &&t) noexcept(::std::is_nothrow_constructible_v<Parent, T>): Parent(::std::forward<T>(t)) {}
	constexpr optional(optional &&) noexcept(::std::is_nothrow_move_constructible_v<Parent>) = default;
	constexpr optional(optional const &) noexcept(::std::is_nothrow_copy_constructible_v<Parent>) = default;
	template<class StartIterator, class StopIterator> constexpr optional(StartIterator start, StopIterator stop) noexcept(noexcept(Parent(*start)))
		: Parent(start == stop? Parent{} : Parent{*start}) {}
	~optional() = default;

	optional &operator=(optional &&) noexcept(::std::is_nothrow_move_assignable_v<Parent>) = default;
	optional &operator=(optional const &) noexcept(::std::is_nothrow_copy_assignable_v<Parent>) = default;
	template<class That> optional &operator=(That &&that) noexcept(::std::is_nothrow_assignable_v<Parent, That>) {
		return Parent::operator=(::std::forward<That>(that)), *this;
	}

	using Parent::operator->, Parent::operator*, Parent::has_value, Parent::value, Parent::value_or, Parent::swap, Parent::reset, Parent::emplace;

	constexpr operator bool() const noexcept {
		if constexpr(::std::is_same_v<remove_cvr_t<T>, bool>) return has_value() && **this;
		else return Parent::operator bool();
	}

	constexpr bool empty() const noexcept { return !has_value(); }

	constexpr operator value_type &() { return value(); }
	constexpr operator value_type const &() const { return value(); }

	constexpr size_type size() const { return has_value(); }
private:
	template<class... Fs> using result_t = optional_impl::result_t<T, Fs...>;
	template<class... Fs> using const_result_t = optional_impl::result_t<const T, Fs...>;
	template<class... As> using optional_result_t = optional<::std::invoke_result_t<T, As...>>;
	template<class... As> using const_optional_result_t = optional<::std::invoke_result_t<const T, As...>>;

	template<class U> struct unmaybe {
		using type = U;
		static constexpr U &&value(U &&t) noexcept { return ::std::forward<U>(t); }
	};
	template<class U> using unmaybe_t = typename unmaybe<U>::type;
	template<class U> struct unmaybe<optional<U>> {
		using type = U;
		static constexpr U const &value(optional<U> const &t) noexcept { return *t; }
	};

	template<class U> using remove_cvr_t = ::std::remove_cv_t<::std::remove_reference_t<U>>;

	template<class U> struct is_optional { static constexpr bool value = false; };
	template<class U> static constexpr bool is_optional_v = is_optional<::std::decay_t<U>>::value;
	template<class U> struct is_optional<optional<U>> { static constexpr bool value = true; };

	template<class A1, class... Args> static constexpr bool ready(A1 &&a1, Args &&...args) noexcept {
		if constexpr(is_optional_v<A1>) return a1.has_value() && ready(args...);
		return ready(args...);
	}
	static constexpr bool ready() noexcept { return true; }

	template<class A> static constexpr decltype(auto) values(A &&a) noexcept { return unmaybe<A>::value(::std::forward<A>(a)); }
	// localize the declval
	static constexpr type_t<::std::add_rvalue_reference<value_type>> declval() noexcept;
	static constexpr type_t<::std::add_rvalue_reference<::std::add_const_t<value_type>>> const_declval() noexcept;

	template<class Op, class Right, bool enabled = !compiles<Op, optional, unmaybe_t<Right>>() && compiles<Op, value_type, unmaybe_t<Right>>()> struct right_app {
		using type = optional<decltype(Op::call(declval(), values(::std::declval<Right>())))>;
		static constexpr bool is_noexc = noexcept(Op::call(declval(), values(::std::declval<Right>())));
	};
	template<class Op, class Right> using right_type = typename right_app<Op, Right>::type;
	template<class Op, class Right> static constexpr bool is_right_noexc = right_app<Op, Right>::is_noexc;

	template<class Op, class Right, bool enabled = 
		!compiles<Op, ::std::add_const_t<optional>, unmaybe_t<Right>>() && compiles<Op, ::std::add_const_t<value_type>, unmaybe_t<Right>>()>
	struct right_const_app {
		using type = optional<decltype(Op::call(const_declval(), values(::std::declval<Right>())))>;
		static constexpr bool is_noexc = noexcept(Op::call(const_declval(), values(::std::declval<Right>())));
	};
	template<class Op, class Right> using right_const_type = typename right_const_app<Op, Right>::type;
	template<class Op, class Right> static constexpr bool is_right_const_noexc = right_const_app<Op, Right>::is_noexc;
public:
	using iterator = opt_itr<::std::decay_t<T>>;
	using const_iterator = opt_itr<::std::add_const_t<::std::decay_t<T>>>;

	static constexpr bool itr_eq(iterator const &left, iterator const &right) noexcept { return left.ptr == right.ptr; }
	static constexpr bool itr_ne(iterator const &left, iterator const &right) noexcept { return left.ptr != right.ptr; }
	static constexpr bool itr_eq(const_iterator const &left, const_iterator const &right) noexcept { return left.ptr == right.ptr; }
	static constexpr bool itr_ne(const_iterator const &left, const_iterator const &right) noexcept { return left.ptr != right.ptr; }
	/*
	struct const_iterator;
	struct iterator: itr<::std::decay_t<T>> {
		using This = iterator;
		using Parent = itr<::std::decay_t<T>>;

		constexpr iterator() noexcept = default;

		constexpr bool operator==(iterator const &that) const noexcept { return Parent::operator==(that); }
		constexpr bool operator!=(iterator const &that) const noexcept { return Parent::operator!=(that); }
		constexpr bool operator==(const_iterator const &that) const noexcept { return that == *this; }
		constexpr bool operator!=(const_iterator const &that) const noexcept { return that != *this; }
	private:
		using itr<::std::decay_t<T>>::begin;
		using itr<::std::decay_t<T>>::ptr;
		friend class optional;
		friend class const_iterator;
		constexpr iterator(::std::decay_t<T> *begin) noexcept: itr<::std::decay_t<T>>(begin) {}
		constexpr iterator(::std::decay_t<T> *begin, ::std::decay_t<T> *ptr) noexcept: itr<::std::decay_t<T>>(begin, ptr) {}
	};
	struct const_iterator: itr<::std::add_const_t<::std::decay_t<T>>> {
		using This = iterator;
		using Parent = itr<::std::add_const_t<::std::decay_t<T>>>;

		constexpr const_iterator() noexcept = default;
		constexpr const_iterator(iterator const &i) noexcept: Parent(i.begin, i.ptr) {}

		constexpr bool operator==(const_iterator const &that) const noexcept { return Parent::operator==(that); }
		constexpr bool operator!=(const_iterator const &that) const noexcept { return Parent::operator!=(that); }
		constexpr bool operator==(iterator const &that) const noexcept { return Parent::operator==(that.ptr); }
		constexpr bool operator!=(iterator const &that) const noexcept { return Parent::operator!=(that.ptr); }
	private:
		friend class optional;
		constexpr const_iterator(::std::decay_t<T> const *begin) noexcept: itr<const ::std::decay_t<T>>(begin) {}
		constexpr const_iterator(::std::decay_t<T> const *begin, ::std::decay_t<T> const *ptr) noexcept: itr<const ::std::decay_t<T>>(begin, ptr) {}
	};
	*/

	constexpr iterator begin() noexcept { return iterator(has_value()? &**this : nullptr); }
	constexpr iterator end() noexcept { return iterator(has_value()? &**this : nullptr, nullptr); }
	constexpr const_iterator begin() const noexcept { return const_iterator(has_value()? &**this : nullptr); }
	constexpr const_iterator end() const noexcept { return const_iterator(has_value()? &**this : nullptr, nullptr); }
	constexpr const_iterator cbegin() const noexcept { return const_iterator(has_value()? &**this : nullptr); }
	constexpr const_iterator cend() const noexcept { return const_iterator(has_value()? &**this : nullptr, nullptr); }

#define MAYBE_FMAP_1(...) { if(has_value()) return __VA_ARGS__; return {}; }
#define MAYBE_FMAP_2(u, ...) { if(ready(*this, u)) return __VA_ARGS__; return {}; }

	constexpr value_type flatten() const noexcept { MAYBE_FMAP_1(**this); }

	template<class F, class... MArgs> constexpr optional<::std::invoke_result_t<F, value_type, unmaybe_t<MArgs>...>> fmap(F &&f, MArgs &&...mArgs) const
		noexcept(noexcept(std::forward<F>(f)(**this, values(::std::forward<MArgs>(mArgs))...)))
	{
		MAYBE_FMAP_2(mArgs..., std::forward<F>(f)(**this, values(::std::forward<MArgs>(mArgs))...));
	}
	constexpr optional &flatmap() noexcept { return *this; }
	constexpr optional const &flatmap() const noexcept { return *this; }
	template<class F> constexpr result_t<F> flatmap(F &&f) noexcept(noexcept(::std::invoke(::std::forward<F>(f), **this))) {
		MAYBE_FMAP_1(::std::invoke(::std::forward<F>(f), **this));
	}
	template<class F> constexpr const_result_t<F> flatmap(F &&f) const noexcept(noexcept(::std::invoke(::std::forward<F>(f), **this))) {
		MAYBE_FMAP_1(::std::invoke(::std::forward<F>(f), **this));
	}
	template<class F, class... Fs> constexpr result_t<F, Fs...> flatmap(F &&f, Fs &&...fs)
		noexcept(noexcept(::std::invoke(::std::forward<F>(f), **this).flatmap(std::forward<Fs>(fs)...)))
	{
		MAYBE_FMAP_1(::std::invoke(::std::forward<F>(f), **this).flatmap(std::forward<Fs>(fs)...));
	}
	template<class F, class... Fs> constexpr const_result_t<F, Fs...> flatmap(F &&f, Fs &&...fs) const
		noexcept(noexcept(::std::invoke(::std::forward<F>(f), **this).flatmap(std::forward<Fs>(fs)...)))
	{
		MAYBE_FMAP_1(::std::invoke(::std::forward<F>(f), **this).flatmap(std::forward<Fs>(fs)...));
	}

	template<class... Args> constexpr optional_result_t<Args...> spread(Args &&...args) noexcept(NOEXCEPT(::std::invoke(**this, ::std::forward<Args>(args)...))) {
		MAYBE_FMAP_1(::std::invoke(**this, ::std::forward<Args>(args)...));
	}
	template<class... Args> constexpr const_optional_result_t<Args...> spread(Args &&...args) const
		noexcept(NOEXCEPT(::std::invoke(**this, ::std::forward<Args>(args)...)))
	{
		MAYBE_FMAP_1(::std::invoke(**this, ::std::forward<Args>(args)...));
	}

	template<class... Args> constexpr optional_result_t<Args...> operator()(Args &&...args) noexcept(noexcept(::std::invoke(**this, values(args)...))) {
		MAYBE_FMAP_2(args..., ::std::invoke(**this, values(args)...));
	}
	template<class... Args> constexpr const_optional_result_t<Args...> operator()(Args &&...args) const noexcept(noexcept(::std::invoke(**this, values(args)...))) {
		MAYBE_FMAP_2(args..., ::std::invoke(**this, values(args)...));
	}

	template<class Index> constexpr auto operator[](Index &&i) noexcept(is_right_noexc<subscript, Index>) -> optional<decltype((**this)[values(i)])> {
		MAYBE_FMAP_2(i, (**this)[values(i)]);
	}
	template<class Index> constexpr auto operator[](Index &&i) const noexcept(is_right_noexc<subscript, Index>) -> optional<decltype((**this)[values(i)])> {
		MAYBE_FMAP_2(i, (**this)[values(i)]);
	}
#undef MAYBE_FMAP_1

	template<class U> constexpr right_const_type<mul, U> operator*(U &&u) const noexcept(is_right_const_noexc<mul, U>) { MAYBE_FMAP_2(u, **this * values(u)); }
	template<class U> constexpr right_const_type<div, U> operator/(U &&u) const noexcept(is_right_const_noexc<div, U>) { MAYBE_FMAP_2(u, **this / values(u)); }
	template<class U> constexpr right_const_type<mod, U> operator%(U &&u) const noexcept(is_right_const_noexc<mod, U>) { MAYBE_FMAP_2(u, **this % values(u)); }

	template<class U> constexpr right_const_type<add, U> operator+(U &&u) const noexcept(is_right_const_noexc<add, U>) { MAYBE_FMAP_2(u, **this + values(u)); }
	template<class U> constexpr right_const_type<sub, U> operator-(U &&u) const noexcept(is_right_const_noexc<sub, U>) { MAYBE_FMAP_2(u, **this - values(u)); }

	template<class U> constexpr right_const_type<shl, U> operator<<(U &&u) const noexcept(is_right_const_noexc<shl, U>) { MAYBE_FMAP_2(u, **this << values(u)); }
	template<class U> constexpr right_const_type<shr, U> operator>>(U &&u) const noexcept(is_right_const_noexc<shr, U>) { MAYBE_FMAP_2(u, **this >> values(u)); }

#if __cplusplus > 201703L
	template<class U> constexpr right_const_type<ufo, U> operator<=>(U &&u) const noexcept(is_right_const_noexc<ufo, U>) { MAYBE_FMAP_2(u, **this <=> values(u)); }
#endif

	template<class U> constexpr right_const_type<lt, U> operator<(U &&u) const noexcept(is_right_const_noexc<lt, U>) { MAYBE_FMAP_2(u, **this < values(u)); }
	template<class U> constexpr right_const_type<gt, U> operator>(U &&u) const noexcept(is_right_const_noexc<gt, U>) { MAYBE_FMAP_2(u, **this > values(u)); }
	template<class U> constexpr right_const_type<le, U> operator<=(U &&u) const noexcept(is_right_const_noexc<le, U>) { MAYBE_FMAP_2(u, **this <= values(u)); }
	template<class U> constexpr right_const_type<ge, U> operator>=(U &&u) const noexcept(is_right_const_noexc<ge, U>) { MAYBE_FMAP_2(u, **this >= values(u)); }

	template<class U> constexpr right_const_type<eq, U> operator==(U &&u) const noexcept(is_right_const_noexc<eq, U>) { MAYBE_FMAP_2(u, **this == values(u)); }
	template<class U> constexpr right_const_type<ne, U> operator!=(U &&u) const noexcept(is_right_const_noexc<ne, U>) { MAYBE_FMAP_2(u, **this != values(u)); }

	template<class U> constexpr right_const_type<bit_and, U> operator&(U &&u) const noexcept(is_right_const_noexc<bit_and, U>) {
		MAYBE_FMAP_2(u, **this & values(u));
	}
	template<class U> constexpr right_const_type<bit_xor, U> operator^(U &&u) const noexcept(is_right_const_noexc<bit_xor, U>) {
		MAYBE_FMAP_2(u, **this ^ values(u));
	}
	template<class U> constexpr right_const_type<bit_or, U> operator|(U &&u) const noexcept(is_right_const_noexc<bit_or, U>) {
		MAYBE_FMAP_2(u, **this | values(u));
	}

	template<class U> constexpr right_const_type<bool_and, U> operator&&(U &&u) const noexcept(is_right_const_noexc<bool_and, U>) {
		MAYBE_FMAP_2(u, **this && values(u));
	}
	template<class U> constexpr right_const_type<bool_or, U> operator||(U &&u) const noexcept(is_right_const_noexc<bool_or, U>) {
		MAYBE_FMAP_2(u, **this || values(u));
	}

	template<class U> constexpr right_type<assign_or, U> operator|=(U &&u) noexcept(is_right_noexc<assign_or, U>) { MAYBE_FMAP_2(u, **this |= values(u)); }
	template<class U> constexpr right_type<assign_add, U> operator+=(U &&u) noexcept(is_right_noexc<assign_add, U>) { MAYBE_FMAP_2(u, **this += values(u)); }
	template<class U> constexpr right_type<assign_sub, U> operator-=(U &&u) noexcept(is_right_noexc<assign_sub, U>) { MAYBE_FMAP_2(u, **this -= values(u)); }
	template<class U> constexpr right_type<assign_mul, U> operator*=(U &&u) noexcept(is_right_noexc<assign_mul, U>) { MAYBE_FMAP_2(u, **this *= values(u)); }
	template<class U> constexpr right_type<assign_div, U> operator/=(U &&u) noexcept(is_right_noexc<assign_div, U>) { MAYBE_FMAP_2(u, **this /= values(u)); }
	template<class U> constexpr right_type<assign_mod, U> operator%=(U &&u) noexcept(is_right_noexc<assign_mod, U>) { MAYBE_FMAP_2(u, **this %= values(u)); }
	template<class U> constexpr right_type<assign_and, U> operator&=(U &&u) noexcept(is_right_noexc<assign_and, U>) { MAYBE_FMAP_2(u, **this &= values(u)); }
	template<class U> constexpr right_type<assign_xor, U> operator^=(U &&u) noexcept(is_right_noexc<assign_xor, U>) { MAYBE_FMAP_2(u, **this ^= values(u)); }
	template<class U> constexpr right_type<assign_shl, U> operator<<=(U &&u) noexcept(is_right_noexc<assign_shl, U>) { MAYBE_FMAP_2(u, **this <<= values(u)); }
	template<class U> constexpr right_type<assign_shr, U> operator>>=(U &&u) noexcept(is_right_noexc<assign_shr, U>) { MAYBE_FMAP_2(u, **this >>= values(u)); }
#undef MAYBE_FMAP_2

#define MAYBE_FMAP_2(u, ...) { if(ready(u)) return __VA_ARGS__; return {}; }
private:
	template<class Op, class U, bool enabled = !is_optional_v<U> && !compiles<Op, U, optional>()> struct friend_app {
		using type = optional<decltype(Op::call(std::declval<U>(), std::declval<value_type>()))>;
		static constexpr bool is_noexc = noexcept(::nstd::optional(Op::call(std::declval<U>(), std::declval<value_type>())));
	};
	template<class Op, class U> using friend_t = typename friend_app<Op, U>::type;
	template<class Op, class U> static constexpr bool is_noexcept_friend = friend_app<Op, U>::is_noexc;
public:
	template<class U> friend constexpr friend_t<mul, U> operator*(U &&u, optional const &t) noexcept(is_noexcept_friend<mul, U>) { MAYBE_FMAP_2(t, u * *t); }
	template<class U> friend constexpr friend_t<div, U> operator/(U &&u, optional const &t) noexcept(is_noexcept_friend<div, U>) { MAYBE_FMAP_2(t, u / *t); }
	template<class U> friend constexpr friend_t<mod, U> operator%(U &&u, optional const &t) noexcept(is_noexcept_friend<mod, U>) { MAYBE_FMAP_2(t, u % *t); }

	template<class U> friend constexpr friend_t<add, U> operator+(U &&u, optional const &t) noexcept(is_noexcept_friend<add, U>) { MAYBE_FMAP_2(t, u + *t); }
	template<class U> friend constexpr friend_t<sub, U> operator-(U &&u, optional const &t) noexcept(is_noexcept_friend<sub, U>) { MAYBE_FMAP_2(t, u - *t); }
private:
	template<class Op, class U, class stream, bool enabled = !is_optional_v<U> && !::std::is_base_of_v<stream, remove_cvr_t<U>> && !compiles<Op, U, optional>()>
	struct stream_friend { using type = optional<decltype(Op::call(std::declval<U>(), std::declval<value_type>()))>; };
	template<class Op, class U, class stream> using stream_friend_t = stream_friend<Op, U, stream>;
public:
	template<class U> friend constexpr stream_friend_t<shl, U, ::std::ostream> operator<<(U &&u, optional const &t) noexcept(is_noexcept_friend<shl, U>) {
		MAYBE_FMAP_2(t, u << *t);
	}
	template<class U> friend constexpr stream_friend_t<shr, U, ::std::istream> operator>>(U &&u, optional const &t) noexcept(is_noexcept_friend<shr, U>) {
		MAYBE_FMAP_2(t, u >> *t);
	}

#if __cplusplus > 201703L
	template<class U> friend constexpr friend_t<ufo, U> operator<=>(U &&u, optional const &t) noexcept(is_noexcept_friend<ufo, U>) { MAYBE_FMAP_2(t, u <=> *t); }
#endif
	template<class U> friend constexpr friend_t<lt, U> operator<(U &&u, optional const &t) noexcept(is_noexcept_friend<lt, U>) { MAYBE_FMAP_2(t, u < *t); }
	template<class U> friend constexpr friend_t<gt, U> operator>(U &&u, optional const &t) noexcept(is_noexcept_friend<gt, U>) { MAYBE_FMAP_2(t, u > *t); }

	template<class U> friend constexpr friend_t<le, U> operator<=(U &&u, optional const &t) noexcept(is_noexcept_friend<le, U>) { MAYBE_FMAP_2(t, u <= *t); }
	template<class U> friend constexpr friend_t<ge, U> operator>=(U &&u, optional const &t) noexcept(is_noexcept_friend<ge, U>) { MAYBE_FMAP_2(t, u >= *t); }

	template<class U> friend constexpr friend_t<eq, U> operator==(U &&u, optional const &t) noexcept(is_noexcept_friend<eq, U>) { MAYBE_FMAP_2(t, u == *t); }
	template<class U> friend constexpr friend_t<ne, U> operator!=(U &&u, optional const &t) noexcept(is_noexcept_friend<ne, U>) { MAYBE_FMAP_2(t, u != *t); }

	template<class U> friend constexpr friend_t<bit_and, U> operator&(U &&u, optional const &t) noexcept(is_noexcept_friend<bit_and, U>) { MAYBE_FMAP_2(t, u & *t); }
	template<class U> friend constexpr friend_t<bit_xor, U> operator^(U &&u, optional const &t) noexcept(is_noexcept_friend<bit_xor, U>) { MAYBE_FMAP_2(t, u ^ *t); }
	template<class U> friend constexpr friend_t<bit_or, U> operator|(U &&u, optional const &t) noexcept(is_noexcept_friend<bit_or, U>) { MAYBE_FMAP_2(t, u | *t); }

	template<class U> friend constexpr friend_t<bool_and, U> operator&&(U &&u, optional const &t) noexcept(is_noexcept_friend<bool_and, U>) { MAYBE_FMAP_2(t, u && *t); }
	template<class U> friend constexpr friend_t<bool_or, U> operator||(U &&u, optional const &t) noexcept(is_noexcept_friend<bool_or, U>) { MAYBE_FMAP_2(t, u || *t); }

	template<class U> friend constexpr friend_t<assign_add, U> operator+=(U &&u, optional const &t) noexcept(is_noexcept_friend<assign_add, U>) {
		MAYBE_FMAP_2(t, u += *t);
	}
	template<class U> friend constexpr friend_t<assign_sub, U> operator-=(U &&u, optional const &t) noexcept(is_noexcept_friend<assign_sub, U>) {
		MAYBE_FMAP_2(t, u -= *t);
	}
	template<class U> friend constexpr friend_t<assign_mul, U> operator*=(U &&u, optional const &t) noexcept(is_noexcept_friend<assign_mul, U>) {
		MAYBE_FMAP_2(t, u *= *t);
	}
	template<class U> friend constexpr friend_t<assign_div, U> operator/=(U &&u, optional const &t) noexcept(is_noexcept_friend<assign_div, U>) {
		MAYBE_FMAP_2(t, u /= *t);
	}
	template<class U> friend constexpr friend_t<assign_mod, U> operator%=(U &&u, optional const &t) noexcept(is_noexcept_friend<assign_mod, U>) {
		MAYBE_FMAP_2(t, u %= *t);
	}
	template<class U> friend constexpr friend_t<assign_shl, U> operator<<=(U &&u, optional const &t) noexcept(is_noexcept_friend<assign_shl, U>) {
		MAYBE_FMAP_2(t, u <<= *t);
	}
	template<class U> friend constexpr friend_t<assign_shr, U> operator>>=(U &&u, optional const &t) noexcept(is_noexcept_friend<assign_shr, U>) {
		MAYBE_FMAP_2(t, u >>= *t);
	}
	template<class U> friend constexpr friend_t<assign_and, U> operator&=(U &&u, optional const &t) noexcept(is_noexcept_friend<assign_and, U>) {
		MAYBE_FMAP_2(t, u &= *t);
	}
	template<class U> friend constexpr friend_t<assign_xor, U> operator^=(U &&u, optional const &t) noexcept(is_noexcept_friend<assign_xor, U>) {
		MAYBE_FMAP_2(t, u ^= *t);
	}
	template<class U> friend constexpr friend_t<assign_or, U> operator|=(U &&u, optional const &t) noexcept(is_noexcept_friend<assign_or, U>) {
		MAYBE_FMAP_2(t, u |= *t);
	}
};
template<class T> optional(T &&) -> optional<T>;

template<class T> template<class Op, class Right> struct optional<T>::right_app<Op, Right, false> {};
template<class T> template<class Op, class Right> struct optional<T>::right_const_app<Op, Right, false> {};
template<class T> template<class Op, class U> struct optional<T>::friend_app<Op, U, false> {};
template<class T> template<class Op, class U, class stream> struct optional<T>::stream_friend<Op, U, stream, false> {};

#undef MAYBE_FMAP_2
#define MAYBE_FMAP_1(...) { if(m.has_value()) return __VA_ARGS__; return {}; }

template<class T> inline constexpr optional<T> operator++(optional<T> &m, int) noexcept(noexcept((*m)++)) { MAYBE_FMAP_1((*m)++); }
template<class T> inline constexpr optional<T> operator--(optional<T> &m, int) noexcept(noexcept((*m)--)) { MAYBE_FMAP_1((*m)--); }

template<class T> inline constexpr optional<T> &operator++(optional<T> &m) noexcept(noexcept(++*m)) { if(m.has_value()) ++*m; return m; }
template<class T> inline constexpr optional<T> &operator--(optional<T> &m) noexcept(noexcept(--*m)) { if(m.has_value()) --*m; return m; }

template<class T> inline constexpr optional<decltype(!::std::declval<T>())> operator!(optional<T> const &m) noexcept(noexcept(!*m)) { MAYBE_FMAP_1(!*m); }
template<class T> inline constexpr optional<decltype(~::std::declval<T>())> operator~(optional<T> const &m) noexcept(noexcept(~*m)) { MAYBE_FMAP_1(~*m); }

#undef MAYBE_FMAP_1

template<class T> inline ::std::ostream &operator<<(::std::ostream &s, optional<T> const &m) {
	return m.has_value()? s << "Just{" << *m << '}' : s << "Nothing";
}

template<class T> inline ::std::istream & operator>>(::std::istream &s, optional<T> &m) {
	if(m.has_value()) {
		try { if(!(s >> *m)) m.reset(); }
		catch(...) { m.reset(); }
		return s;
	}
	if constexpr(::std::is_default_constructible_v<T>) {
		try {
			T t;
			if(s >> t) m = ::std::move(t);
		}
		catch(...) {}
		return s;
	}
	else throw ::std::bad_optional_access();
}

#if __cplusplus > 201703L
template<class Operand1, class Operand2> using lt_eq_gt = INFIX_TYPE(Operand1, <=>, Operand2);
class ufo: public binary_operator<lt_eq_gt> {
	template<class... Ts> struct impl;
public:
	template<class... Ts> static constexpr decltype(auto) call(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> constexpr decltype(auto) operator()(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
	template<class... Ts> static constexpr decltype(auto) spread(Ts &&...ts) noexcept(noexcept(impl<Ts...>::call(std::forward<Ts>(ts)...))) {
		return impl<Ts...>::call(std::forward<Ts>(ts)...);
	}
};
template<class T1, class T2, class... Ts> struct ufo::impl<T1, T2, Ts...> {
	static constexpr decltype(auto) call(T1 &&t1, T2 &&t2, Ts &&...ts)
		noexcept(noexcept(ufo::impl<T2, Ts...>::grow(std::forward<T2>(t2), std::forward<TS>(ts)...))
			 && noexcept(ufo::impl<T2, Ts...>::shrink(std::forward<T2>(t2), std::forward<TS>(ts)...))
			 && noexcept(ufo::impl<T2, Ts...>::stay(std::forward<T2>(t2), std::forward<TS>(ts)...)))
	{
		auto dispatch{std::forward<T1>(t1) <=> t2};
		if(dispatch < 0) return ufo::impl<T2, Ts...>::grow(std::forward<T2>(t2), std::forward<TS>(ts)...);
		else if(dispatch > 0) return ufo::impl<T2, Ts...>::shrink(std::forward<T2>(t2), std::forward<TS>(ts)...);
		else return ufo::impl<T2, Ts...>::stay(std::forward<T2>(t2), std::forward<TS>(ts)...);
	}
	static constexpr decltype(auto) grow(T1 &&T1, T2 &&t2, Ts &&...ts) noexcept(noexcept(ufo::impl<T2, Ts...>::grow(std::forward<T2>(t2), std::forward<TS>(ts)...)))
	{
		auto next{std::forward<T1>(t1) <=> t2};
		if(next < 0) return ufo::impl<T2, Ts...>::grow(std::forward<T2>(t2), std::forward<Ts>(ts)...);
		using RetVal = decltype(ufo::impl<T2, Ts...>::grow(std::forward<T2>(t2), std::forward<Ts>(ts)...));
		return RetVal{};
	}
	static constexpr decltype(auto) shrink(T1 &&T1, T2 &&t2, Ts &&...ts)
		noexcept(noexcept(ufo::impl<T2, Ts...>::shrink(std::forward<T2>(t2), std::forward<TS>(ts)...)))
	{
		auto next{std::forward<T1>(t1) <=> t2};
		if(next > 0) return ufo::impl<T2, Ts...>::shrink(std::forward<T2>(t2), std::forward<Ts>(ts)...);
		using RetVal = decltype(ufo::impl<T2, Ts...>::shrink(std::forward<T2>(t2), std::forward<Ts>(ts)...));
		return RetVal{};
	}
	static constexpr decltype(auto) stay(T1 &&T1, T2 &&t2, Ts &&...ts) noexcept(noexcept(ufo::impl<T2, Ts...>::stay(std::forward<T2>(t2), std::forward<TS>(ts)...)))
	{
		auto next{std::forward<T1>(t1) <=> t2};
		if(next == 0) return ufo::impl<T2, Ts...>::stay(std::forward<T2>(t2), std::forward<Ts>(ts)...);
		using RetVal = decltype(ufo::impl<T2, Ts...>::stay(std::forward<T2>(t2), std::forward<Ts>(ts)...));
		return RetVal{};
	}
};
template<class T1, class T2> struct ufo::impl<T1, T2> {
	static constexpr auto call(T1 &&t1, T2 &&t2) noexcept(noexcept(optional(std::forward<T1>(t1) <=> std::forward<T2>(t2)))) {
		return optional(std::forward<T1>(t1) <=> std::forward<T2>(t2));
	}
	static constexpr auto grow(T1 &&t1, T2 &&t2) noexcept(noexcept(optional(std::forward<T1>(t1) <=> std::forward<T2>(t2)))) {
		auto retVal{std::forward<T1>(t1) <=> std::forward<T2>(t2)};
		if(retVal < 0) return optional(std::move(retVal));
		return optional<decltype(retVal)>{};
	}
	static constexpr auto shrink(T1 &&t1, T2 &&t2) noexcept(noexcept(optional(std::forward<T1>(t1) <=> std::forward<T2>(t2)))) {
		auto retVal{std::forward<T1>(t1) <=> std::forward<T2>(t2)};
		if(retVal > 0) return optional(std::move(retVal));
		return optional<decltype(retVal)>{};
	}
	static constexpr auto stay(T1 &&t1, T2 &&t2) noexcept(noexcept(optional(std::forward<T1>(t1) <=> std::forward<T2>(t2)))) {
		auto retVal{std::forward<T1>(t1) <=> std::forward<T2>(t2)};
		if(retVal == 0) return optional(std::move(retVal));
		return optional<decltype(retVal)>{};
	}
};
template<class T1> struct ufo::impl<T1> {
	static constexpr auto call(T1 &&t1) noexcept(noexcept(optional(std::forward<T1>(t1) <=> std::forward<T2>(t2))) { return optional(t1 <=> t1)); }
};
template<> struct ufo::impl<> {
	static constexpr auto call() noexcept { return optional(0); }
};
#endif

template<typename T> using default_constructible_t = ::std::conditional_t<::std::is_default_constructible_v<T>, T, optional<T>>;

}

#ifndef SHOW_NOTHING

#include "show.h"

namespace nstd {

SHOW_NSTD_ROOT(1, optional);

}

#endif

#endif
