simpleutil
==========

A random collection of C++ util templates, include-only, small items not found on STL.

All the objects are defined in top-level namespace `nstd` (few of them are in deeper nested namespaces.)

Currently, this library contains the following headers.

simpleutil/adl\_or\_std.h
-------------------------

### Macros

<dl>
<p><dt><strong>ADL_OR_STD(name)</strong></dt>
<dd>Defines class <code><i>name</i></code> in namespace <code>adl&#95;or&#95;std</code> with a static method <code>call</code> such that <code>adl&#95;or&#95;std::<i>name</i>::call(t)</code> returns either <code>t.<i>name</i>()</code>, or <code>std::<i>name</i>(t)</code>, or simply <code><i>name</i>(t)</code>, whichever applicable. Additionally, it defines <code>operator()</code>, that does the same, static constexpr boolean function <code>is&#95;noexcept&lt;T&gt;()</code> that returns whether an appropriate call would be noexcept, static constexpr boolean member <code>value&lt;T&gt;</code> that is true whenever <code>T</code> can at all be an argument for <code><i>name</i></code> (either member, or in namespace <code>std</code>, or ADL-found) and member type <code>type&lt;T&gt;</code> that would be the result of such a call.</dd></p>
</dl>

simpleutil/optional.h
---------------------

### Classes

<dl>
<p><dt><strong>template&lt;class T&gt; struct optional;</strong></dt>
<dd>An augmented version of <code>std::optional</code>, this class defines operators that act monadically (i.e. if <code>b</code> is of type <code>option&lt;bool&gt;</code> then <code>!b</code> is <code>Nothing</code> when <code>b</code> is <code>Nothing</code>, and a <code>Just</code> negation of <code>b</code>&apos;s value otherwise; and <code>option&lt;F&gt;</code> defines <code>operator()</code> when <code>F</code> is a callable type) and additionally methods <code>fmap</code> and <code>flatmap</code>.</dd></p>
</dl>

simpleutil/range.h
------------------

### Classes

<dl>
<p><dt><strong>template&lt;class Itr, class End&gt; struct range;</strong></dt>
<dd>Pair of iterators that defines methods <code>begin</code>/<code>cbegin</code>, returning its first element, and <code>end</code>/<code>cend</code>, returning second.</dd></p>
<p><dt><strong>template&lt;class Ctr&gt; struct tail;</strong></dt>
<dd>Container adaptor whose elements are those of <code>Ctr</code> starting with the second.</dd></p>
<p><dt><strong>template&lt;class Ctr&gt; struct init;</strong></dt>
<dd>Container adaptor whose elements are those of <code>Ctr</code> not including the last.</dd></p>
<p><dt><strong>template&lt;class Ctr&gt; struct trim;</strong></dt>
<dd>Container adaptor. <code>trim(ctr, h, t)</code>, when iterated, behaves same as <code>ctr</code> itself except for <code>h</code> first and <code>t</code> last elements are skipped.</dd></p>
<p><dt><strong>template&lt;class... Ctrs&gt; class zip;</strong></dt>
<dd>Container adaptor whose elements are tuples consisting of corresponding elements of <code>Ctrs</code>. Iteration stops when the shortest of construents is exhausted.</dd></p>
<p><dt><strong>template&lt;class... Ctrs&gt; class mzip;</strong></dt>
<dd>Container adaptor whose elements are tuples consisting of <code>option&lt;T&gt;</code> where <code>T</code> is the element type of corresponding element of pack <code>Ctrs</code>. Iteration stops when the longest of construents is exhausted (for those exhausted before that, corresponding tuple elements are <code>Nothing</code>).</dd></p>
<p><dt><strong>template&lt;class F, class... Ctrs&gt; class transform</strong></dt>
<dd><code>transform(f, ctr1...)</code> is a container whose elements are <code>f(e1...)</code>, where <code>ei</code> is a corresponding element of <code>ctri</code>. Iteration stops when the shortest of construents is exhausted.</dd></p>
<p><dt><strong>template&lt;class F, class... Ctrs&gt; class flatten;</strong></dt>
<dd><code>flatten(f, ctr1...)</code> is a container whose elements are those of <code>f(e1...)</code>, iterated through in order, where <code>ei</code> is a corresponding element of <code>ctri</code>. (It is an error if <code>f</code> does not return an iterable when invoked on sets of elements of <code>ctrs</code>.) Iteration stops when the shortest of construents is exhausted.</dd></p>
<p><dt><strong>template&lt;class... Ctrs&gt; struct concat;</strong></dt>
<dd>Container adaptor whose elements are those of all the <code>Ctrs</code>, traversed in sequence. It is an error if element types of <code>Ctrs</code> are not compatible.</dd></p>
<p><dt><strong>template&lt;class Predicate, class Ctr&gt; struct filter;</strong></dt>
<dd>Container adaptor whose elements are those of <code>Ctr</code> that satisfy <code>Predicate</code>.</dd></p>
<p><dt><strong>template&lt;class Predicate, class Ctr&gt; struct take_while;</strong></dt>
<dd>The longest prefix of <code>Ctr</code> whose elements satisfy <code>Predicate</code>.</dd></p>
<p><dt><strong>template&lt;class Predicate, class Ctr&gt; struct drop_while;</strong></dt>
<dd>Suffix of <code>Ctr</code> that starts with the first element that does not satisfy <code>Predicate</code>.</dd></p>
<p><dt><strong>template&lt;class Ctr&gt; struct take;</strong></dt>
<dd><code>take(n, ctr)</code> is a sequence of <code>n</code> first elements of <code>ctr</code>.</dd></p>
<p><dt><strong>template&lt;class T, ::std::size_t n&gt; struct ring_array;</strong></dt>
<dd>A type basically similar to <code>std::array&lt;T, n&gt;</code> but with improved construction semantics and additional operations <code>push_back(T t)</code> and <code>emplace_back(args...)</code>. When one them is invoked, its argument(s) becomes the new rightmost element to the array, while all the other elements are shifted one position left, the leftmost one discarded, thus the length of the array remains constant. Appending an element is actually done in constant time, all the other elements are not copied or moved. Like its <code>std::</code> counterpart, provides tuple semantics (<code>nstd::is_tuple</code>, <code>nstd::tuple_size</code>, <code>nstd::tuple_element</code> and <code>nstd::apply</code>, as defined in <strong>typeutil.h</strong>.)</dd></p>
<p><dt><strong>template&lt;typename F, typename T, ::std::size_t n&gt; struct recurrence_relation;</strong></dt>
<dd>A finite difference equation, <code>recurrence_relation(f, t0, t1...)</code> is an object that, when dereferenced, returns <code>t0</code>; later, after it's been princremented, it returns <code>t1</code> when dereferenced. After all the initial arguments were returned, next dereference returns <code>f(t0, t1...)</code>; then it's <code>f(t1, ..., f(t0, t1...))</code>, and so forth. Together with <code>nstd::sequence</code> and <code>nstd::track</code>, this class makes the third way in this library to contain the list of all Fibonacci numbers in a single container; unlike the former, it is relatively fast and, unlike the latter, its memory footprint is only the storage for the function and all the arguments.</dd></p>
</dl>

### Functions

<dl>
<p><dt><strong>template&lt;class Container, class Range&gt; inline constexpr auto make_container(Range &amp;&amp;range);</strong></dt>
<dd>Returns <code>Container</code> whose elements are those of <code>range</code>.</dd></p>
<p><dt><strong>template&lt;class Ctr, class... Fs&gt; constexpr auto flatmap(Ctr &amp;&amp;ctr, Fs &amp;&amp;...fs);</strong></dt>
<dd>Applies monadic composition of <code>fs</code>, left-to-right, to <code>ctr</code> (i.e. <code>flatmap(c, f1, f2)</code> is the concatenation of results of <code>f2</code> applied to all the elements of <code>f1</code> applied to all the elements of <code>c</code>.)</dd></p>
<p><dt><strong>template&lt;class Head, class Tail&gt; inline constexpr auto cons(Head &amp;&amp;head, Tail &amp;&amp;tail);</strong></dt>
<dd>Prepends <code>head</code> to <code>tail</code> provided its type is compatible with that of elements of <code>tail</code>.</dd></p>
<p><dt><strong>template&lt;class Init, class Last&gt; inline constexpr auto push_back(Init &amp;&amp;init, Last &amp;&amp;last);</strong></dt>
<dd>Appends <code>last</code> to <code>init</code> provided its type is compatible with that of elements of <code>tail</code>.</dd></p>
</dl>

simpleutil/functor.h
--------------------

### Classes

<dl>
<p><dt><strong>template&lt;class Ctr&gt; struct functor;</strong></dt>
<dd>Container adaptor that defines monadic operations: apply a range-returning function to each element of container and join the results; fmap a function over container; discard (sequence) container&apos;s contents for the sake of a range-returning function; apply an operator (i.e. a function that operates over the whole range, not element-wise) to the container as a whole.</dd></p>
</dl>

### Functions

<dl>
<p><dt><strong>template&lt;typename Ctr&gt; inline constexpr void seq(Ctr &amp;&amp;ctr);</strong></dt>
<dd>Iterates through <code>ctr</code> discarding element values.</dd></p>
</dl>

simpleutil/sequence.h
---------------------

#### Highly experimental

<dl>
<p><dt><strong>template&lt;typename Element&gt; struct sequence;</strong></dt>
<dd>A generic sequence type that can hold inside (rather that copy contents of) any iterable whose element type is the same. Can be infinite, for instance, when self-referential:<br/>
<code>sequence&lt;unsigned int&gt; fibs;
fibs = concat(::std::vector{0u, 1u}, transform(add{}, fibs, tail(fibs)));
    // fibs is now the sequence of all Fibonacci numbers modulo (max unsigned int + 1)</code><br/>
One major drawback of this class is that due to combinatorial explosion, even at <strong>O2</strong> level, iterating over the aforementioned <code>fibs</code> is likely to run tens or hundreds of times slower than over its equivalent in Haskell.</dd></p>
<p><dt><strong>template&lt;typename Element, typename Sequence = sequence&lt;Element&gt;, typename Cache = ::std::vector&lt;Element&gt;&gt; struct track;</strong></dt>
<dd>A generic sequence type that can hold inside (rather that copy contents of) any iterable whose element type is the same. Can be infinite, for instance, when self-referential. One incredibly significant distinction from <code>sequence</code> is that <code>track</code>, when iterated, caches the underlying container's elements in a private member of type <code>Cache</code>. This leads to the fact that the Fibonacci example from above, if implemented using <code>track</code>:<br/>
<code>track&lt;unsigned int&gt; fibs;
fibs = concat(::std::vector{0u, 1u}, transform(add{}, fibs, tail(fibs)));
    // fibs is now the track of all Fibonacci numbers modulo (max unsigned int + 1)</code><br/>
runs approximately as fast as its Haskell equivalent, even at default optimization level.<br/>
One minor drawback of this class is that it currently lacks generic definition and can only be instantiated with default values for <code>Sequence</code> and <code>Cache</code>.</dd></p>

simpleutil/iterator.h
---------------------

### Classes

#### `namespace ::nstd::adl_or_std`

<dl>
<p><dt><strong>struct begin;</strong></dt>
<dd>As defined in adl&#95;or&#95;std.h, invokes an appropriate <code>begin</code> on a container.</dd></p>
<p><dt><strong>struct end;</strong></dt>
<dd>As defined in adl&#95;or&#95;std.h, invokes an appropriate <code>end</code> on a container.</dd></p>
<p><dt><strong>struct empty;</strong></dt>
<dd>As defined in adl&#95;or&#95;std.h, invokes an appropriate <code>empty</code> on a container.</dd></p>
</dl>

#### Iterator adapters

<dl>
<p><dt><strong>template&lt;class Sink, class Comma&gt; struct join_iterator;</strong></dt>
<dd><code>Sink</code> can be a <code>std::ostream</code>, or any container. When a value is sent to <code>join_iterator</code> same as it is done to <code>std::ostream_iterator</code> (via assignment or assingment to the result of dereferencing), first a <code>Comma</code> is sent to <code>Sink</code> if it is not the first value posted, and then the value itself is posted. To post a value on <code>std::ostream</code> means to output it via <code>&lt;&lt;</code>. To post an value on a container of this value&apos;s type, means add it to the container (either via <code>push_back</code>, or iteratorless <code>insert</code>.) To post a range to another range means to insert it. But comma is added first. Unless it is <code>void</code>; then this iterator simply glues together all the posted values.</dd></p>
<p><dt><strong>template&lt;class... Itrs&gt; class zip_iterator;</strong></dt>
<dd>This is the iterator type to <code>zip</code> from <strong>range.h</strong> but can be used on its own, to zip arbitrary iterators.</dd></p>
<p><dt><strong>template&lt;class... Itrs&gt; class mzip_iterator;</strong></dt>
<dd>This is the iterator type to <code>mzip</code> from <strong>range.h</strong> but can be used on its own, to mzip arbitrary iterators.</dd></p>
<p><dt><strong>template&lt;class F, class... Itrs&gt; struct transform_iterator;</strong></dt>
<dd>This is the iterator type to <code>transform</code> from <strong>range.h</strong> but can probably be used on its own, to transform arbitrary iterators ... stopping iteration is not that trivial.</dd></p>
<p><dt><strong>template&lt;class Left, class Right, class F, class... Fs&gt; class flat_iterator;</strong></dt>
<dd>This is the iterator type to <code>flatten</code> and <code>flatmap</code> from <strong>range.h</strong>.</dd></p>
<p><dt><strong>template&lt;class Itr, class End, class Aftermath&gt; struct concat_iterator;</strong></dt>
<dd>This is the iterator type to <code>concat</code> from <strong>range.h</strong>. <code>Aftermath</code> is the container to be iterated over when <code>Itr</code> reaches <code>End</code>.</dd></p>
<p><dt><strong>template&lt;class Predicate, class Itr, class End&gt; struct filter_iterator;</strong></dt>
<dd>This is the iterator type to <code>filter</code> from <strong>range.h</strong> but can be used on its own.</dd></p>
<p><dt><strong>template&lt;class Predicate, class Itr&gt; struct take_while_iterator;</strong></dt>
<dd>This is the iterator type to <code>take_while</code> from <strong>range.h</strong> but can be used on its own.</dd></p>
<p><dt><strong>template&lt;class Itr&gt; struct take_iterator;</strong></dt>
<dd>This is the iterator type to <code>take</code> from <strong>range.h</strong> but can be used on its own.</dd></p>
</dl>

simpleutil/polymorphic_object.h
-------------------------

### Classes

<dl>
<p><dt><strong>template&lt;class Proto, class... Interface&gt; struct polymorphic_object;</strong></dt>
<dd>Instances of this highly experimental class can store any values provided those conform to <code>Interface</code>, i.e. elements of <code>Interface</code> can be invoked on them, and produce results compatible with those when invoked on objects of type <code>Proto</code>. E.g. if we have the following declarations:
<pre>struct increment {
    template<typename T> int operator()(T &&t) { return std::forward<T>(t) + 1; }
};
polymorphic&#95;storage answer(42, increment{});</pre>
then <code>answer = 3.14;</code> is a valid statement and <code>answer.invoke(increment{})</code> should produce <code>4</code> (3.14 plus one converted to int.)</dd></p>
</dl>

### Functions

<dl>
<p><dt><strong>template&lt;class F, class Proto, class... Interface&gt; inline constexpr decltype(auto) invoke(F &amp;&amp;f, polymorphic_object&lt;Proto, Interface...&gt; &amp;duck);</strong></dt>
<dd>If <code>F</code> is any of <code>Interface</code>, invokes it on the stored object and returns the result; otherwise tries to call <code>std::invoke(std::forward&lt;F&gt;(f), duck);</code>.</dd></p>
<p><dt><strong>template&lt;class F, class Proto, class... Interface&gt; inline constexpr decltype(auto) invoke(F &amp;&amp;f, polymorphic_object&lt;Proto, Interface...&gt; const &amp;duck);</strong></dt>
<dd>If <code>F</code> is any of <code>Interface</code>, invokes it on the stored object and returns the result; otherwise tries to call <code>std::invoke(std::forward&lt;F&gt;(f), duck);</code>.</dd></p>
</dl>

simpleutil/functional.h
-----------------------

### Classes

<dl>
<p><dt><strong>template&lt;class C&gt; struct constructor;</strong></dt>
<dd>When invoked (via <code>constructor::call(a1...)</code>, <code>constructor::apply(a1...)</code> or <code>operator()(a1...)</code>) returns a fresh instance of <code>C</code> with <code>a1...</code> as construction arguments.</dd></p>
</dl>

### Functions

<dl>
<p><dt><strong>template&lt;class Root, class... Branches&gt; inline constexpr auto compose(Root &amp;&amp;r, Branches... bs);</strong></dt>
<dd>Returns a function that, when invoked with arguments <code>a1...</code> effectively returns <code>r(bs(a1...)...)</code>. If there&apos;s but a single branch, both <code>r</code> and that branch are forwarded into the function objects; when there are several branches, they are copied.<br/>
<code>r</code> can be a tuple of functions; in this case each one of them is applied to the results of <code>bs</code> (those latter called only once per composition invocation) and the result returned is the tuple of individual results.<br/>
Implementation is (probably) stack-friendly: no extra &quot;recursive&quot; (i.e. deeper in the stack) calls are performed when invoking a composition of objects one or both of which are compositions or pipes.</dd></p>
<p><dt><strong>template&lt;class... Fs&gt; inline constexpr auto pipe(Fs &amp;&amp;...fs);</strong></dt>
<dd>Returns a fresh function object that, when called with arguments <code>a1...</code> effectively calls the leftmost of <code>fs</code> with those arguments, passes returned value to the next function, and so forth. Each of <code>Fs</code> can be actually a tuple of functions; in this case they are called in parallel with the result(s) of previous function(s) as argument(s) and their results are fed as arguments to the subsequent function(s).<br/>
Implementation is (probably) stack-friendly: no extra &quot;recursive&quot; (i.e. deeper in the stack) calls are performed when invoking a pipeline of objects one or more of which are compositions or pipes.</dd></p>
</dl>

simpleutil/operator.h
---------------------

### Classes

<p><dt><strong>template&lt;class Operator&gt; struct mute;</strong></dt>
<dd>Changes <code>Operator</code>&apos;s methods <code>call</code>, <code>apply</code> and <code>operator()</code> into those returning void (discards return value.)</dd></p>
<p><dt><strong>template&lt;template&lt;class&gt; class Operator&gt; class unary_operator: protected sfinae;</strong></dt>
<dd>Basic class for unary operators.</dd></p>
<p><dt><strong>template&lt;template&lt;class&gt; class Operator&gt; class binary_operator: protected sfinae;</strong></dt>
<dd>Basic class for binary operators.</dd></p>
<p><dt><strong>template&lt;template&lt;class&gt; class Operator&gt; class ternary_operator: protected sfinae;</strong></dt>
<dd>Basic class for ternary operators.</dd></p>
The following table summarizes operator wrappers defined in this header.

<table>
<thead>
<tr><th>Usage</th><th>Arity</th><th>Equivalent C++ expression</th></tr>
</thead>
<tbody>
<tr><td><code>preinc::call(x)</code></td><td>1</td><td><code>++x</code></td></tr>
<tr><td><code>predec::call(x)</code></td><td>1</td><td><code>--x</code></td></tr>
<tr><td><code>typecast&lt;T&gt;::call(x)</code></td><td>1</td><td><code>(T)x</code></td></tr>
<tr><td><code>funcall::call(f, a1...)</code></td><td>&#8734;</td><td><code>f(a1...)</code></td></tr>
<tr><td><code>subscript::call(a, i)</code></td><td>2</td><td><code>a[i]</code></td></tr>
<tr><td><code>arrow::call(x)</code></td><td>1</td><td><code>x::operator-&gt;()</code></td></tr>
<tr><td><code>preinc::call(x)</code></td><td>1</td><td><code>++x</code></td></tr>
<tr><td><code>predec::call(x)</code></td><td>1</td><td><code>--x</code></td></tr>
<tr><td><code>un&#95;plus::call(x)</code></td><td>1</td><td><code>+x</code></td></tr>
<tr><td><code>un&#95;minus::call(x)</code></td><td>1</td><td><code>-x</code></td></tr>
<tr><td><code>bool&#95;not::call(x)</code></td><td>1</td><td><code>!x</code></td></tr>
<tr><td><code>bit&#95;not::call(x)</code></td><td>1</td><td><code>~x</code></td></tr>
<tr><td><code>dereference::call(x)</code></td><td>1</td><td><code>&#42;x</code></td></tr>
<tr><td><code>address&#95;of::call(x)</code></td><td>1</td><td><code>&amp;x</code></td></tr>
<tr><td><code>size&#95;of::call(x)</code></td><td>1</td><td><code>sizeof(x)</code></td></tr>
<tr><td><code>neuf&lt;C&gt;::call(a1...)</code></td><td>&#8734;</td><td><code>new C(a1...)</code></td></tr>
<tr><td><code>neuf&lt;C&gt;::call(a1...)</code></td><td>&#8734;</td><td><code>new C(a1...)</code></td></tr>
<tr><td><code>placement&#95;neuf&lt;C&gt;::call(buf, a1...)</code></td><td>&#8734;</td><td><code>new(buf) C(a1...)</code></td></tr>
<tr><td><code>neuf&lt;C&gt;::call(a1...)</code></td><td>&#8734;</td><td><code>new C(a1...)</code></td></tr>
<tr><td><code>effacer&lt;C&gt;::call(x)</code></td><td>1</td><td><code>delete x</code></td></tr>
<tr><td><code>member::call(a, b)</code></td><td>2</td><td><code>a-&gt;&#42;b</code></td></tr>
<tr><td><code>mul::call(a, b)</code></td><td>2</td><td><code>a &#42; b</code></td></tr>
<tr><td><code>div::call(a, b)</code></td><td>2</td><td><code>a / b</code></td></tr>
<tr><td><code>mod::call(a, b)</code></td><td>2</td><td><code>a % b</code></td></tr>
<tr><td><code>add::call(a, b)</code></td><td>2</td><td><code>a + b</code></td></tr>
<tr><td><code>sub::call(a, b)</code></td><td>2</td><td><code>a - b</code></td></tr>
<tr><td><code>shl::call(a, b)</code></td><td>2</td><td><code>a &lt;&lt; b</code></td></tr>
<tr><td><code>shr::call(a, b)</code></td><td>2</td><td><code>a &gt;&gt; b</code></td></tr>
<tr><td><code>bit&#95;and::call(a, b)</code></td><td>2</td><td><code>a &amp; b</code></td></tr>
<tr><td><code>bit&#95;or::call(a, b)</code></td><td>2</td><td><code>a | b</code></td></tr>
<tr><td><code>bit&#95;xor::call(a, b)</code></td><td>2</td><td><code>a ^ b</code></td></tr>
<tr><td><code>bool&#95;and::call(a, b)</code></td><td>2</td><td><code>a &amp;&amp; b</code></td></tr>
<tr><td><code>bool&#95;or::call(a, b)</code></td><td>2</td><td><code>a || b</code></td></tr>
<tr><td><code>bool&#95;xor::call(a, b)</code></td><td>2</td><td><code>a ^^ b (not in real C++)</code></td></tr>
<tr><td><code>assign::call(a, b)</code></td><td>2</td><td><code>a = b</code></td></tr>
<tr><td><code>assign&#95;add::call(a, b)</code></td><td>2</td><td><code>a += b</code></td></tr>
<tr><td><code>assign&#95;sub::call(a, b)</code></td><td>2</td><td><code>a -= b</code></td></tr>
<tr><td><code>assign&#95;mul::call(a, b)</code></td><td>2</td><td><code>a &#42;= b</code></td></tr>
<tr><td><code>assign&#95;div::call(a, b)</code></td><td>2</td><td><code>a /= b</code></td></tr>
<tr><td><code>assign&#95;mod::call(a, b)</code></td><td>2</td><td><code>a %= b</code></td></tr>
<tr><td><code>assign&#95;shl::call(a, b)</code></td><td>2</td><td><code>a &lt;&lt;= b</code></td></tr>
<tr><td><code>assign&#95;shr::call(a, b)</code></td><td>2</td><td><code>a &gt;&gt;= b</code></td></tr>
<tr><td><code>assign&#95;and::call(a, b)</code></td><td>2</td><td><code>a &amp;&amp;= b (not in real C++)</code></td></tr>
<tr><td><code>assign&#95;or::call(a, b)</code></td><td>2</td><td><code>a ||= b (not in real C++)</code></td></tr>
<tr><td><code>assign&#95;xor::call(a, b)</code></td><td>2</td><td><code>a ^^= b (not in real C++)</code></td></tr>
</tbody>
</table>

simpleutil/private\_namespace.h
-------------------------------

### Classes

<dl>
<p><dt><strong>class private_namespace;</strong></dt>
<dd>A non-instantiable, non-copyable, non-movable class. Children of this class are used throughout this library as namespaces only accessible to their friends.</dd></p>
</dl>

simpleutil/show.h
-----------------

### Classes

<dl>
<p><dt><strong>template&lt;class T&gt; class show&#95;type;</strong></dt>
<dd>This namespace-like struct defines methods:
<ul>
<li><code>show(stream)</code> inserts <code>T</code>&rsquo;s string representation into stream;</li>
<li><code>show()</code> returns this string representation;</li>
<li><code>indent(stream, tab)</code> does the same as <code>show(stream)</code>, but for template instantiations the representation is multi-line and indented, with <code>tab</code> as indenting character;</li>
<li><code>indent(tab = "\t")</code> returns such an indented string representation;</li>
</ul></dd></p>
</dl>

### Functions

<dl>
<p><dt><strong>template&lt;class T, class Stream&gt; inline Stream &amp;&amp;type&#95;name(Stream &amp;&amp;stream);</strong></dt>
<dd>Insert&apos;s <code>T</code>&apos;s string representation into <code>stream</code> as per <code>show&#95;type&lt;T&gt;::show(stream)</code>.</dd></p>
<p><dt><strong>template&lt;class T&gt; inline std::string type&#95;name();<br/>
template&lt;class T, class CharT, class Traits = ::std::char&#95;traits&lt;CharT&gt;, class Allocator = ::std::allocator&lt;CharT&gt;&gt; inline ::std::basic&#95;string&lt;CharT, Traits, Allocator&gt; type&#95;name();</strong></dt>
<dd>Returns string representation of <code>T</code>.</dd></p>
<p><dt><strong>template&lt;class T, class Stream&gt; inline Stream &amp;&amp;indented&#95;type&#95;name(Stream &amp;&amp;stream, char const &#42;tab = &quot;\t&quot;);</strong></dt>
<dd>Insert&apos;s <code>T</code>&apos;s string representation into <code>stream</code> as per <code>show&#95;type&lt;T&gt;::indent(stream)</code>.</dd></p>
<p><dt><strong>template&lt;class T&gt; inline std::string indented&#95;type&#95;name(char const *tab = &quot;\t&quot;);<br/>template&lt;class T, class CharT, class Traits = ::std::char&#95;traits&lt;CharT&gt;, class Allocator = ::std::allocator&lt;CharT&gt;&gt; inline ::std::basic&#95;string&lt;CharT, Traits, Allocator&gt; indented&#95;type&#95;name(char const *tab = &quot;\t&quot;);</strong></dt>
<dd>Returns string representation of &lt;T&gt;.</dd></p>
</dl>

### Macros

<dl>
<p><dt><strong>SHOW&#95;SIMPLE(<i>type</i>)</strong></dt>
<dd>After this macro invocation, classes and functions of this header show <code><i>type</i></code> as string <code>&quot;<i>type</i>&quot;</code>.</dd></p>
<p><dt><strong>SHOW&#95;NSTD&#95;SIMPLE(<i>type</i>)</strong></dt>
<dd>After this macro invocation, classes and functions of this header show <code>nstd::<i>type</i></code> as string <code>&quot;nstd::<i>type</i>&quot;</code>.</dd></p>
<p><dt><strong>SHOW&#95;STD&#95;SIMPLE(<i>type</i>)</strong></dt>
<dd>After this macro invocation, classes and functions of this header show <code>std::<i>type</i></code> as string <code>&quot;std::<i>type</i>&quot;</code>.</dd></p>
<p><dt><strong>SHOW&#95;ROOT(valence, <i>template</i>)</strong></dt>
<dd>After this macro invocation, classes and functions of this header show <code><i>template</i>&lt;Arg...&gt;</code> as string <code>&quot;<i>template</i>&lt;Arg...&gt;&quot;</code>.</dd></p>
<p><dt><strong>SHOW&#95;NSTD&#95;ROOT(valence, <i>template</i>)</strong></dt>
<dd>After this macro invocation, classes and functions of this header show <code>nstd::<i>template</i>&lt;Arg...&gt;</code> as string <code>&quot;nstd::<i>template</i>&lt;Arg...&gt;&quot;</code>.</dd></p>
<p><dt><strong>SHOW&#95;STD&#95;ROOT(valence, <i>template</i>)</strong></dt>
<dd>After this macro invocation, classes and functions of this header show <code>std::<i>template</i>&lt;Arg...&gt;</code> as string <code>&quot;std::<i>template</i>&lt;Arg...&gt;&quot;</code>.</dd></p>
<p><dt><strong>SHOW&#95;SEQ&#95;ROOT(<i>template</i>)</strong></dt>
<dd>After this macro invocation, classes and functions of this header show <code><i>template</i>&lt;Arg, a1...&gt;</code> as string <code>&quot;<i>template</i>&lt;Arg, a1...&gt;&quot;</code>.</dd></p>
<p><dt><strong>SHOW&#95;NSTD&#95;SEQ&#95;ROOT(<i>template</i>)</strong></dt>
<dd>After this macro invocation, classes and functions of this header show <code>nstd::<i>template</i>&lt;Arg, a1...&gt;</code> as string <code>&quot;nstd::<i>template</i>&lt;Arg, a1...&gt;&quot;</code>.</dd></p>
<p><dt><strong>SHOW&#95;STD&#95;SEQ&#95;ROOT(<i>template</i>)</strong></dt>
<dd>After this macro invocation, classes and functions of this header show <code>std::<i>template</i>&lt;Arg, a1...&gt;</code> as string <code>&quot;std::<i>template</i>&lt;Arg, a1...&gt;&quot;</code>.</dd></p>
</dl>

simpleutil/show\_stl.h
----------------------

This header contains **show.h**-like string representations for STL types.

simpleutil/typeutil.h
---------------------

This header contains various small useful definitions and utils used by the rest of the library. It is so huge that it makes no sense to enumerate them in this brief README.
